<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单管理</title>
<link href="../ExtJS4.2/resources/css/ext-all-neptune-rtl.css" rel="stylesheet">
<link href="../ExtJS4.2/css/icon.css" rel="stylesheet">
<script src="../ExtJS4.2/ext-all.js"></script>
<script src="../ExtJS4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="../js/localXHR.js"></script>
<script type="text/javascript" src="js/menu/menu.js"></script>
<style type="text/css">
.ux-form-search-trigger {background-image:url(../../ExtJS4.2/icons/zoom.png)!important; margin-top: 3px; width:16px; height:16px;}
.ux-form-search-trigger-over { background-position:-16px 0; }
.ux-form-search-trigger-click { background-position:-16px 0; }
.x-form-trigger-wrap-focus .x-form-trigger{
background-position: -64px 0;
}
.x-item-disabled .x-form-item-label, .x-item-disabled .x-form-field, .x-item-disabled .x-form-display-field, .x-item-disabled .x-form-cb-label, .x-item-disabled .x-form-trigger {
    filter: alpha(opacity=30);
    opacity: .3;
    color: red;
}
</style>
</head>
<body>
</body>
</html>