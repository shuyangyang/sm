<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
<link href="../ExtJS4.2/resources/css/ext-all-neptune-rtl.css" rel="stylesheet">
<link href="../ExtJS4.2/css/icon.css" rel="stylesheet">
<script src="../ExtJS4.2/ext-all.js"></script>
<script src="../ExtJS4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="../js/localXHR.js"></script>
<script type="text/javascript" src="../js/func.js"></script>
<script type="text/javascript" src="js/user/user.js"></script>
<script type="text/javascript" src="js/user/userController.js"></script>
<script type="text/javascript" src="js/user/updateUserAndDelUser.js"></script>
</head>
<body>
</body>
</html>