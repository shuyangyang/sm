var fields = [  
  {name:'user_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
  {name:'user_name'},  
  {name:'login_name'},  
  {name:'login_password'},
  {name:'user_status'},
  {name:'create_time'},
  {name:'update_time'}
];
//编辑用户
function updateUser(){
	var rec = grid.getSelectionModel().getLastSelected();
	var loginName = rec.get('login_name');
	updateForm.form.reset();
	updateUserwin.show();
	updateForm.form.load({
		url: '/user/loadUser?loginName=' + loginName,
		method:'POST',
		waitMsg : '正在载入数据...',
		reader: Ext.create('Ext.data.JsonReader', {
			 type  : 'json',
			 root  : 'data'
		 },fields),
		success : function(form,action) {},
		failure : function(form,action) {
			Ext.Msg.alert('编辑用户', '数据读取失败');
			updateUserwin.hide();
		}
	});
}
//删除用户
function delUser(){
	var loginNames = '';
	var userIds = '';
	var rec = grid.getSelectionModel().getSelection();
	for(var i=0; i<rec.length; i++){
		if(i!=rec.length-1){
			loginNames += rec[i].get('user_name')+',';
			userIds +=rec[i].get('user_id')+',';
		}else{
			loginNames += rec[i].get('user_name');
			userIds += rec[i].get('user_id');
		}
	}
		Ext.Msg.show({
			title:'删除用户',
			msg: '确认要将用户（' + loginNames + '）删除？',
			buttons: Ext.Msg.YESNOCANCEL,
			fn: function(btn, text) {
				if(btn == "yes") {
					Ext.Ajax.request({
						url: '/user/delUser?userIds='+userIds,
						success: function(response,options) {
							var respText = Ext.JSON.decode(response.responseText);
							if(respText.code != 'OK') {
								Ext.Msg.alert('删除用户失败', respText.message + "（" + respText.code + "）");
							}else{
								Ext.Msg.alert('删除用户成功', '用户（' + loginNames + '）已被删除！');
								store.load({params:{start:0,limit:15}}); 
							}
						},
						failure: function(response,options) {
							Ext.Msg.alert('删除用户失败', '删除用户（' + loginNames + '）失败！');
						}
					});
				}
			},
			icon: Ext.MessageBox.QUESTION
		});	
}
//编辑用户
var updateForm = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{  
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
				fieldLabel: 'ID',
				name: 'user_id',
				hidden:true
			},{
				fieldLabel: '用户名',
				name: 'user_name',
				maxLengthText :'用户名长度不能超过20',  
	            maxLength : 20,
				allowBlank:false
			},{
				fieldLabel: '登录名',
				name: 'login_name',
				allowBlank:false,
				disabled:true,
				maxLengthText :'登录名长度不能超过20',  
	            maxLength : 20
			},{
				fieldLabel: '登录密码',
				name: 'login_password',
				maxLengthText :'密码长度不能超过20',  
	            maxLength : 20,
	            minLengthText :'密码长度至少6位',  
	            minLength : 6,
				allowBlank:false,
				inputType: 'password'
			},{
				xtype: 'combobox',
				fieldLabel: '用户状态',
				name: 'user_status',
				allowBlank:false,
				queryMode: 'local',
			    displayField: 'name',
			    editable:false,
			    valueField: 'abbr',	
				store: Ext.create('Ext.data.Store', {
					 fields: ['abbr', 'name'],
					    data : [
					        {"abbr":"AVAILABLE", "name":"正常"},
					        {"abbr":"DISABLED", "name":"禁用"}
					    ]
	             })
			},{
				fieldLabel: '创建时间',
				name: 'create_time',
				hidden:true
			}
		],
		buttons: [{
			id:'doubleClickEditUser',
			text: '保&nbsp;存',
			handler: function() {
				if(updateForm.form.isValid()) {
					updateForm.form.submit({
						url: '/user/updaetUser',
						method: 'POST',
						success: function(form, action) {
							Ext.Msg.alert('系统提示', '修改用户信息成功！');
							updateUserwin.hide();
							store.load({params:{start:0,limit:15}}); 
						},
						failure: function(form, action) {
							switch (action.failureType) {
								case Ext.form.Action.CLIENT_INVALID:
									Ext.Msg.alert('修改失败', '数据字段格式错误！');
									break;
								case Ext.form.Action.CONNECT_FAILURE:
									Ext.Msg.alert('修改失败', '连接异常！');
									break;
								case Ext.form.Action.SERVER_INVALID:
								   Ext.Msg.alert('修改失败', action.result.message);
						   }
						},
						waitMsg: '数据保存中，请稍候...'
					});
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				updateUserwin.hide();
			}
		}]
});

var updateUserwin = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:250,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "编辑用户",
	iconCls: "Vcardedit",
	items: [updateForm]
});