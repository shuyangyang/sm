Ext.onReady(function() {
	//解决在extJS框架内容中超时点击登录问题，不会出现table中出现登录页面
	if(top.document.title != document.title) {
		top.document.location.href = "/Web-Admin/login.jsp";
	}
	
    var userLoginPanel = Ext.create('Ext.panel.Panel', {  
        bodyCls: 'bgimage',  
        border : false,  
        defaults:{  
            margin:'58 0'  
        },  
        items:{  
            xtype : 'tabpanel',  
            id : 'loginTabs',  
            activeTab : 0,
            height : 180,  
            border : false,  
            items:[{  
                title : "身份认证",  
                xtype : 'form',  
                id : 'loginForm',  
                defaults : {  
                    width : 260,  
                    margin: '20 0 0 70'  
                },  
                // The fields  
                defaultType : 'textfield',  
                labelWidth : 40,  
                items: [{  
                    fieldLabel: '用户名',
                    id  : 'loginname',
                    cls : 'user',  
                    name: 'loginname',  
                    labelAlign:'right',  
                    labelWidth:65,  
                    maxLength : 30,  
                    emptyText:'请在这里填写用户名',  
                    maxLengthText : '账号的最大长度为30个字符',  
                    blankText:"用户名不能为空，请填写！",//错误提示信息，默认为This field is required!  
                    allowBlank: false  
                },{  
                    fieldLabel: '密   码',  
                    cls : 'key',  
                    name: 'password',  
                    inputType:"password",  
                    labelWidth:65,  
                    labelAlign:'right',  
                    emptyText:'请在这里填写密码',  
                    maxLengthText :'密码长度不能超过20',
                    maxLength : 20,  
                    blankText:"密码不能为空，请填写！",//错误提示信息，默认为This field is required!  
                    allowBlank: false  ,
                    listeners : {
						specialkey : function(field, e) {
							if (e.getKey() == Ext.EventObject.ENTER) {
								login();
							}
						}
					}
                }, {  
                    id : 'id_reg_panel',  
                    xtype : 'panel',  
                    border : false,  
                    hidden : true,  
                    html : '<br><span id="messageTip" style="color:red"> </span>'  
                }]  
            }, {  
                title : '关于',  
                contentEl : 'aboutDiv',  
                defaults : {  
                    width : 230  
                }  
            }]  
        }  
    });  
      
    Ext.create('Ext.window.Window', {  
        title : 'SHUYANGYANG®中小学教学管理系统',  
        width : 440,  
        height : 300,  
        layout: 'fit',  
        plain : true,  
        modal : false,  
        maximizable : false,  
        draggable : true,  
        closable : false,  
        resizable : false,  
        items: userLoginPanel,
        defaultFocus:'loginname',
        // 重置 和 登录 按钮.  
        buttons: [{  
            text: '重置',  
            iconCls : 'Wrench',  
            handler: function() {  
            	clearCookie('shuyangyang.login.account');
				var account = Ext.getCmp('loginname');
				Ext.getCmp('loginForm').form.reset();
				account.setValue('');
				account.focus();
            }  
        }, {
            text: '登录',  
            iconCls : 'User',  
            handler: function() {
            	login();
            }
        }]
    }).show();
    
    /**
	 * 提交登陆请求
	 */
	function login() {
		if (Ext.getCmp('loginForm').form.isValid()) {
			Ext.getCmp('loginForm').form.submit({
				url : '/user/login',
				waitTitle : '提示',
				method : 'POST',
				waitMsg : '正在验证您的身份,请稍候.....',
				success : function(form, action) {
					var account = Ext.getCmp('loginname');
					setCookie("shuyangyang.login.account", account.getValue(), 240);
					setCookie("shuyangyang.login.userid", action.result.userid, 240);
					Ext.MessageBox.wait("登录成功，正在跳转中，请稍候...", "登陆中");
					window.location.href = '../index.jsp';
				},
				failure : function(form, action) {
					switch (action.failureType) {
						case Ext.form.Action.CLIENT_INVALID:
							Ext.Msg.alert('登录失败', '数据字段格式错误！');
							break;
						case Ext.form.Action.CONNECT_FAILURE:
							Ext.Msg.alert('登录失败', '连接异常！');
							break;
						case Ext.form.Action.SERVER_INVALID:
						   Ext.Msg.alert('登录失败', action.result.message);
					}
				}
			});
		}
	}
});  