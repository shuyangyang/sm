function manPermission(){
	 var columns = [
	                {xtype: 'rownumberer'},
	                {header:'编号',dataIndex:'permission_id',hidden: true},
	                {header:'权限名称',dataIndex:'permission_name',width:'20%'},
	                {header:'权限ID',dataIndex:'kind',width:'15%'},
	                {header:'权限状态',dataIndex:'value',width:'10%',renderer:function(value){
	                    if(value=='true'){
	                        return "<span style='color:red;font-weight:bold';>禁用</span>";  
	                    } else {
	                        return "<span style='color:green;font-weight:bold';>正常</span>";  
	                    }
	         		}},
	         	   {header:'描述',dataIndex:'description',width:'18%'},
	                {header:'创建时间',dataIndex:'create_time',renderer:function(value){
	             	   return formatDateTime(value);
	                },width:'18%'},
	                {header:'最后更新时间',dataIndex:'update_time',renderer:function(value){
	             	   return formatDateTime(value);
	                },width:'18%'}
	            ];
	 
	 var PermissionGridStore = Ext.create("Ext.data.Store",{
	        proxy:{
	            type:'ajax',  
	            url:'/permission/showPermissionByMenuId',
	            reader:{  
	                type:'json',  
	                totalProperty:'total',  
	                root:'data',  
	                idProperty:'id'  
	            }  
	        },  
	        fields:[
	           {name:'permission_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
	           {name:'permission_name'},
	           {name:'kind'},
	           {name:'value'},
	           {name:'create_time'},
	           {name:'update_time'},
	           {name:'description'}
	        ]  
	    });

	var menupermissionstore = Ext.create("Ext.data.Store",{
	    proxy:{  
	        type:'ajax',  
	        url:'/menu/showAllMenu',  
	        reader:{  
	            type:'json',  
	            totalProperty:'total',  
	            root:'data',  
	            idProperty:'id'  
	        }  
	    },  
	    fields:[  
	       {name:'menu_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
	       {name:'text'},
	       {name:'description'}
	    ]  
	});
	//数据排序
	menupermissionstore.sort('menu_id','asc');
	//加载数据  
	menupermissionstore.load(); 

	var sm = Ext.create('Ext.selection.CheckboxModel');
	var menugrid = Ext.create("Ext.grid.Panel",{
		region: 'center',
		border: false,
		store: PermissionGridStore,
		selModel: sm,
	    columns: columns,
	    region: 'center', //框架中显示位置，单独运行可去掉此段
	    loadMask:true, //显示遮罩和提示功能,即加载Loading……  
	    forceFit:true, //自动填满表格  
	    columnLines:false, //列的边框
	    rowLines:true, //设置为false则取消行的框线样式
	    dockedItems: [{
	    	xtype:'toolbar',
	    	dock:'top',
	    	displayInfo: true,
	    	items:[
	    	       '-',
	    	       {
	    	    	   fieldLabel: '菜单选项',
	    	    	   xtype:'combo',
	    	    	   id:'menuConfig',
	    	    	   name:'text',
	    	           valueField: 'menu_id',
	    	           store: menupermissionstore,
	    	           editable:false,
	    	           displayField: 'text',
	    	           listeners:{
	    	        	   select:function(combo,record,opts) {
	    	        		   menuId = record[0].raw.menu_id;
	    	        		   PermissionGridStore.removeAll();
	    	        		   PermissionGridStore.load({params:{menuId:menuId}}); 
	    	        	   }
	    	           }
	    	       },'-',{
	    	    	   xtype:'button',
	    	    	   id:'saveMenuPermission',
	    	    	   text: '保&nbsp;存',
	    	    	   iconCls:'Accept',
	    	    	   listeners: {
	    	    		   click:function(){
	    	    			   var rec =menugrid.getSelectionModel().getSelection();
	    	    			   var permissionIds="";
	    	    			   for(var i=0; i<rec.length; i++){
	    	    					if(i!=rec.length-1){
	    	    						permissionIds += rec[i].get('permission_id')+',';
	    	    					}else{
	    	    						permissionIds += rec[i].get('permission_id');
	    	    					}
	    	    				}
	    	    			   var menuId = Ext.getCmp('menuConfig').getValue();
	    	    			   var roleId = grid.getSelectionModel().getLastSelected().get('role_id');
	    	    			   if(menuId==null||permissionIds.length<=0){
	    	    				 Ext.Msg.alert('分配失败', "请至少选择一项！");
	    	    			   }else{
	    	    				   Ext.Ajax.request({
	   	    	   					url: '/menu/saveRoleMenuPermission?menuId='+menuId+"&permissionIds="+permissionIds+"&roleId="+roleId,
	   	    	   					success: function(response,options) {
	   	    	   						var respText = Ext.JSON.decode(response.responseText);
	   	    	   						if(respText.code != 'OK') {
	   	    	   							Ext.Msg.alert('分配失败', respText.message + "（" + respText.code + "）");
	   	    	   						}else{
	   	    	   							Ext.Msg.alert('分配成功', '操作成功！您可以继续操作当前菜单对应权限或者其他菜单权限。');
	   	    	   						}
	   	    	   					},
	   	    	   					failure: function(response,options) {
	   	    	   						Ext.Msg.alert('分配失败', '角色-菜单-权限关系保存失败！');
	   	    	   					}
	   	    	   				});
	    	    			   }
	    	    		   }
	    	    	   }
	    	       }]
	    }]
	});
	
	PermissionGridStore.on('load',function(store,records,options){
	    var menuId = Ext.getCmp('menuConfig').getValue();
	    var roleId = grid.getSelectionModel().getLastSelected().get('role_id');
	    Ext.Ajax.request({
			url: '/menu/showPermissionByParams?menuId='+menuId+"&roleId="+roleId,
			success: function(response,options) {
				var respText = Ext.JSON.decode(response.responseText);
				if(respText.code != 'OK') {
					//Ext.Msg.alert('读取失败', respText.message + "（" + respText.code + "）");
				}else{
					 var arr = [];
	            	 PermissionGridStore.each(function(record){
	                      for(var i =0; i<respText.data.length; i++){
	                          if(respText.data[i].permission_id == record.data.permission_id){
	                              arr.push(record);
	                              break;
	                          }
	                      }
	                  });
		            //console.info(arr); 选中已经选择过的权限
					sm.select(arr);
				}
			},
			failure: function(response,options) {
				Ext.Msg.alert('分配失败', '角色-菜单-权限关系保存失败！');
			}
		});
	});
	
	// 表格配置结束
	Ext.create('Ext.window.Window', {
		layout:'fit',
		width: 850,
		height:400,
		autoShow:true,
		maximizable: true,
		closeAction:'destroy',
		modal: true,
		plain: true,
		title: "分配角色-菜单-权限",
		iconCls: "Vcard",
		items: [menugrid]
	});
}
