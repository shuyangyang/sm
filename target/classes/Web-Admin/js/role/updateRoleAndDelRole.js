var fields = [  
  {name:'role_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
  {name:'role_name'},  
  {name:'status'},  
  {name:'description'},
  {name:'create_time'},
  {name:'update_time'}
];
//编辑角色
function updateRole(){
	var rec = grid.getSelectionModel().getLastSelected();
	var roleId = rec.get('role_id');
	updateForm.form.reset();
	updateRolewin.show();
	updateForm.form.load({
		url: '/role/loadRole?roleId=' + roleId,
		method:'POST',
		waitMsg : '正在载入数据...',
		reader: Ext.create('Ext.data.JsonReader', {
			 type  : 'json',
			 root  : 'data'
		 },fields),
		success : function(form,action) {},
		failure : function(form,action) {
			Ext.Msg.alert('编辑角色', '数据读取失败');
			updateRolewin.hide();
		}
	});
}
//删除角色
function delRole(){
	var roleNames = '';
	var roleIds = '';
	var rec = grid.getSelectionModel().getSelection();
	for(var i=0; i<rec.length; i++){
		if(i!=rec.length-1){
			roleNames += rec[i].get('role_name')+',';
			roleIds +=rec[i].get('role_id')+',';
		}else{
			roleNames += rec[i].get('role_name');
			roleIds += rec[i].get('role_id');
		}
	}
		Ext.Msg.show({
			title:'删除角色',
			msg: '确认要将角色（' + roleNames + '）删除？',
			buttons: Ext.Msg.YESNOCANCEL,
			fn: function(btn, text) {
				if(btn == "yes") {
					Ext.Ajax.request({
						url: '/role/delRole?roleIds='+roleIds,
						success: function(response,options) {
							var respText = Ext.JSON.decode(response.responseText);
							if(respText.code != 'OK') {
								Ext.Msg.alert('删除角色失败', respText.message + "（" + respText.code + "）");
							}else{
								Ext.Msg.alert('删除角色成功', '角色（' + roleNames + '）已被删除！');
								store.load({params:{start:0,limit:15}}); 
							}
						},
						failure: function(response,options) {
							Ext.Msg.alert('删除角色失败', '删除角色（' + roleNames + '）失败！');
						}
					});
				}
			},
			icon: Ext.MessageBox.QUESTION
		});	
}
//编辑角色
var updateForm = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{  
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
				fieldLabel: 'ID',
				name: 'role_id',
				hidden:true
			},{
				fieldLabel: '角色名',
				name: 'role_name',
				maxLengthText :'角色名长度不能超过20',  
	            maxLength : 20,
				allowBlank:false
			},{
				xtype: 'combobox',
				fieldLabel: '角色状态',
				name: 'status',
				allowBlank:false,
				queryMode: 'local',
				editable:false,
			    displayField: 'name',
			    valueField: 'abbr',	
				store: Ext.create('Ext.data.Store', {
					 fields: ['abbr', 'name'],
					    data : [
					        {"abbr":"AVAILABLE", "name":"正常"},
					        {"abbr":"DISABLED", "name":"禁用"}
					    ]
	             })
			},{
				fieldLabel: '角色描述',
				xtype: 'textareafield',
				name: 'description',
				inputType: 'description'
			},{
				fieldLabel: '创建时间',
				name: 'create_time',
				hidden:true
			}
		],
		buttons: [{
			id:'doubleClickSaveRole',
			text: '保&nbsp;存',
			handler: function() {
				if(updateForm.form.isValid()) {
					updateForm.form.submit({
						url: '/role/updateRole',
						method: 'POST',
						success: function(form, action) {
							Ext.Msg.alert('修改成功', '修改角色成功！');
							updateRolewin.hide();
							store.load({params:{start:0,limit:15}}); 
						},
						failure: function(form, action) {
							switch (action.failureType) {
								case Ext.form.Action.CLIENT_INVALID:
									Ext.Msg.alert('修改失败', '数据字段格式错误！');
									break;
								case Ext.form.Action.CONNECT_FAILURE:
									Ext.Msg.alert('修改失败', '连接异常！');
									break;
								case Ext.form.Action.SERVER_INVALID:
								   Ext.Msg.alert('修改失败', action.result.message);
						   }
						},
						waitMsg: '数据保存中，请稍候...'
					});
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				updateRolewin.hide();
			}
		}]
});

var updateRolewin = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:250,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "编辑角色",
	iconCls: "Vcardedit",
	items: [updateForm]
});