<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎进入学校综合管理平台V1.0</title>
<link href="ExtJS4.2/resources/css/ext-all-neptune.css" rel="stylesheet">
<link href="ExtJS4.2/css/icon.css" rel="stylesheet">
<script src="ExtJS4.2/ext-all.js"></script>
<script src="ExtJS4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="js/localXHR.js"></script>
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/nav.js"></script>
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
<script src="js/comboxGrid/GridComboBox.js"></script>
<script src="js/comboxGrid/GridComboBoxList.js"></script>
<script type="text/javascript" src="js/email.js"></script>

<style type="text/css">
a.nav-link  { 
	font:normal 20pt 100; 
	color:#000; 
	text-decoration:none;
	float:right;
	margin-right:20px;
	margin-top:40px;
	display:block;
}

a.nav-link:hover  { 
	color:#FF0000;
	text-decoration: none; 
	float:right;
	margin-right:20px;
	margin-top:40px;
	display:block;
}
.copy-right{
	font-family: "Century Gothic",Arial, Helvetica, sans-serif;
}
.copy-right a{
	background:none;
	color:#e54040;
	font-size:0.9em;
}
.copy-right a:hover{
	color:#666;
}
.boy-row .x-grid-cell {
	background-color: #FFFFDD;
    font-weight: bold;
}
.gril-row .x-grid-cell {
    background-color: #ededed;
}
</style>
<script type="text/javascript">
<!--
window.history.forward(1);
//-->
</script>
</head>
<body>
<div id="north-div" style="height: 75px; background-image: url('image/bg2.jpg');">
	<img alt="中小学教学管理系统" src="Web-Admin/images/Login_Banner.jpg" height="58px">
	<a href="#" class="nav-link" onclick="doQuit()"><img alt="退出系统" src="ExtJS4.2/icons/exclamation.png">&nbsp;退出</a>
	<a href="#" class="nav-link" onclick=''><img alt="帮助" src="ExtJS4.2/icons/help.png">&nbsp;帮助</a>
	<a href="#" class="nav-link" onclick='doChangePwd()'><img alt="修改密码" src="ExtJS4.2/icons/key.png">&nbsp;密码</a>
	<a href="#" class="nav-link" onclick='messageBox()'><span id="unReadMess"><img alt="站内消息" src="ExtJS4.2/icons/email.png">消息(0)</span></a>
	<!-- <a href="#" class="nav-link" onclick=''><img alt="站内消息" src="ExtJS4.2/icons/email.png">&nbsp;我的消息(0)</a> -->
	<a href="http://www.shuyangyang.com.cn" class="nav-link" target="_blank"><img alt="官方主页" src="ExtJS4.2/icons/house.png">&nbsp;主页</a>
</div>
<div id="south-div" align="center" class="copy-right">
&copy; All rights Reserved | Designed by <a href="http://www.shuyangyang.com.cn/">束洋洋</a>
</div>
<div id="ckeditorContent" style="height: 430px;"></div>
</body>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/web_socket.js"></script>
<script type="text/javascript" src="js/jquery.WebSocket.js"></script>
<script type='text/javascript'>
//ws://localhost/servlet/a
var ws;
var WEB_SOCKET_SWF_LOCATION = 'media/WebSocketMain.swf';
var WEB_SOCKET_DEBUG = true;
	$(function() {
		 connection();
	});
	
	function connection(){
		   ws = $.websocket({
		        domain:"127.0.0.1", 
		        port:"80",
		        protocol:"messagepush/email", 
		        onOpen:function(event){
		        	ws.send("${sessionScope.loginLogInfo.loginName}");  
		        },  
		        onError:function(event){
		         	//alert("error:"+ event)
		        },  
		        onMessage:function(result){  
		        	receiveMessage(result);
		        },
		        onClose:function(event){
		        	ws = null;
			    }
		    });  
	}
	
	function receiveMessage(result){
		showMessage(result);
	}
 
	function showMessage(msg){
		msg = msg.split(":");
		if(msg[0]=="${sessionScope.loginLogInfo.loginName}"){
			$("#unReadMess").html("<img alt='站内消息' src='ExtJS4.2/icons/email_starred.png'>消息("+msg[1]+")</span>");
		}
	}
</script>
</html>