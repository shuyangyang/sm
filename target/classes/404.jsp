<%@ page language="java" contentType="text/html; charset=UTF-8"	isErrorPage="true" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>此页面找不到了</title>
		<link href="/css/style.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<!--start-wrap--->
		<div class="wrap">
			<!---start-header---->
				<div class="header">
					<div class="logo">
						<h1><a href="javascript:history.back(-1);">页面找不到了</a></h1>
					</div>
				</div>
			<!---End-header---->
			<!--start-content------>
			<div class="content">
				<img src="/image/error-img.png" title="error" />
				<p><span><label>O</label>hh.....</span>你请求的页面找不到了。</p>
				<a href="/Web-Admin/login.jsp">返回首页</a>
				<div class="copy-right">
					<p>&copy; All rights Reserved | Designed by <a href="http://www.shuyangyang.com.cn/">束洋洋</a></p>
				</div>
   			</div>
			<!--End-Cotent------>
		</div>
		<!--End-wrap--->
	</body>
</html>