Ext.define('ExtMVC.view.portal.PortletPanel', {
    extend: 'ExtMVC.view.app.PortalPanel',    
    
    alias: 'widget.portletpanel',

    uses: ['ExtMVC.util.Constants'],

    getTools: function(){
        return [{
            xtype: 'tool',
            type: 'gear',
            handler: function(e, target, panelHeader, tool){
                var portlet = panelHeader.ownerCt;
                portlet.setLoading('Loading...');
                Ext.defer(function() {
                    portlet.setLoading(false);
                }, 1000);
            }
        },{
            xtype: 'tool',
            type: 'help',
            handler: function(e, target, panelHeader, tool){
            	Ext.Msg.alert('帮助', '此页面的面板都可以随意拖动关闭操作！');
            }
        }];
    },
    
    initComponent: function() {
    	
        Ext.apply(this, {
            items: [{
                id: 'col-1',
                items: [{
                    id: 'loginInfoPor',
                    iconCls:'House',
                    title: '登录信息',
                    tools: this.getTools(),
                    height:400
                },{
                    id: 'portlet-1',
                    iconCls:'House',
                    title: '公告信息',
                    tools: this.getTools(),
                    html: ExtMVC.util.Constants.shortBogusMarkup
                }]
            },{
                id: 'col-2',
                items: [{
                    id: 'portlet-2',
                    iconCls:'House',
                    title: '开发情况',
                    tools: this.getTools(),
                    html: ExtMVC.util.Constants.bogusMarkup
                }]
            },{
                id: 'col-3',
                items: [{
                    id: 'portlet-4',
                    iconCls:'House',
                    title: '系统状况',
                    tools: this.getTools(),
                    html: ExtMVC.util.Constants.shortBogusMarkup
                },{
                    id: 'portlet-5',
                    iconCls:'House',
                    title: '备注事项',
                    tools: this.getTools(),
                    html: ExtMVC.util.Constants.shortBogusMarkup
                },{
                    id: 'portlet-3',
                    iconCls:'House',
                    title: '待办事项',
                    tools: this.getTools(),
                    html: ExtMVC.util.Constants.shortBogusMarkup
                }]
            }]
            
        });
        
        var userinfo_tpl = new Ext.Template([
	'<p>登录帐号&nbsp;:&nbsp;&nbsp;&nbsp;<font color="green">{loginName}</font></p>',
	'<p>用户名称&nbsp;:&nbsp;&nbsp;&nbsp;<font color="green">{userName}</font></p>',
	'所属部门&nbsp;:&nbsp;&nbsp;&nbsp;<font color="green">{deptName}</font>',
	'<p>所属角色&nbsp;:&nbsp;&nbsp;&nbsp;<font color="green">{roleName}</font><p/>',
	'<hr/><h3>上次登录相关信息>></h3>',
	'<p>登录IP &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<font color="red">{ip}</font></p>',
	'登录时间&nbsp;:&nbsp;<font color="blue"> {loginTime}</font><br />',
	'<hr/><p style="color:red">友情提示：如果非本人登录，请注意修改密码。</p>'
                                     ]);
      //获取Session相关信息
        Ext.Ajax.request({
          url: '/loginLog/info',
          timeout: 30000,
        	success: function(response,options) {
        		var respText = Ext.JSON.decode(response.responseText);
        		if(respText.code == 'OK') {
        			var info = new Array();
        			info['userName'] = respText.data.userName;
        			info['deptName'] = respText.data.deptName;
        			info['loginName'] = respText.data.loginName;
        			info['ip'] = respText.data.ip;
        			info['roleName'] = respText.data.roleName;
        			info['loginTime'] = formatDateTime(respText.data.loginTime);
        			userinfo_tpl.overwrite(Ext.getCmp("loginInfoPor").body,info);
        		}else{}
        	},
        	failure: function(response,options) {}
        });
        
        this.callParent(arguments);
    }
});

