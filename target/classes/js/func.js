function decodeArgs(str) {
	if(!str) return "";
	var keys = new Array();
	var args = str.split(';');
	for(var i=0; i<args.length; i++) {
		var args2 = args[i].split('=');
		if(args2.length > 1) {
			var key = args2[0];
			var val = args2[1];
			keys[key] = val;
		} else {
			var key = args2[0];
			keys[key] = '';
		}
	}
	return keys;
}

function decodeURL(url) {
	url = url.replace('%3D', '=');
	return url;
}

function formatDate(time) {
	if(time && time!= '') {
		var date = new Date();
		date.setTime(time);
		return date.format('Y-m-d h:i:s');
	} else {
		return '';
	}
}

/**
 * 设置Cookie
 * 
 * @param {} name
 * @param {} value
 */
function setCookie(name, value, minuts) {
	var argv = setCookie.arguments;
	var argc = setCookie.arguments.length;
	var expiration = new Date((new Date()).getTime() + minuts * 60000 * 60);
	document.cookie = name
			+ "="
			+ escape(value)
			+ "; expires=" + expiration
					.toGMTString();
}

/**
 * 获取Cookie
 * 
 * @param {} Name
 * @return {}
 */
function getCookie(Name) {
	var search = Name + "="
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search)
		if (offset != -1) {
			offset += search.length
			end = document.cookie.indexOf(";", offset)
			if (end == -1)
				end = document.cookie.length
			return unescape(document.cookie.substring(offset, end))
		} else
			return ""
	}
}

/**
 * 从缓存中清除Cookie
 * 
 * @param {} name
 */
function clearCookie(name) {
	var expdate = new Date();
	expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
	setCookie(name, "", expdate);
}

/**
 * 格式化数据
 * 
 * @param {} type
 * @param {} val
 */
function formatValue(type, val) {
	return eval('format'+type+'(\''+val+'\')');
}

/**
 * 格式化字符串
 * 
 * @param {} val
 */
function formatString(val) {
	return val;
}

/**
 * 格式化布尔变量
 * 
 * @param {} val
 */
function formatBoolean(val) {
	if(val) {
		return '是';
	} else {
		return '否';
	}
}

/**
 * 格式化字符串
 * 
 * @param {} val
 */
function formatInteger(val) {
	return val;
}

/**
 * 格式化字符串
 * 
 * @param {} val
 */
function formatDateTime(val) {
	if(val && val != '' && val != 'null') {
		var date = new Date();
		date.setTime(val);
		var seperator1 = "-";
		var seperator2 = ":";
		var month = date.getMonth() + 1;
		var strDate = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (strDate >= 0 && strDate <= 9) {
			strDate = "0" + strDate;
		}
		var year = date.getYear();// + 1900;
		if(year<1000){
			year=year+1900;
		}
		var hour = date.getHours();
		if(hour < 10) {
			hour = "0" + hour;
		}

		var min = date.getMinutes();
		if(min < 10) {
			min = "0" + min;
		}
		
		var sec = date.getSeconds();
		if(sec < 10) {
			sec = "0" + sec;
		}

		var currentdate = year + seperator1 + month + seperator1 + strDate
				+ " " + hour + seperator2 + min
				+ seperator2 + sec;
		return currentdate;
	} else {
		return '';
	}
}

/**
 * 格式化时长
 * 
 * @param {} val
 */
function formatTimeLength(seconds) {
//	return seconds+'秒';
	var hh;
	var mm;
	var ss;
	//传入的时间为空或小于0
	if(seconds==null||seconds<0){
		return;
	}
	//得到小时
	hh=seconds/3600|0;
	seconds=parseInt(seconds)-hh*3600;
	if(parseInt(hh)<10){
		hh="0"+hh;
	}
	//得到分
	mm=seconds/60|0;
	//得到秒
	ss=parseInt(seconds)-mm*60;
	if(parseInt(mm)<10){
		mm="0"+mm;    
	}
	if(ss<10){
		ss="0"+ss;      
	}
	return hh+":"+mm+":"+ss;
}

/**
 * 格式化浮点型
 * 
 * @param {} val
 */
function formatFloat(val) {
	return val;
}

/**
 * 格式化比率型
 * 
 * @param {} val
 */
function formatRate(val) {
	return val + '%';
}

function formatTimestamp(timestamp) {
    return (new Date(timestamp * 1000)).format("yyyy-MM-dd hh:mm:ss");
} 
