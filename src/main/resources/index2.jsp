<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>欢迎进入学校综合管理平台V1.0</title>
<link href="ExtJS4.2/resources/css/ext-all-neptune.css" rel="stylesheet">
<link href="ExtJS4.2/css/icon.css" rel="stylesheet">
<script src="ExtJS4.2/ext-all.js"></script>
<script src="ExtJS4.2/locale/ext-lang-zh_CN.js"></script>
<script src="js/comboxGrid/GridComboBox.js"></script>
<script src="js/comboxGrid/GridComboBoxList.js"></script>
<script src="js/comboxGrid/page.js"></script>
</head>
<body>
<div id="demo"></div>
</body>
</html>