<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页</title>
</head>
<body>
<div id="showContent">
</div>
<h3>开发日记</h3>
<ul>
<li style="color: green">用户管理：完成，待测试</li>
<li style="color: green">部门管理：完成，待测试</li>
<li style="color: green">角色管理：完成，待测试</li>
<li style="color: green">菜单管理：完成，待测试</li>
<li style="color: red">权限管理：待完成，各模块串联起来 (部门-角色-菜单-权限)2015年10月13日 16:58:40</li>
<li>数据字典</li>
<li>待完善……</li>
</ul>
<h3>异常待解决</h3>
<ul type="circle">
<li>session超时或者为空，页面自动跳到登录页面</li>
<li>区别关闭、刷新浏览器事件（无法做到）</li>
<li>……</li>
</ul>
</body>
</html>