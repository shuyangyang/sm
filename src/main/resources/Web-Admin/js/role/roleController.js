function addRole() {
	form.form.reset();
	win.show();
}

function managerMeun(){
	var treestore = Ext.create('Ext.data.TreeStore', { 
		autoLoad : true,
		// 根节点的参数是parentId
        nodeParam : 'roleId',
        // 根节点的参数值是0
        defaultRootId : grid.getSelectionModel().getLastSelected().get('role_id'),
		proxy : {
             type : 'ajax',
             url : '/menu/showMenuTree',
             reader : {
                 type : 'json',
                 root : 'children'//数据  
             }
         },
	     root : {
	         text : '分配菜单',
	         expanded : true 
	     } 
	});
	
	var columns = [{
        text: '编号',
        flex: 1,
        dataIndex: 'menu_id',
        hidden: true,
        sortable: true
	},{
        xtype: 'treecolumn', //this is so we know which column will show the tree
        text: '名称',
        flex: 2,
        sortable: true,
        dataIndex: 'text'
    }];
	
	var tree = Ext.create("Ext.tree.Panel", {
		width		: 700,
		store		: treestore,
		rootVisible	: false,
	    //selModel: sm,
	    loadMask	: true, //显示遮罩和提示功能,即加载Loading……  
	    columnLines	: false, //列的边框
	    border		: false,
	    forceFit	: true, //自动填满表格 
	    rowLines	: false, //设置为false则取消行的框线样式
	    columns		: columns,
	    tbar: [{
            text: '保存',
            iconCls:'Bulletdisk',
            scope: this,
            handler: function(){
            	var menuIds = "";
            	var roleId = grid.getSelectionModel().getLastSelected().get('role_id');
            	var rec = tree.getChecked();
            	/*Ext.each(rec ,function(node){
            		console.info("node:"+node.menu_id);
            	});*/
            	//console.info(rec[0].raw);
            	for(var i=0; i<rec.length; i++){
            		if(i!=rec.length-1){
            			menuIds +=rec[i].raw.menu_id+',';
            		}else{
            			menuIds += rec[i].raw.menu_id;
            		}
            	}
            	Ext.Ajax.request({
					url: '/roleMenu/addRoleMenu?roleId='+roleId+"&menuIds="+menuIds,
					success: function(response,options) {
						var respText = Ext.JSON.decode(response.responseText);
						if(respText.code != 'OK') {
							Ext.Msg.alert('分配失败', respText.message + "（" + respText.code + "）");
						}else{
							Ext.Msg.alert('分配成功', '角色-菜单关系保存成功！');
							treewin.hide();
						}
					},
					failure: function(response,options) {
						Ext.Msg.alert('分配失败', '角色-菜单关系保存失败！');
					}
				});
            }
        },{
        	text:'刷新',
        	iconCls: "Arrowrefreshsmall",
        	handler:function(){
        		treestore.reload();
        	}
        }]
	});
	
	tree.expandAll();//展开所有节点
	var treewin = Ext.create('Ext.window.Window', {
		title: "分配菜单",
		iconCls: "Groupadd",
		layout:'fit',
		width: 700,
		height:500,
		closeAction:'hide',
		maximizable: true,
		modal: true,
		plain: true,
		items: [tree]
	});
	
	treewin.show();
}

//The data store containing the list of states
var states = Ext.create('Ext.data.Store', {
    fields: ['abbr', 'name'],
    data : [
        {"abbr":"1", "name":"有效"},
        {"abbr":"0", "name":"无效"}
    ]
});

//增加角色
var form = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{  
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
			fieldLabel: '角色名称',
			name: 'roleName',
			maxLengthText :'角色名称长度不能超过20',  
            maxLength : 20,
			allowBlank:false,
			inputType: 'roleName'
		},{
			fieldLabel: '角色状态',
			xtype:'combo',
            name:'roleStatus',
            valueField: 'abbr',
            allowBlank:false,
            store: states,
            editable:false,
            displayField: 'name',
            queryMode: 'local'
		},{
			fieldLabel: '描述',
			xtype: 'textareafield',
			name: 'description',
			inputType: 'description'
		}
		],
		buttons: [{
			text: '保&nbsp;存',
			handler: function() {
				if(form.form.isValid()) {
					form.form.submit({
						url: '/role/addRole',
						method: 'POST',
						success: function(form, action) {
							Ext.Msg.alert('保存成功', '增加角色成功！');
							win.hide();
							store.load({params:{start:0,limit:15}});
						},
						failure: function(form, action) {
							switch (action.failureType) {
								case Ext.form.Action.CLIENT_INVALID:
									Ext.Msg.alert('修改失败', '数据字段格式错误！');
									break;
								case Ext.form.Action.CONNECT_FAILURE:
									Ext.Msg.alert('修改失败', '连接异常！');
									break;
								case Ext.form.Action.SERVER_INVALID:
								   Ext.Msg.alert('修改失败', action.result.message);
						   }
						},
						waitMsg: '数据保存中，请稍候...'
					});
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				win.hide();
			}
		}]
});

var win = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:250,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "增加新角色",
	iconCls: "Vcard",
	items: [form]
});