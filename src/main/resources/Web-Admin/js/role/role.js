//引入扩展组件
Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Ext.ux', '../ExtJS4.2/ux/');

Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.tip.QuickTipManager',
    'Ext.ux.form.MultiSelect',
    'Ext.ux.form.ItemSelector',
    'Ext.ux.data.PagingMemoryProxy',
    'Ext.ux.ProgressBarPager'
]);
var store;
var grid;
Ext.onReady(function() {
	Ext.tip.QuickTipManager.init();
    var columns = [
                   {xtype: 'rownumberer'},
                   {header:'编号',dataIndex:'role_id',hidden: true},
                   {header:'角色名称',dataIndex:'role_name',width:'20%'},
                   {header:'角色状态',dataIndex:'status',width:'10%',renderer:function(value){  
                       if(value=='DISABLED'){  
                           return "<span style='color:red;font-weight:bold';>禁用</span>";  
                       } else {
                           return "<span style='color:green;font-weight:bold';>正常</span>";  
                       }
           			}},
                   {header:'角色描述',dataIndex:'description',width:'30%'},
                   {header:'创建时间',dataIndex:'create_time',width:'20%',renderer:function(value){
                	   return formatDateTime(value);
                   }},
                   {header:'最后更新时间',dataIndex:'update_time',width:'20%',renderer:function(value){
                	   return formatDateTime(value);
                   }}
               ];
	
    store = Ext.create("Ext.data.Store",{
    	pageSize:15, //每页显示几条数据  
        proxy:{  
            type:'ajax',  
            url:'/role/showRoles',  
            reader:{  
                type:'json',  
                totalProperty:'total',  
                root:'data',  
                idProperty:'id'  
            }  
        },  
        fields:[  
           {name:'role_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
           {name:'role_name'},  
           {name:'status'},
           {name:'create_time'},
           {name:'update_time'},
           {name:'description'}
        ]  
    });
    
    store.on('beforeload', function(loader, node) {
		//var userno = Ext.getCmp('qryUserNo').getValue();
		//var username = Ext.getCmp('qryUserName').getValue();
		//var usertype = Ext.getCmp('qryUserType').getValue();
		//store.baseParams = {userno:userno, username:username, usertype:usertype};
	});
    
    var sm = Ext.create('Ext.selection.CheckboxModel');
    grid = Ext.create("Ext.grid.Panel",{
    	region: 'center',
    	border: false,
    	store: store,
    	selModel: sm,
        columns: columns,
        region: 'center', //框架中显示位置，单独运行可去掉此段
        loadMask:true, //显示遮罩和提示功能,即加载Loading……  
        forceFit:true, //自动填满表格  
        columnLines:false, //列的边框
        rowLines:true, //设置为false则取消行的框线样式
        dockedItems: [{
        	xtype:'toolbar',
        	dock:'top',
        	displayInfo: true,
        	items:[
        	       {
        	    	   xtype:'splitbutton',
        	    	   text: '管理角色',
        	    	   iconCls:'Userhome',
        	    	   menu: Ext.create('Ext.menu.Menu', {
        	    	        items: [
								{id:'addRole',text: '增加角色',iconCls:'Useradd',hidden: true,listeners: {click:function(){addRole();}}},'-',
								{id:'editRole', text: '编辑角色',disabled:true,iconCls:'Useredit',hidden: true,listeners: {click:function(){updateRole();}}},'-',
								{id:'delRole', text: '删除角色',disabled:true,iconCls:'Userdelete',hidden: true,listeners: {click:function(){delRole();}}}
        	    	        ]
        	    	    })
        	       },
        	       {
        	    	   xtype:'button',
        	    	   id:'manMeun',
        	    	   disabled:true,
        	    	   text: '分配菜单',
        	    	   iconCls:'Reportuser',
        	    	   hidden: true,
        	    	   listeners: {
        	    		   click:function(){
        	    			   managerMeun();
        	    		   }
        	    	   }
        	       },
        	       {
        	    	   xtype:'button',
        	    	   id:'manMeunPerm',
        	    	   disabled:true,
        	    	   text: '分配菜单-权限',
        	    	   iconCls:'Reportuser',
        	    	   hidden: true,
        	    	   listeners: {
        	    		   click:function(){
        	    			   manPermission();
        	    		   }
        	    	   }
        	       },'->',
    	    	   {id:'searchInput',xtype: 'combo',name: 'name',hidden: true,
    	    		   fieldLabel: '角色名称(联想输入，自动检索)',labelAlign:'left',labelWidth:200,
    	    		   store:store, 
    	    		   hideTrigger:true,
    	    		   displayField: 'role_name',
    	               valueField: 'role_name',
    	               queryMode:"local",
    	               editable:true,
    	               triggerAction:"all"
    	           }]
        },{
            xtype: 'pagingtoolbar',
            store: store,   // GridPanel使用相同的数据源
            dock: 'bottom',
            displayInfo: true,
            plugins: Ext.create('Ext.ux.ProgressBarPager'),
            emptyMsg: "没有记录" //没有数据时显示信息
        }]
    });
    //数据排序
    store.sort('create_time','asc');
    //加载数据  
    store.load({params:{start:0,limit:15}}); 
    // 表格配置结束
    
    //监听表格
    grid.getSelectionModel().on({
        selectionchange: function(sm, selections) {
            if (selections.length) {
            	Ext.getCmp('editRole').setDisabled(false);
            	Ext.getCmp('delRole').setDisabled(false);
            	Ext.getCmp('manMeun').setDisabled(false);
            	Ext.getCmp('manMeunPerm').setDisabled(false);
            } else {
            	Ext.getCmp('editRole').setDisabled(true);
            	Ext.getCmp('delRole').setDisabled(true);
            	Ext.getCmp('manMeun').setDisabled(true);
            	Ext.getCmp('manMeunPerm').setDisabled(true);
            }
        }
    });
    
    grid.getView().on('render', function(view) {
        view.tip = Ext.create('Ext.tip.ToolTip', {
            // 所有的目标元素
            target: view.el,
            // 每个网格行导致其自己单独的显示和隐藏。
            delegate: view.itemSelector,
            // 在行上移动不能隐藏提示框
            trackMouse: true,
            // 立即呈现，tip.body可参照首秀前。
            renderTo: Ext.getBody(),
            //自定义的样式
            //baseCls:"tip-body",
            height:30,
            listeners: {
                // 当元素被显示时动态改变内容.
                beforeshow: function updateTipBody(tip) {
                    tip.update('描述： "' + view.getRecord(tip.triggerElement).get('description') + '"');
                }
            }
        });
    });
    
    //表格右键菜单  
    var contextmenu = new Ext.menu.Menu({
        id:'theContextMenu',  
        items:[{
        	id:'rightAddRole',
            text:'增加角色',
            iconCls:'Useradd',
            hidden: true,
            handler:function(){  
            	addRole();  
            }  
        },{
        	id:'rightEditRole',
            text:'编辑角色',
            iconCls:'Useredit',
            hidden: true,
            handler:function(){  
            	updateRole();
            }  
        },{
        	id:'rightDelRole',
            text:'删除角色',
            iconCls:'Userdelete',
            hidden: true,
            handler:function(){  
            	delRole();
            }  
        },{
        	id:'rightManMenu',
            text:'分配菜单',
            iconCls:'Groupadd',
            hidden: true,
            handler:function(){  
            	managerMeun();
            }  
        },{
        	id:'rightRefresh',
        	text:'刷新',
        	iconCls:'Arrowrefresh',
        	hidden: true,
        	handler:function(){
        		store.reload();
        	}
        }]  
    });  
    
    grid.on("itemcontextmenu",function(view,record,item,index,e){  
        e.preventDefault();  
        contextmenu.showAt(e.getXY());  
    });
    
    grid.on("itemdblclick",function(grid, row){
    	updateRole(); 
    });
    

	Ext.Ajax.request({
        url: '/menuPer/getMenuPer',
        timeout: 30000,
      	success: function(response,options) {
      		var respText = Ext.JSON.decode(response.responseText);
      		if(respText.code == 'OK') {
      			var rec = respText.data;
      			for(var i=0; i<rec.length; i++){
      				if(rec[i].kind=='addRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('addRole').hide();
      					}else{
      						Ext.getCmp('addRole').show();
      					}
      				}
      				if(rec[i].kind=='editRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('editRole').hide();
      					}else{
      						Ext.getCmp('editRole').show();
      					}
      				}
      				if(rec[i].kind=='delRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('delRole').hide();
      					}else{
      						Ext.getCmp('delRole').show();
      					}
      				}
      				if(rec[i].kind=='manMeun'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('manMeun').hide();
      					}else{
      						Ext.getCmp('manMeun').show();
      					}
      				}
      				if(rec[i].kind=='manMeunPerm'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('manMeunPerm').hide();
      					}else{
      						Ext.getCmp('manMeunPerm').show();
      					}
      				}
      				if(rec[i].kind=='searchInput'){
      					var addUserV = rec[i].value;
      					//按钮不能使用.hide();
      					if(addUserV =='true'){
      						Ext.getCmp('searchInput').hide();
      					}else{
      						Ext.getCmp('searchInput').show();
      					}
      				}
      				if(rec[i].kind=='rightAddRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('rightAddRole').hide();
      					}else{
      						Ext.getCmp('rightAddRole').show();
      					}
      				}
      				if(rec[i].kind=='rightEditRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('rightEditRole').hide();
      					}else{
      						Ext.getCmp('rightEditRole').show();
      					}
      				}
      				if(rec[i].kind=='rightDelRole'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('rightDelRole').hide();
      					}else{
      						Ext.getCmp('rightDelRole').show();
      					}
      				}
      				if(rec[i].kind=='rightManMenu'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('rightManMenu').hide();
      					}else{
      						Ext.getCmp('rightManMenu').show();
      					}
      				}
      				if(rec[i].kind=='rightRefresh'){
      					var addUserV = rec[i].value;
      					if(addUserV =='true'){
      						Ext.getCmp('rightRefresh').hide();
      					}else{
      						Ext.getCmp('rightRefresh').show();
      					}
      				}
      			}
      		}
      	},
      	failure: function(response,options) {}
      });

    
	// 整体架构容器
	Ext.create("Ext.container.Viewport", {
		layout : 'border',
		autoHeight: true,
		border: false,
		items : [grid]
	});
});