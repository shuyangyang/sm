//引入扩展组件
Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Ext.ux', '../ExtJS4.2/ux/');

Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.tip.QuickTipManager',
    'Ext.ux.form.MultiSelect',
    'Ext.ux.form.ItemSelector',
    'Ext.ux.data.PagingMemoryProxy',
    'Ext.ux.ProgressBarPager'
]);
var store;
var grid;
Ext.onReady(function() {
	Ext.tip.QuickTipManager.init();
	
    var columns = [
                   {xtype: 'rownumberer'},
                   {header:'素材编号',dataIndex:'article_id',hidden:true},
                   {header:'采集正文内容',dataIndex:'content',width:'53%'},
                   {header:'采集的图片',dataIndex:'picUrl',width:'10%',renderer:function(value,record){
                	   if(value==null){
                		   return "无";
                	   }else{
                		   return "<a href='/showPic.jsp?picurl="+value+"' target='_blank'>查看图片</a>";
                	   }
           			}},
                   {header:'原帖地址',dataIndex:'article_url',width:'10%',renderer:function(value){  
                	   return "<a href='"+value+"' target='_blank'>原帖地址</a>";
          			}},
                   {header:'采集分类',dataIndex:'source',width:'7%'},
                   {header:'采集时间',dataIndex:'createTime',width:'20%',renderer:function(value){
                	   return formatDateTime(value);
                   }}
               ];
	
    store = Ext.create("Ext.data.Store",{
    	pageSize:15, //每页显示几条数据  
        proxy:{  
            type:'ajax',  
            url:'/weibo/showWeibos',  
            reader:{  
                type:'json',  
                totalProperty:'total',  
                root:'data',  
                idProperty:'id'  
            }  
        },  
        fields:[  
           {name:'article_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
           {name:'content'},  
           {name:'picUrl'},
           {name:'article_url'},
           {name:'source'},
           {name:'createTime'}
        ]  
    });
    
    var sm = Ext.create('Ext.selection.CheckboxModel');
    grid = Ext.create("Ext.grid.Panel",{
    	region: 'center',
    	border: false,
    	store: store,
    	selModel: sm,
        columns: columns,
        region: 'center', //框架中显示位置，单独运行可去掉此段
        loadMask:true, //显示遮罩和提示功能,即加载Loading……  
        forceFit:true, //自动填满表格  
        columnLines:false, //列的边框
        rowLines:true, //设置为false则取消行的框线样式
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: store,   // GridPanel使用相同的数据源
            dock: 'bottom',
            displayInfo: true,
            plugins: Ext.create('Ext.ux.ProgressBarPager'),
            emptyMsg: "没有记录" //没有数据时显示信息
        }]
    });
    //数据排序
    store.sort('createTime','asc');
    //加载数据  
    store.load({params:{start:0,limit:15}}); 
    // 表格配置结束

    
    grid.getView().on('render', function(view) {
        view.tip = Ext.create('Ext.tip.ToolTip', {
            // 所有的目标元素
            target: view.el,
            // 每个网格行导致其自己单独的显示和隐藏。
            delegate: view.itemSelector,
            // 在行上移动不能隐藏提示框
            trackMouse: true,
            // 立即呈现，tip.body可参照首秀前。
            renderTo: Ext.getBody(),
            //自定义的样式
            //baseCls:"tip-body",
            height:100,
            listeners: {
                // 当元素被显示时动态改变内容.
                beforeshow: function updateTipBody(tip) {
                	//<img src='"+value+"' width='50px' height='50px'>
                    tip.update(view.getRecord(tip.triggerElement).get('content') + "<br>");
                }
            }
        });
    });
    
    //表格右键菜单  
    var contextmenu = new Ext.menu.Menu({
    	id:'houhou',
        items:[{
            text:'无趣内容',
            iconCls:'Emoticonunhappy',
            handler:function(){
            	var rec = grid.getSelectionModel().getLastSelected();
            	var articleId = rec.get('article_id');
            	Ext.Ajax.request({
        			url: '/weibo/delSucai?articleId='+articleId,
        			success: function(response,options) {
        				Ext.Msg.alert('提示', '删除成功！');
        				store.reload();
        			},
        			failure: function(response,options) {
        				Ext.Msg.alert('提示', '删除失败！');
        			}
        		});
            }  
        },{
        	text:'刷新',
        	iconCls:'Arrowrefresh',
        	handler:function(){
        		store.reload();
        	}
        }]  
    });  
    
    grid.on("itemcontextmenu",function(view,record,item,index,e){  
        e.preventDefault();  
        contextmenu.showAt(e.getXY());  
    });

    
	// 整体架构容器
	Ext.create("Ext.container.Viewport", {
		layout : 'border',
		autoHeight: true,
		border: false,
		items : [grid]
	});
});