function addPermission(){
	form.form.reset();
	win.show();
}
var fields = [  
   {name:'permission_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
   {name:'permission_name'},
   {name:'kind'},
   {name:'value'},
   {name:'create_time'},
   {name:'update_time'},
   {name:'description'}
];
function updatePermission(){
	var rec = grid.getSelectionModel().getLastSelected();
	var permissionId = rec.get('permission_id');
	editForm.form.reset();
	editWin.show();
	editForm.form.load({
		url: '/permission/loadPermission?permissionId=' + permissionId,
		method:'POST',
		waitMsg : '正在载入数据...',
		reader: Ext.create('Ext.data.JsonReader', {
			 type  : 'json',
			 root  : 'data'
		 },fields),
		success : function(form,action) {},
		failure : function(form,action) {
			Ext.Msg.alert('系统提示', '数据读取失败');
			editWin.hide();
		}
	});
}

//删除权限
function delPermission(){
	var permissionNames = '';
	var permissionIds = '';
	var rec = grid.getSelectionModel().getSelection();
	for(var i=0; i<rec.length; i++){
		if(i!=rec.length-1){
			permissionNames += rec[i].get('permission_name')+',';
			permissionIds +=rec[i].get('permission_id')+',';
		}else{
			permissionNames += rec[i].get('permission_name');
			permissionIds += rec[i].get('permission_id');
		}
	}
	Ext.Msg.show({
		title:'删除权限',
		msg: '确认要将权限（' + permissionNames + '）删除？',
		buttons: Ext.Msg.YESNOCANCEL,
		fn: function(btn, text) {
			if(btn == "yes") {
				Ext.Ajax.request({
					url: '/permission/delPermission?permissionIds='+permissionIds,
					success: function(response,options) {
						var respText = Ext.JSON.decode(response.responseText);
						if(respText.code != 'OK') {
							Ext.Msg.alert('系统提示', respText.message + "（" + respText.code + "）");
						}else{
							Ext.Msg.alert('系统提示', '权限（' + permissionNames + '）已被删除！');
							gridStore.load({params:{menuId:menuId}});
						}
					},
					failure: function(response,options) {
						Ext.Msg.alert('系统提示', '删除权限（' + permissionNames + '）失败！');
					}
				});
			}
		},
		icon: Ext.MessageBox.QUESTION
	});	
}

var valueStates = Ext.create('Ext.data.Store', {
    fields: ['abbr', 'name'],
    data : [
        {"abbr":"true", "name":"禁用"},
        {"abbr":"false", "name":"正常"}
    ]
});
//增加权限
var form = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
			fieldLabel: '权限名称',
			name: 'permissionName',
			maxLengthText :'权限名长度不能超过30',  
            maxLength : 30,
			allowBlank:false,
			inputType: 'permissionName'
		},{
			fieldLabel: '权限ID',
			name: 'kind',
			allowBlank:false,
			maxLengthText :'KIND长度不能超过40',  
            maxLength : 40, 
			inputType: 'kind'
		},{
			fieldLabel: '权限状态',
			name: 'value',
			xtype:'combo',
			valueField: 'abbr',
            allowBlank:false,
            store: valueStates,
            editable:false,
            displayField: 'name',
            queryMode: 'local'
		},{
			fieldLabel: '描述',
			xtype: 'textareafield',
			name: 'description',
			maxLengthText :'KIND长度不能超过200',  
            maxLength : 200, 
			inputType: 'description'
		}
		],
		buttons: [{
			text: '保&nbsp;存',
			handler: function() {
				if(form.form.isValid()) {
					if(menuId==undefined){
						menuId = "";
					}
					form.form.submit({
						url: '/permission/addPermission?menuId='+menuId,
						method: 'POST',
						success: function(form, action) {
							Ext.Msg.alert('保存成功', '添加权限成功！');
							win.hide();
							gridStore.load({params:{menuId:menuId}}); 
						},
						failure: function(form, action) {
							switch (action.failureType) {
								case Ext.form.Action.CLIENT_INVALID:
									Ext.Msg.alert('添加失败', '数据字段格式错误！');
									break;
								case Ext.form.Action.CONNECT_FAILURE:
									Ext.Msg.alert('添加失败', '连接异常！');
									break;
								case Ext.form.Action.SERVER_INVALID:
								   Ext.Msg.alert('添加失败', action.result.message);
						   }
						},
						waitMsg: '数据保存中，请稍候...'
					});
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				win.hide();
			}
		}]
});

var win = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:300,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "添加权限",
	iconCls: "Vcard",
	items: [form]
});

//编辑权限
var editForm = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
			fieldLabel: 'ID',
			name: 'permission_id',
			hidden:true
		},{
			fieldLabel: '权限名称',
			name: 'permission_name',
			maxLengthText :'权限名长度不能超过30',  
            maxLength : 30,
			allowBlank:false
		},{
			fieldLabel: '权限ID',
			name: 'kind',
			allowBlank:false,
			maxLengthText :'KIND长度不能超过40',  
            maxLength : 40
		},{
			fieldLabel: '权限状态',
			xtype:'combo',
            name:'value',
            valueField: 'abbr',
            allowBlank:false,
            editable:false,
            displayField: 'name',
            queryMode: 'local',
            store: valueStates
		},{
			fieldLabel: '描述',
			xtype: 'textareafield',
			maxLengthText :'KIND长度不能超过200',  
            maxLength : 200, 
			name: 'description'
		}
		],
		buttons: [{
			id:'savePermission',
			text: '保&nbsp;存',
			handler: function() {
				if(editForm.form.isValid()) {
					if(menuId==undefined){
						menuId = "";
					}
					editForm.form.submit({
						url: '/permission/updaetPermission',
						method: 'POST',
						success: function(form, action) {
							Ext.Msg.alert('系统提示', '编辑权限成功！');
							editWin.hide();
							gridStore.load({params:{menuId:menuId}}); 
						},
						failure: function(form, action) {
							switch (action.failureType) {
								case Ext.form.Action.CLIENT_INVALID:
									Ext.Msg.alert('系统提示', '数据字段格式错误！');
									break;
								case Ext.form.Action.CONNECT_FAILURE:
									Ext.Msg.alert('系统提示', '连接异常！');
									break;
								case Ext.form.Action.SERVER_INVALID:
								   Ext.Msg.alert('系统提示', action.result.message);
						   }
						},
						waitMsg: '数据保存中，请稍候...'
					});
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				editWin.hide();
			}
		}]
});

var editWin = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:300,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "编辑权限",
	iconCls: "Vcard",
	items: [editForm]
});