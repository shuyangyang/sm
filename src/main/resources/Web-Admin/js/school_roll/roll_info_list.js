//引入扩展组件
Ext.Loader.setConfig({enabled: true});

Ext.Loader.setPath('Ext.ux', '../ExtJS4.2/ux/');

Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.util.*',
    'Ext.tip.QuickTipManager',
    'Ext.ux.data.PagingMemoryProxy',
    'Ext.ux.ProgressBarPager'
]);
var store;
var grid;
var userId;
Ext.onReady(function() {
    var columns = [
                   {xtype: 'rownumberer'},
                   {header:'学籍编号',dataIndex:'user_id',hidden: true},
                   {header:'学号',dataIndex:'user_name'},
                   {header:'姓名',dataIndex:'user_name'},
                   {header:'性别',dataIndex:'login_name'},
                   {header:'班级',dataIndex:'login_password'},
           		   {header:'国籍',dataIndex:'login_name'},
                   {header:'民族',dataIndex:'user_status'},
                   {header:'就读方式',dataIndex:'user_status'},
                   {header:'入学时间',dataIndex:'create_time',renderer:function(value){
                	   return formatDateTime(value);
                   }},
                   {header:'毕业时间',dataIndex:'update_time',width:150,renderer:function(value){
                	   return formatDateTime(value);
                   }}
               ];
	
    store = Ext.create("Ext.data.Store",{
    	pageSize:15, //每页显示几条数据  
        proxy:{  
            type:'ajax',  
            url:'/user/showUser',  
            reader:{  
                type:'json',
                totalProperty:'total',  
                root:'data',  
                idProperty:'id'  
            }  
        },  
        fields:[  
           {name:'user_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
           {name:'user_name'},  
           {name:'login_name'},  
           {name:'login_password'},
           {name:'user_status'},
           {name:'create_time'},
           {name:'update_time'}
        ]  
    });
    
    var user_dept_store =Ext.create("Ext.data.Store",{
        proxy:{
            type:'ajax',  
            url:'/roll/findRollInfoList',
            extraParams:{
                'userId':userId
            },
            reader:{
                type:'json',
                totalProperty:'total',  
                root:'data',  
                idProperty:'dept_id'  
            }
        },
        fields:[  
           {name:'dept_name'},
           {name:'description'}
        ]  
    });
    
    var sm = Ext.create('Ext.selection.CheckboxModel');
    grid = Ext.create("Ext.grid.Panel",{
    	id:'userGrid',
    	border: false,
    	autoScroll:true,
    	store: store,
    	selModel: sm,
        columns: columns,
        height:520,
        region: 'center', //框架中显示位置，单独运行可去掉此段
        loadMask:true, //显示遮罩和提示功能,即加载Loading……  
        forceFit:true, //自动填满表格  
        columnLines:false, //列的边框
        rowLines:true, //设置为false则取消行的框线样式
        dockedItems: [{
        	xtype:'toolbar',
        	dock:'top',
        	displayInfo: true,
        	items:[
        	       '-',
        	       {
        	    	   xtype:'splitbutton',
        	    	   text: '管理学籍',
        	    	   iconCls:'Userhome',
        	    	   menu: Ext.create('Ext.menu.Menu', {
        	    	        items: [
								{id:'addUser',text: '增加学籍',iconCls:'Useradd', listeners: {click:function(){editRoll_info();}}},'-',
								{id:'editUser', text: '编辑学籍',disabled:true,iconCls:'Useredit',listeners: {click:function(){updateUser();}}},'-',
								{id:'delUser', text: '删除学籍',disabled:true,iconCls:'Userdelete',listeners: {click:function(){delUser();}}}]
        	    	    })
        	       },
                   {xtype: 'button', id:'importUser',text: '批量导入学籍',iconCls:'Databasecopy',
        	    	   listeners: {
        	    		   click:function(){
        	    			   
        	    		   }
        	    	   }},   
        	       {xtype: 'button', id:'exportUser', text: '导出 学籍',iconCls:'Pageexcel',
        	    	   listeners: {
        	    		   click:function(){
        	    			   exportUserToExcel();
        	    		   }
    	    	   }},'->',
    	    	   {id:'searchInput',xtype: 'combo',name: 'name',
    	    		   fieldLabel: '登录名(联想输入，自动检索)',labelAlign:'left',labelWidth:180,
    	    		   store:store, 
    	    		   hideTrigger:true,
    	    		   displayField: 'login_name',
    	               valueField: 'login_name',
    	               queryMode:"local",
    	               editable:true,
    	               triggerAction:"all"
    	           },
                   {id:'searchAll',xtype: 'button', text: '显示全部',iconCls:'Applicationformmagnify',listeners:{click:function(){store.load({params:{start:0,limit:15}}); }}}]
        },{
            xtype: 'pagingtoolbar',
            store: store,   // GridPanel使用相同的数据源
            dock: 'bottom',
            displayInfo: true,
            plugins: Ext.create('Ext.ux.ProgressBarPager'),
            emptyMsg: "没有记录" //没有数据时显示信息
        }]
    });
    //数据排序
    store.sort('create_time','asc');
    //加载数据  
    store.load({params:{start:0,limit:15}}); 
    // 表格配置结束
    
    //监听表格
    grid.getSelectionModel().on({
        selectionchange: function(sm, selections) {
            if (selections.length) {
            	Ext.getCmp('editUser').setDisabled(false);
            	Ext.getCmp('delUser').setDisabled(false);
            } else {
            	Ext.getCmp('editUser').setDisabled(true);
            	Ext.getCmp('delUser').setDisabled(true);
            }
        }
    });
    
    //表格右键菜单  
    var contextmenu = new Ext.menu.Menu({  
        id:'theContextMenu',  
        items:[{
            text:'增加学籍',
            iconCls:'Useradd',
            handler:function(){  
            	editRoll_info();  
            }  
        },{
            text:'编辑学籍',
            iconCls:'Useredit',
            handler:function(){  
            	updateUser();
            }  
        },{
            text:'删除学籍',
            iconCls:'Userdelete',
            handler:function(){  
            	delUser();
            }  
        },{
            text:'批量导入学籍',
            iconCls:'Databasecopy',
            handler:function(){  
            	uploadUser();
            }  
        },{
            text:'导出学籍',
            iconCls:'Pageexcel',
            handler:function(){  
            	exportUserToExcel();
            } 
        },{
			text : '刷新',
			iconCls: "Arrowrefreshsmall",
			handler : function(){
				store.reload();
			}  
		}]  
    });  
    
    grid.on("itemcontextmenu",function(view,record,item,index,e){  
        e.preventDefault();  
        contextmenu.showAt(e.getXY());  
    });
    
    grid.on("itemdblclick",function(grid, row){
    	updateUser(); 
    });
    
    var centerPanel = Ext.create("Ext.panel.Panel", {
        region: 'center',
        type:'vbox',
        defaults: {
            autoScroll: true,
          //  autoHeight: true
        },
        items: [grid]
    });
    
	// 整体架构容器
	Ext.create("Ext.container.Viewport", {
		layout : 'border',
		autoHeight: true,
		border: false,
		items : [centerPanel]
	});
});