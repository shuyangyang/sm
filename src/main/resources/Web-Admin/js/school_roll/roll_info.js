function editRoll_info(){
	var gradeClassStates = Ext.create('Ext.data.Store', {
		proxy:{  
            type:'ajax',  
            url:'/gradeTheClass/showGradeClass',
            reader:{  
                type:'json',
                totalProperty:'total',  
                root:'data',  
                idProperty:'id'  
            }  
        },  
	    fields: ['gradeClass', 'pvalue']
	});
	var sexStates = Ext.create('Ext.data.Store', {
	    fields: ['sex', 'name'],
	    data : [
	        {"sex":"1", "name":"男"},
	        {"sex":"0", "name":"女"}
	    ]
	});
	var studyingWayStates = Ext.create('Ext.data.Store', {
	    fields: ['studyingWay', 'name'],
	    data : [
	        {"studyingWay":"1", "name":"寄宿生"},
	        {"studyingWay":"0", "name":"走读生"}
	    ]
	});
	var trailingStates = Ext.create('Ext.data.Store', {
	    fields: ['trailing', 'name'],
	    data : [
	        {"trailing":"1", "name":"是"},
	        {"trailing":"0", "name":"否"}
	    ]
	});
	//floatingPopulationStates
	var floatingPopulationStates = Ext.create('Ext.data.Store', {
	    fields: ['floatingPopulation', 'name'],
	    data : [
	        {"floatingPopulation":"1", "name":"是"},
	        {"floatingPopulation":"0", "name":"否"}
	    ]
	});
	//Left-behindChildrenStates
	var LeftbehindChildrenStates = Ext.create('Ext.data.Store', {
	    fields: ['LeftbehindChildren', 'name'],
	    data : [
	        {"LeftbehindChildren":"1", "name":"是"},
	        {"LeftbehindChildren":"0", "name":"否"}
	    ]
	});
	//graduate
	var graduateStates = Ext.create('Ext.data.Store', {
	    fields: ['graduate', 'name'],
	    data : [
	        {"graduate":"1", "name":"未结业"},
	        {"graduate":"1", "name":"复读"},
	        {"graduate":"0", "name":"已结业"}
	    ]
	});
	var orphanStates = Ext.create('Ext.data.Store', {
	    fields: ['orphan', 'name'],
	    data : [
	        {"orphan":"1", "name":"是"},
	        {"orphan":"0", "name":"否"}
	    ]
	});
	var patchStates = Ext.create('Ext.data.Store', {
	    fields: ['patch', 'name'],
	    data : [
	        {"patch":"1", "name":"是"},
	        {"patch":"0", "name":"否"}
	    ]
	});
	var outsideStates = Ext.create('Ext.data.Store', {
	    fields: ['outside', 'name'],
	    data : [
	        {"outside":"1", "name":"无"},
	        {"outside":"2", "name":"归侨"},
	        {"outside":"3", "name":"华侨"},
	        {"outside":"4", "name":"侨眷"},
	        {"outside":"5", "name":"港澳"},
	        {"outside":"6", "name":"台胞"},
	        {"outside":"7", "name":"外籍华人"}
	    ]
	});
	var bestStudentStates = Ext.create('Ext.data.Store', {
	    fields: ['bestStudent', 'name'],
	    data : [
	        {"bestStudent":"1", "name":"是"},
	        {"bestStudent":"0", "name":"否"}
	    ]
	});
	//学前教育
	var preschoolEducationStates = Ext.create('Ext.data.Store', {
	    fields: ['preschoolEducation', 'name'],
	    data : [
	        {"preschoolEducation":"1", "name":"是"},
	        {"preschoolEducation":"0", "name":"否"}
	    ]
	});
	//是否申请资助
	var subsidizationStates = Ext.create('Ext.data.Store', {
	    fields: ['subsidization', 'name'],
	    data : [
	        {"subsidization":"1", "name":"是"},
	        {"subsidization":"0", "name":"否"}
	    ]
	});
	var onlyChildStates = Ext.create('Ext.data.Store', {
	    fields: ['onlyChild', 'name'],
	    data : [
	        {"onlyChild":"1", "name":"是"},
	        {"onlyChild":"0", "name":"否"}
	    ]
	});
	//是否监护人
	var isfamilyStates = Ext.create('Ext.data.Store', {
	    fields: ['isfamily', 'name'],
	    data : [
	        {"isfamily":"1", "name":"是"},
	        {"isfamily":"0", "name":"否"}
	    ]
	});
	var matriculationStates = Ext.create('Ext.data.Store', {
	    fields: ['matriculation', 'name'],
	    data : [
	        {"matriculation":"1", "name":"就近入学"},
	        {"matriculation":"0", "name":"统一招生考试"},
	        {"matriculation":"0", "name":"体育特招"},
	        {"matriculation":"0", "name":"艺术特招"},
	        {"matriculation":"0", "name":"其他"}
	    ]
	});
	var bloodStates = Ext.create('Ext.data.Store', {
	    fields: ['blood', 'name'],
	    data : [
	        {"blood":"A", "name":"A型"},
	        {"blood":"B", "name":"B型"},
	        {"blood":"AB", "name":"AB型"},
	        {"blood":"O", "name":"O型"}
	    ]
	});
	//familyties
	var familytiesStates = Ext.create('Ext.data.Store', {
	    fields: ['familyties', 'name'],
	    data : [
	        {"familyties":"0", "name":"父子"},
	        {"familyties":"1", "name":"父女"},
	        {"familyties":"2", "name":"母子"},
	        {"familyties":"3", "name":"母女"},
	        {"familyties":"4", "name":"其他"}
	    ]
	});
	var certificateStates = Ext.create('Ext.data.Store', {
	    fields: ['certificate', 'name'],
	    data : [
	        {"certificate":"1", "name":"身份证"},
	        {"certificate":"2", "name":"护照"},
	        {"certificate":"3", "name":"军官证"},
	        {"certificate":"4", "name":"驾照"},
	        {"certificate":"5", "name":"其他"}
	    ]
	});
	var healthyStates = Ext.create('Ext.data.Store', {
	    fields: ['healthy', 'name'],
	    data : [
	        {"healthy":"健康或良好", "name":"健康或良好"},
	        {"healthy":"一般或较弱", "name":"一般或较弱"},
	        {"healthy":"有慢性疾病", "name":"有慢性疾病"},
	        {"healthy":"有生理缺陷", "name":"有生理缺陷"},
	        {"healthy":"残疾", "name":"残疾"}
	    ]
	});
	var politicalStates = Ext.create('Ext.data.Store', {
	    fields: ['political', 'name'],
	    data : [
	        {"political":"0", "name":"中共党员"},
	        {"political":"1", "name":"中共预备党员"},
	        {"political":"2", "name":"共青团员"},
	        {"political":"3", "name":"群众"}
	    ]
	});
	var householdStates = Ext.create('Ext.data.Store', {
	    fields: ['household', 'name'],
	    data : [
	        {"household":"1", "name":"农业户口"},
	        {"household":"0", "name":"非农业户口"}
	    ]
	});
	var nationalityStates = Ext.create('Ext.data.Store', {
	    fields: ['nationality', 'name'],
	    data : [
				{"nationality":"中国China", name:"中国China"},
				{"nationality":"中国台湾China Taiwan", name:"中国台湾China Taiwan"},
				{"nationality":"香港Hong Kong Special Administrative Region of China", name:"香港Hong Kong Special Administrative Region of China"},
				{"nationality":"澳门Macao Special Administrative Region of China", name:"澳门Macao Special Administrative Region of China"},
				{"nationality":"阿鲁巴Aruba", name:"阿鲁巴Aruba"},
				{"nationality":"阿富汗伊斯兰国Afghanistan", name:"阿富汗伊斯兰国Afghanistan"},
				{"nationality":"安哥拉共和国Angola", name:"安哥拉共和国Angola"},
				{"nationality":"安圭拉Anguilla", name:"安圭拉Anguilla"},
				{"nationality":"阿尔巴尼亚共和国Albania", name:"阿尔巴尼亚共和国Albania"},
				{"nationality":"安道尔公国Andorra", name:"安道尔公国Andorra"},
				{"nationality":"荷属安的列斯Netherlands Antilles", name:"荷属安的列斯Netherlands Antilles"},
				{"nationality":"阿拉伯联合酋长国United Arab Emirates", name:"阿拉伯联合酋长国United Arab Emirates"},
				{"nationality":"阿根廷共和国Argentina", name:"阿根廷共和国Argentina"},
				{"nationality":"亚美尼亚共和国Armenia", name:"亚美尼亚共和国Armenia"},
				{"nationality":"美属萨摩亚American Samoa", name:"美属萨摩亚American Samoa"},
				{"nationality":"南极洲Antarctica", name:"南极洲Antarctica"},
				{"nationality":"法属南部领土French Southern Territories Gabon", name:"法属南部领土French Southern Territories Gabon"},
				{"nationality":"安提瓜和巴布达Antigua", name:"安提瓜和巴布达Antigua"},
				{"nationality":"澳大利亚联邦Australia", name:"澳大利亚联邦Australia"},
				{"nationality":"奥地利共和国Austria", name:"奥地利共和国Austria"},
				{"nationality":"阿塞拜疆共和国Azerbaijian", name:"阿塞拜疆共和国Azerbaijian"},
				{"nationality":"布隆迪共和国Burundi", name:"布隆迪共和国Burundi"},
				{"nationality":"比利时王国Belgium", name:"比利时王国Belgium"},
				{"nationality":"贝宁共和国Benin", name:"贝宁共和国Benin"},
				{"nationality":"布基纳法索Burkina Faso", name:"布基纳法索Burkina Faso"},
				{"nationality":"孟加拉人民共和国Bangladesh", name:"孟加拉人民共和国Bangladesh"},
				{"nationality":"保加利亚共和国Bulgaria", name:"保加利亚共和国Bulgaria"},
				{"nationality":"巴林国Bahrain", name:"巴林国Bahrain"},
				{"nationality":"巴哈马共和国Bahamas", name:"巴哈马共和国Bahamas"},
				{"nationality":"波斯尼亚和黑塞哥维那共和国Bosnia and Herzegovina", name:"波斯尼亚和黑塞哥维那共和国Bosnia and Herzegovina"},
				{"nationality":"白俄罗斯共和国Belarus", name:"白俄罗斯共和国Belarus"},
				{"nationality":"伯利兹Belize", name:"伯利兹Belize"},
				{"nationality":"百慕大群岛Bermuda", name:"百慕大群岛Bermuda"},
				{"nationality":"玻利维亚共和国Bolivia (Plurinational State of)", name:"玻利维亚共和国Bolivia (Plurinational State of)"},
				{"nationality":"巴西联邦共和国Brazil", name:"巴西联邦共和国Brazil"},
				{"nationality":"巴巴多斯Barbados", name:"巴巴多斯Barbados"},
				{"nationality":"文莱达鲁萨兰国Brunei Darussalam", name:"文莱达鲁萨兰国Brunei Darussalam"},
				{"nationality":"不丹王国Bhutan", name:"不丹王国Bhutan"},
				{"nationality":"布维岛Bouvet Island", name:"布维岛Bouvet Island"},
				{"nationality":"博茨瓦纳共和国Botswana", name:"博茨瓦纳共和国Botswana"},
				{"nationality":"中非共和国Central African Republic", name:"中非共和国Central African Republic"},
				{"nationality":"加拿大Canada", name:"加拿大Canada"},
				{"nationality":"科科斯（基林）群岛Coccs(Keeling) Islands", name:"科科斯（基林）群岛Coccs(Keeling) Islands"},
				{"nationality":"瑞士联邦Switzerland", name:"瑞士联邦Switzerland"},
				{"nationality":"智利共和国Chile", name:"智利共和国Chile"},
				{"nationality":"科特迪瓦共和国Côte d'Ivoire", name:"科特迪瓦共和国Côte d'Ivoire"},
				{"nationality":"喀麦隆共和国Cameroon", name:"喀麦隆共和国Cameroon"},
				{"nationality":"刚果共和国Congo", name:"刚果共和国Congo"},
				{"nationality":"库克群岛Cook Islands", name:"库克群岛Cook Islands"},
				{"nationality":"哥伦比亚共和国Colombia", name:"哥伦比亚共和国Colombia"},
				{"nationality":"科摩罗伊斯兰联邦共和国Comoros", name:"科摩罗伊斯兰联邦共和国Comoros"},
				{"nationality":"佛得角共和国Cape Verde", name:"佛得角共和国Cape Verde"},
				{"nationality":"哥斯达黎加共和国Costa Rica", name:"哥斯达黎加共和国Costa Rica"},
				{"nationality":"古巴共和国Cuba", name:"古巴共和国Cuba"},
				{"nationality":"圣诞岛Christmas Island", name:"圣诞岛Christmas Island"},
				{"nationality":"开曼群岛Cayman Islands", name:"开曼群岛Cayman Islands"},
				{"nationality":"塞浦路斯共和国Cyprus", name:"塞浦路斯共和国Cyprus"},
				{"nationality":"捷克共和国Czech Republic", name:"捷克共和国Czech Republic"},
				{"nationality":"德意志联邦共和国Germany", name:"德意志联邦共和国Germany"},
				{"nationality":"吉布提共和国Djibouti", name:"吉布提共和国Djibouti"},
				{"nationality":"多米尼克联邦Dominica", name:"多米尼克联邦Dominica"},
				{"nationality":"丹麦王国Denmark", name:"丹麦王国Denmark"},
				{"nationality":"多米尼加共和国Dominican Republic", name:"多米尼加共和国Dominican Republic"},
				{"nationality":"阿尔及利亚民主人民共和国Algeria", name:"阿尔及利亚民主人民共和国Algeria"},
				{"nationality":"厄瓜多尔共和国Ecuador", name:"厄瓜多尔共和国Ecuador"},
				{"nationality":"阿拉伯埃及共和国Egypt", name:"阿拉伯埃及共和国Egypt"},
				{"nationality":"厄立特里亚国Eritrea", name:"厄立特里亚国Eritrea"},
				{"nationality":"西撒哈拉Western Sahara", name:"西撒哈拉Western Sahara"},
				{"nationality":"西班牙Spain", name:"西班牙Spain"},
				{"nationality":"爱沙尼亚共和国Estonia", name:"爱沙尼亚共和国Estonia"},
				{"nationality":"埃塞俄比亚Ethiopia", name:"埃塞俄比亚Ethiopia"},
				{"nationality":"芬兰共和国Finland", name:"芬兰共和国Finland"},
				{"nationality":"斐济共和国Fiji", name:"斐济共和国Fiji"},
				{"nationality":"马尔维纳斯群岛Falkland Islands (Malvinas)", name:"马尔维纳斯群岛Falkland Islands (Malvinas)"},
				{"nationality":"法兰斯共和国France", name:"法兰斯共和国France"},
				{"nationality":"法罗群岛Faeroe Islands", name:"法罗群岛Faeroe Islands"},
				{"nationality":"密克罗尼西亚联邦Micronesia (Federated States of)", name:"密克罗尼西亚联邦Micronesia (Federated States of)"},
				{"nationality":"加蓬共和国Gabon", name:"加蓬共和国Gabon"},
				{"nationality":"大不列颠及北爱尔兰联合王国United Kingdom of Great Britain and Northern Ireland", name:"大不列颠及北爱尔兰联合王国United Kingdom of Great Britain and Northern Ireland"},
				{"nationality":"格鲁吉亚共和国Georgia", name:"格鲁吉亚共和国Georgia"},
				{"nationality":"加纳共和国Ghana", name:"加纳共和国Ghana"},
				{"nationality":"直布罗陀Gibraltar", name:"直布罗陀Gibraltar"},
				{"nationality":"几内亚共和国Guinea", name:"几内亚共和国Guinea"},
				{"nationality":"瓜德罗普Guadeloupe", name:"瓜德罗普Guadeloupe"},
				{"nationality":"冈比亚共和国Gambia", name:"冈比亚共和国Gambia"},
				{"nationality":"几内亚比绍Guinea-Bissau", name:"几内亚比绍Guinea-Bissau"},
				{"nationality":"赤道几内亚共和国Equatorial Guinea", name:"赤道几内亚共和国Equatorial Guinea"},
				{"nationality":"希腊共和国Greece", name:"希腊共和国Greece"},
				{"nationality":"格林纳达Grenada", name:"格林纳达Grenada"},
				{"nationality":"格陵兰Greenland", name:"格陵兰Greenland"},
				{"nationality":"危地马拉共和国Guatemala", name:"危地马拉共和国Guatemala"},
				{"nationality":"法属圭亚那French Guiana", name:"法属圭亚那French Guiana"},
				{"nationality":"关岛Guam", name:"关岛Guam"},
				{"nationality":"圭亚那合作共和国Guyana", name:"圭亚那合作共和国Guyana"},
				{"nationality":"赫德岛和麦克唐纳岛Heard Islands and McDonald Islands", name:"赫德岛和麦克唐纳岛Heard Islands and McDonald Islands"},
				{"nationality":"洪都拉斯共和国Honduras", name:"洪都拉斯共和国Honduras"},
				{"nationality":"克罗地亚共和国Croatia", name:"克罗地亚共和国Croatia"},
				{"nationality":"海地共和国Haiti", name:"海地共和国Haiti"},
				{"nationality":"匈牙利共和国Hungary", name:"匈牙利共和国Hungary"},
				{"nationality":"印度尼西亚共和国Indonesia", name:"印度尼西亚共和国Indonesia"},
				{"nationality":"印度共和国India", name:"印度共和国India"},
				{"nationality":"英属印度洋领土British Indian Ocean Territory", name:"英属印度洋领土British Indian Ocean Territory"},
				{"nationality":"爱尔兰Ireland", name:"爱尔兰Ireland"},
				{"nationality":"伊朗伊斯兰共和国Iran (Islamic Republic of)", name:"伊朗伊斯兰共和国Iran (Islamic Republic of)"},
				{"nationality":"伊拉克共和国Iraq", name:"伊拉克共和国Iraq"},
				{"nationality":"冰岛共和国Iceland", name:"冰岛共和国Iceland"},
				{"nationality":"以色列国Israel", name:"以色列国Israel"},
				{"nationality":"意大利共和国Italy", name:"意大利共和国Italy"},
				{"nationality":"牙买加Jamaica", name:"牙买加Jamaica"},
				{"nationality":"约旦哈西姆王国Jordan", name:"约旦哈西姆王国Jordan"},
				{"nationality":"日本国Japan", name:"日本国Japan"},
				{"nationality":"哈萨克斯坦共和国Kazakhstan", name:"哈萨克斯坦共和国Kazakhstan"},
				{"nationality":"肯尼亚共和国Kenya", name:"肯尼亚共和国Kenya"},
				{"nationality":"吉尔吉斯共和国Kyrgyzstan", name:"吉尔吉斯共和国Kyrgyzstan"},
				{"nationality":"柬埔寨王国Cambodia", name:"柬埔寨王国Cambodia"},
				{"nationality":"基里巴斯共和国Kiribati", name:"基里巴斯共和国Kiribati"},
				{"nationality":"圣基茨和尼维斯联邦Saint Kitts and Nevis", name:"圣基茨和尼维斯联邦Saint Kitts and Nevis"},
				{"nationality":"大韩民国Republic of Korea", name:"大韩民国Republic of Korea"},
				{"nationality":"科威特国Kuwait", name:"科威特国Kuwait"},
				{"nationality":"老挝人民民主共和国Lao People's Democratic Republic", name:"老挝人民民主共和国Lao People's Democratic Republic"},
				{"nationality":"黎巴嫩共和国Lebanon", name:"黎巴嫩共和国Lebanon"},
				{"nationality":"利比里亚共和国Liberia", name:"利比里亚共和国Liberia"},
				{"nationality":"大阿拉伯利比亚人民社会主义民众国Libyan Arab Jamahiriya", name:"大阿拉伯利比亚人民社会主义民众国Libyan Arab Jamahiriya"},
				{"nationality":"圣卢西亚Saint Lucia", name:"圣卢西亚Saint Lucia"},
				{"nationality":"列支敦士登公国Liechtenstein", name:"列支敦士登公国Liechtenstein"},
				{"nationality":"斯里兰卡民族社会主义共和国Sri Lanka", name:"斯里兰卡民族社会主义共和国Sri Lanka"},
				{"nationality":"莱索托王国Lesotho", name:"莱索托王国Lesotho"},
				{"nationality":"立陶宛共和国Lithuania", name:"立陶宛共和国Lithuania"},
				{"nationality":"卢森堡大公国Luxembourg", name:"卢森堡大公国Luxembourg"},
				{"nationality":"拉脱维亚共和国Latvia", name:"拉脱维亚共和国Latvia"},
				{"nationality":"摩洛哥王国Morocco", name:"摩洛哥王国Morocco"},
				{"nationality":"摩纳哥王国Monaco", name:"摩纳哥王国Monaco"},
				{"nationality":"摩尔多瓦共和国Republic of Moldova", name:"摩尔多瓦共和国Republic of Moldova"},
				{"nationality":"马达加斯加共和国Madagascar", name:"马达加斯加共和国Madagascar"},
				{"nationality":"马尔代夫共和国Maldives", name:"马尔代夫共和国Maldives"},
				{"nationality":"墨西哥合众国Mexico", name:"墨西哥合众国Mexico"},
				{"nationality":"玛绍尔群岛共和国Marshall Islands", name:"玛绍尔群岛共和国Marshall Islands"},
				{"nationality":"马其顿共和国The former Yugoslav Republic of Macedonia", name:"马其顿共和国The former Yugoslav Republic of Macedonia"},
				{"nationality":"马里共和国Mali", name:"马里共和国Mali"},
				{"nationality":"马耳他共和国Malta", name:"马耳他共和国Malta"},
				{"nationality":"缅甸联邦Myanmar", name:"缅甸联邦Myanmar"},
				{"nationality":"蒙古国Mongolia", name:"蒙古国Mongolia"},
				{"nationality":"北马里亚娜自由联邦Northern Mariana Islands", name:"北马里亚娜自由联邦Northern Mariana Islands"},
				{"nationality":"莫桑比克共和国Mozambique", name:"莫桑比克共和国Mozambique"},
				{"nationality":"毛里塔尼亚伊斯兰共和国Mauritania", name:"毛里塔尼亚伊斯兰共和国Mauritania"},
				{"nationality":"蒙特塞拉特Montserrat", name:"蒙特塞拉特Montserrat"},
				{"nationality":"马提尼克Martinique", name:"马提尼克Martinique"},
				{"nationality":"毛里求斯共和国Mauritius", name:"毛里求斯共和国Mauritius"},
				{"nationality":"马拉维共和国Malawi", name:"马拉维共和国Malawi"},
				{"nationality":"马来西亚Malaysia", name:"马来西亚Malaysia"},
				{"nationality":"马约特Mayotte", name:"马约特Mayotte"},
				{"nationality":"纳米比亚共和国Namibia", name:"纳米比亚共和国Namibia"},
				{"nationality":"新喀里多尼亚New Caledonia", name:"新喀里多尼亚New Caledonia"},
				{"nationality":"尼日尔共和国Niger", name:"尼日尔共和国Niger"},
				{"nationality":"诺福克岛Norfolk Island", name:"诺福克岛Norfolk Island"},
				{"nationality":"尼日利亚联邦共和国Nigeria", name:"尼日利亚联邦共和国Nigeria"},
				{"nationality":"尼加拉瓜共和国Nicaragua", name:"尼加拉瓜共和国Nicaragua"},
				{"nationality":"纽埃Niue", name:"纽埃Niue"},
				{"nationality":"荷兰王国Netherlands", name:"荷兰王国Netherlands"},
				{"nationality":"挪威王国Norway", name:"挪威王国Norway"},
				{"nationality":"尼泊尔王国Nepal", name:"尼泊尔王国Nepal"},
				{"nationality":"瑙鲁共和国Nauru", name:"瑙鲁共和国Nauru"},
				{"nationality":"新西兰New Zealand", name:"新西兰New Zealand"},
				{"nationality":"阿曼苏丹国Oman", name:"阿曼苏丹国Oman"},
				{"nationality":"巴基斯坦伊斯兰共和国Pakistan", name:"巴基斯坦伊斯兰共和国Pakistan"},
				{"nationality":"巴拿马共和国Panama", name:"巴拿马共和国Panama"},
				{"nationality":"皮特凯恩岛Pitcairn", name:"皮特凯恩岛Pitcairn"},
				{"nationality":"秘鲁共和国Peru", name:"秘鲁共和国Peru"},
				{"nationality":"菲律宾共和国Philippines", name:"菲律宾共和国Philippines"},
				{"nationality":"贝劳共和国Palau", name:"贝劳共和国Palau"},
				{"nationality":"巴布亚新几内亚独立国Papua New Guinea", name:"巴布亚新几内亚独立国Papua New Guinea"},
				{"nationality":"波兰共和国Poland", name:"波兰共和国Poland"},
				{"nationality":"波多黎各自由联邦Puerto Rico", name:"波多黎各自由联邦Puerto Rico"},
				{"nationality":"朝鲜民主主义人民共和国Democratic People's Republic of Korea", name:"朝鲜民主主义人民共和国Democratic People's Republic of Korea"},
				{"nationality":"葡萄牙共和国Portugal", name:"葡萄牙共和国Portugal"},
				{"nationality":"巴拉圭共和国Paraguay", name:"巴拉圭共和国Paraguay"},
				{"nationality":"巴勒斯坦国The State of Palestine", name:"巴勒斯坦国The State of Palestine"},
				{"nationality":"法属伯利尼西亚French Polynesia", name:"法属伯利尼西亚French Polynesia"},
				{"nationality":"卡塔尔国Qatar", name:"卡塔尔国Qatar"},
				{"nationality":"留尼汪Réunion", name:"留尼汪Réunion"},
				{"nationality":"罗马尼亚Romania", name:"罗马尼亚Romania"},
				{"nationality":"俄罗斯联邦Russian Federation", name:"俄罗斯联邦Russian Federation"},
				{"nationality":"卢旺达共和国Rwanda", name:"卢旺达共和国Rwanda"},
				{"nationality":"沙特阿拉伯王国Saudi Arabia", name:"沙特阿拉伯王国Saudi Arabia"},
				{"nationality":"塞尔维亚和黑山Serbia and Montenegro", name:"塞尔维亚和黑山Serbia and Montenegro"},
				{"nationality":"苏丹共和国Sudan", name:"苏丹共和国Sudan"},
				{"nationality":"塞内加尔共和国Senegal", name:"塞内加尔共和国Senegal"},
				{"nationality":"新加坡共和国Singapore", name:"新加坡共和国Singapore"},
				{"nationality":"南乔治岛和南桑德韦奇岛South Georgia and South Sandwich Islands", name:"南乔治岛和南桑德韦奇岛South Georgia and South Sandwich Islands"},
				{"nationality":"圣赫勒拿Saint Helena", name:"圣赫勒拿Saint Helena"},
				{"nationality":"斯瓦尔巴群岛Svalbard and Jan Mayen Islands", name:"斯瓦尔巴群岛Svalbard and Jan Mayen Islands"},
				{"nationality":"所罗门群岛Solomon Islands", name:"所罗门群岛Solomon Islands"},
				{"nationality":"塞拉利昂共和国Sierra Leone", name:"塞拉利昂共和国Sierra Leone"},
				{"nationality":"萨尔多瓦共和国El Salvador", name:"萨尔多瓦共和国El Salvador"},
				{"nationality":"圣马力诺共和国San Marino", name:"圣马力诺共和国San Marino"},
				{"nationality":"索马里共和国Somalia", name:"索马里共和国Somalia"},
				{"nationality":"圣皮埃尔和密克隆Saint Pierre and Miquelon", name:"圣皮埃尔和密克隆Saint Pierre and Miquelon"},
				{"nationality":"圣多美和普林西比民主共和国Sao Tome and Principe", name:"圣多美和普林西比民主共和国Sao Tome and Principe"},
				{"nationality":"苏里南共和国Suriname", name:"苏里南共和国Suriname"},
				{"nationality":"斯洛伐克共和国Slovakia", name:"斯洛伐克共和国Slovakia"},
				{"nationality":"斯洛文尼亚共和国Slovenia", name:"斯洛文尼亚共和国Slovenia"},
				{"nationality":"瑞典王国Sweden", name:"瑞典王国Sweden"},
				{"nationality":"斯威士兰王国Swaziland", name:"斯威士兰王国Swaziland"},
				{"nationality":"塞舌尔共和国Seychelles", name:"塞舌尔共和国Seychelles"},
				{"nationality":"阿拉伯叙利亚共和国Syrian Arab Republic", name:"阿拉伯叙利亚共和国Syrian Arab Republic"},
				{"nationality":"特克斯和凯科斯群岛Turks and Caicos Islands", name:"特克斯和凯科斯群岛Turks and Caicos Islands"},
				{"nationality":"乍得共和国Chad", name:"乍得共和国Chad"},
				{"nationality":"多哥共和国Togo", name:"多哥共和国Togo"},
				{"nationality":"泰王国Thailand", name:"泰王国Thailand"},
				{"nationality":"塔吉克斯坦共和国Tajikistan", name:"塔吉克斯坦共和国Tajikistan"},
				{"nationality":"托克劳Tokelau", name:"托克劳Tokelau"},
				{"nationality":"土库曼斯坦Turkmenistan", name:"土库曼斯坦Turkmenistan"},
				{"nationality":"东帝汶Timor-Leste", name:"东帝汶Timor-Leste"},
				{"nationality":"汤加王国Tonga", name:"汤加王国Tonga"},
				{"nationality":"特立尼达和多巴哥共和国Trinidad and Tobago", name:"特立尼达和多巴哥共和国Trinidad and Tobago"},
				{"nationality":"突尼斯共和国Tunisia", name:"突尼斯共和国Tunisia"},
				{"nationality":"土耳其共和国Turkey", name:"土耳其共和国Turkey"},
				{"nationality":"图瓦卢Tuvalu", name:"图瓦卢Tuvalu"},
				{"nationality":"坦桑尼亚联合共和国United Republic of Tanzania", name:"坦桑尼亚联合共和国United Republic of Tanzania"},
				{"nationality":"乌干达共和国Uganda", name:"乌干达共和国Uganda"},
				{"nationality":"乌克兰Ukraine", name:"乌克兰Ukraine"},
				{"nationality":"美属太平洋各群岛U.S. Trust Territory of the Pacific Islands", name:"美属太平洋各群岛U.S. Trust Territory of the Pacific Islands"},
				{"nationality":"乌拉圭东岸共和国Uruguay", name:"乌拉圭东岸共和国Uruguay"},
				{"nationality":"美利坚合众国United States of America", name:"美利坚合众国United States of America"},
				{"nationality":"乌兹别克斯坦共和国Uzbekistan", name:"乌兹别克斯坦共和国Uzbekistan"},
				{"nationality":"梵蒂冈城国Holy See", name:"梵蒂冈城国Holy See"},
				{"nationality":"圣文森特和格林纳丁斯Saint Vincent and the Grenadines", name:"圣文森特和格林纳丁斯Saint Vincent and the Grenadines"},
				{"nationality":"委内瑞拉共和国Venezuela (Bolivarian Republic of)", name:"委内瑞拉共和国Venezuela (Bolivarian Republic of)"},
				{"nationality":"英属维尔京群岛British Virgin Islands", name:"英属维尔京群岛British Virgin Islands"},
				{"nationality":"美属维尔京群岛United States Virgin Islands", name:"美属维尔京群岛United States Virgin Islands"},
				{"nationality":"越南社会主义共和国Viet Nam", name:"越南社会主义共和国Viet Nam"},
				{"nationality":"瓦努阿图共和国Vanuatu", name:"瓦努阿图共和国Vanuatu"},
				{"nationality":"瓦利斯和富图纳群岛Wallis and Futuna Islands", name:"瓦利斯和富图纳群岛Wallis and Futuna Islands"},
				{"nationality":"西萨摩亚独立国Samoa", name:"西萨摩亚独立国Samoa"},
				{"nationality":"也门共和国Yemen", name:"也门共和国Yemen"},
				{"nationality":"南斯拉夫联盟共和国Federal Republic of Yugoslavia", name:"南斯拉夫联盟共和国Federal Republic of Yugoslavia"},
				{"nationality":"南非共和国South Africa", name:"南非共和国South Africa"},
				{"nationality":"扎伊尔共和国Democratic Republic of the Congo", name:"扎伊尔共和国Democratic Republic of the Congo"},
				{"nationality":"赞比亚共和国Zambia", name:"赞比亚共和国Zambia"},
				{"nationality":"津巴布韦共和国Zimbabwe", name:"津巴布韦共和国Zimbabwe"}
	    ]
	});
	var nationStates = Ext.create('Ext.data.Store', {
	    fields: ['nation', 'name'],
	    data : [
	        {"nation":"汉族", "name":"汉族"},
	        {"nation":"壮族", "name":"壮族"},
	        {"nation":"满族", "name":"满族"},
	        {"nation":"回族", "name":"回族"},
	        {"nation":"苗族", "name":"苗族"},
	        {"nation":"维吾尔族", "name":"维吾尔族"},
	        {"nation":"维吾尔族", "name":"土家族"},
	        {"nation":"彝族", "name":"彝族"},
	        {"nation":"蒙古族", "name":"蒙古族"},
	        {"nation":"藏族", "name":"藏族"},
	        {"nation":"布依族", "name":"布依族"},
	        {"nation":"侗族", "name":"侗族"},
	        {"nation":"瑶族", "name":"瑶族"},
	        {"nation":"朝鲜族", "name":"朝鲜族"},
	        {"nation":"白族", "name":"白族"},
	        {"nation":"哈尼族", "name":"哈尼族"},
	        {"nation":"哈萨克族", "name":"哈萨克族"},
	        {"nation":"黎族", "name":"黎族"},
	        {"nation":"傣族", "name":"傣族"},
	        {"nation":"畲族", "name":"畲族"},
	        {"nation":"傈僳族", "name":"傈僳族"},
	        {"nation":"仡佬族", "name":"仡佬族"},
	        {"nation":"东乡族", "name":"东乡族"},
	        {"nation":"高山族", "name":"高山族"},
	        {"nation":"拉祜族", "name":"拉祜族"},
	        {"nation":"水族", "name":"水族"},
	        {"nation":"佤族", "name":"佤族"},
	        {"nation":"纳西族", "name":"纳西族"},
	        {"nation":"羌族", "name":"羌族"},
	        {"nation":"土族", "name":"土族"},
	        {"nation":"仫佬族", "name":"仫佬族"},
	        {"nation":"锡伯族", "name":"锡伯族"},
	        {"nation":"柯尔克孜族", "name":"柯尔克孜族"},
	        {"nation":"达斡尔族", "name":"达斡尔族"},
	        {"nation":"景颇族", "name":"景颇族"},
	        {"nation":"毛南族", "name":"毛南族"},
	        {"nation":"撒拉族", "name":"撒拉族"},
	        {"nation":"布朗族", "name":"布朗族"},
	        {"nation":"塔吉克族", "name":"塔吉克族"},
	        {"nation":"阿昌族", "name":"阿昌族"},
	        {"nation":"普米族", "name":"普米族"},
	        {"nation":"鄂温克族", "name":"鄂温克族"},
	        {"nation":"怒族", "name":"怒族"},
	        {"nation":"京族", "name":"京族"},
	        {"nation":"基诺族", "name":"基诺族"},
	        {"nation":"德昂族", "name":"德昂族"},
	        {"nation":"保安族", "name":"保安族"},
	        {"nation":"俄罗斯族", "name":"俄罗斯族"},
	        {"nation":"裕固族", "name":"裕固族"},
	        {"nation":"乌兹别克族", "name":"乌兹别克族"},
	        {"nation":"门巴族", "name":"门巴族"},
	        {"nation":"鄂伦春族", "name":"鄂伦春族"},
	        {"nation":"独龙族", "name":"独龙族"},
	        {"nation":"塔塔尔族", "name":"塔塔尔族"},
	        {"nation":"赫哲族", "name":"赫哲族"},
	        {"nation":"珞巴族", "name":"珞巴族"}
	    ]
	});
	gradeClassStates.load();
	var required = '<span style="color:red;font-weight:bold" data-qtip="这是一个必填项">*</span>';
	var form = Ext.create('Ext.form.Panel', {
		 border : false,
		 autoHeight : true,
		 autoScroll:true,
		 forceFit:true, //自动填满表格  
		 region: 'center', //框架中显示位置，单独运行可去掉此段
	     labelAlign : "right",
	     layout: 'border',
	     defaults: {  
	         split: false,                 //是否有分割线  
	         collapsible: false,           //是否可以折叠
	         border:false
	     },  
	     items: [{
	    	 region:'center',
	    	 split: false,
	    	 width:'84%',
	    	 border:false,
	    	 autoScroll:true,
	    	 items:[{
	    		 xtype: 'fieldset',
	             title: '学生学籍基本信息',
	             collapsible: true,
	             width:'99%',
	             margin:'0 0 10px 5px',
	             defaults: {
	                 anchor: '100%',
	                 layout: {
	                     type: 'hbox',
	                     defaultMargins: {top: 10, right: 0, bottom: 0, left: 0}
	                 }
	             },
	    		 items:[{
		    		 	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                fieldLabel: '学籍辅号',
			                emptyText:'系统自动生成，不需填写',
			                columnWidth: 0.33,
			                readOnly:true,
			                name: 'auxiliary_no'
			            },{
			                xtype:'textfield',
			                labelAlign:'right',
			                fieldLabel: '学号',
			                emptyText:'系统自动生成，不需填写',
			                readOnly:true,
			                columnWidth: 0.33,
			                name: 'student_no'
			            },{
			            	xtype:'combo',
			                store: sexStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'sex',
			                editable:false,
			                labelAlign:'right',
			                fieldLabel: '性别',
			                emptyText:'选择性别',
			                allowBlank:false,
			                afterLabelTextTpl: required,
			                columnWidth: 0.33,
			                name: 'sex'
			            }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                fieldLabel: '姓名',
			                emptyText:'输入姓名',
			                allowBlank:false,
			                maxLength:50,
			                maxLengthText:'姓名最大50个字符',
			                afterLabelTextTpl: required,
			                columnWidth: 0.33,
			                name: 'student_name'
			            },{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                regex: /^[A-Z]+\s{1}[A-z]+(\s{1}[A-Z]+)*\s?$/,
			                regexText: "姓名拼音须以英文大写拼写，以空格分隔",
			                fieldLabel: '姓名拼音',
			                emptyText:'如李四的姓名拼音：LI SI',
			                maxLength:50,
			                maxLengthText:'姓名拼音最大50个字符',
			                afterLabelTextTpl: required,
			                allowBlank:false,
			                name: 'student_name_py'
			            },{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '曾用名',
			                maxLength:50,
			                maxLengthText:'曾用名最大50个字符',
			                emptyText:'曾用名可选填',
			                name: 'former_name'
			            }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: bloodStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'blood',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                editable:false,
			                fieldLabel: '血型',
			                emptyText:'选择血型',
			                name: 'blood'
				        },{
				        	xtype:'datefield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '入学年月',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择入学年月',
			                format:'Y-m-d',
			                maxValue: new Date(),
			                name: 'dates_attended'
				        },{
				        	xtype:'combo',
			                store: nationStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'nation',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                emptyText:'选择民族',
			                editable:false,
			                fieldLabel: '民族',
			                name: 'nation'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: gradeClassStates,
			                queryMode: 'local',
			                displayField: 'gradeClass',
			                valueField: 'pvalue',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '班级',
			                editable:false,
			                emptyText:'请选择班级',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                name: 'grade_class'
				        },{
				        	xtype:'combo',
			                store: healthyStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'healthy',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '健康状况',
			                editable:false,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                emptyText:'选择健康状况',
			                name: 'healthy_status'
				        },{
				        	xtype:'datefield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '出生日期',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择出生年月',
			                format:'Y-m-d',
			                maxValue: new Date(),
			                name: 'dateOfBirth'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                regex: /[1-9]\d{5}(?!\d)/,
			                regexText: "中国邮政编码为6位数字",
			                fieldLabel: '邮政编码',
			                emptyText:'输入邮政编码',
			                name: 'zip_code'
				        },{
				        	xtype:'combo',
			                store: householdStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'household',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '户口性质',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择户口性质',
			                name: 'account'
				        },{
				        	xtype:'combo',
			                store: nationalityStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'nationality',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择国籍/地区',
			                fieldLabel: '国籍/地区',
			                name: 'nationality'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: certificateStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'certificate',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '证件类型',
			                editable:false,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                emptyText:'选择证件类型',
			                name: 'id_type'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '证件号码',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                maxLength:18,
			                maxLengthText:'证件号码最大18个字符',
			                regex: /^[A-Za-z0-9]+$/,
			                regexText: "证件号码只能是数字和字母组成",
			                emptyText:'填写证件号码',
			                name: 'id_number'
				        },{
				        	xtype:'combo',
			                store: studyingWayStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'studyingWay',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                emptyText:'选择就读方式',
			                editable:false,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '就读方式',
			                name: 'studying_way'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: matriculationStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'matriculation',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择入学方式',
			                fieldLabel: '入学方式',
			                name: 'matriculation'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                maxLength:10,
			                maxLengthText:'户口所在地最大10个字符',
			                fieldLabel: '户口所在地',
			                emptyText:'填写户口所在地',
			                name: 'domicile_place'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '出生地',
			                emptyText:'填写出生地',
			                maxLength:25,
			                maxLengthText:'出生地最大25个字符',
			                name: 'birth_place'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                emptyText:'填写籍贯',
			                fieldLabel: '籍贯',
			                maxLength:2,
			                maxLengthText:'籍贯最大2个字符，如皖、沪等',
			                name: 'native_place'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '来源学校',
			                emptyText:'填写来源学校',
			                maxLength:30,
			                maxLengthText:'来源学校最大30个字符',
			                name: 'com_from_school'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '来源地区',
			                emptyText:'填写来源地区',
			                maxLength:30,
			                maxLengthText:'来源地区最大30个字符',
			                name: 'com_from_prefecture'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'datefield',
			                format:'Y-m-d',
			                maxValue: new Date(),
			                labelAlign:'right',
			                columnWidth: 0.33,
			                editable:false,
			                fieldLabel: '来源学校结束学业年月',
			                emptyText:'选择来源学校结束学业年月',
			                name: 'fgraduate_date'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '联系电话',
			                emptyText:'填写联系电话',
			                regex: /^\d+$/,
			                regexText: "电话号码只能为数字",
			                name: 'phone'
				        },{
				        	xtype:'combo',
			                store: trailingStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'trailing',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否进城务工人员随迁子女',
			                emptyText:'选择是否进城务工人员随迁子女',
			                name: 'traling_status'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: bestStudentStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'bestStudent',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否烈士或优抚子女',
			                emptyText:'选择是否烈士或优抚子女',
			                name: 'best_student_status'
				        },{
				        	xtype:'combo',
			                labelAlign:'right',
			                store: outsideStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'outside',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '港澳台侨外',
			                emptyText:'选择港澳台侨外',
			                name: 'outside_status'
				        },{
				        	xtype:'combo',
			                store: onlyChildStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'onlyChild',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否独生子女',
			                emptyText:'选择是否独生子女',
			                name: 'onlychild_status'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '0 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: patchStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'patch',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否享受一补',
			                emptyText:'选择是否享受一补',
			                name: 'enjoy_fill'
				        },{
				        	xtype:'combo',
				        	store: orphanStates,
				            queryMode: 'local',
				            displayField: 'name',
				            valueField: 'orphan',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否孤儿',
			                emptyText:'选择是否孤儿',
			                name: 'orphan_status'
				        },{
				        	xtype:'combo',
			                store: LeftbehindChildrenStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'LeftbehindChildren',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否留守儿童',
			                emptyText:'选择是否留守儿童',
			                name: 'leftBehindChild_status'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: preschoolEducationStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'preschoolEducation',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否受过学前教育',
			                emptyText:'选择是否受过学前教育',
			                name: 'prescholl_edu_status'
				        },{
				        	xtype:'combo',
			                store: subsidizationStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'subsidization',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否需要申请资助',
			                emptyText:'选择是否需要申请资助',
			                name: 'subsidization'
				        },{
				        	xtype:'combo',
			                store: floatingPopulationStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'floatingPopulation',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否流动人口',
			                emptyText:'是否流动人口',
			                name: 'floating_population'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'datefield',
			                format:'Y-m-d',
			                maxValue: new Date(),
			                labelAlign:'right',
			                columnWidth: 0.33,
			                editable:false,
			                fieldLabel: '结束学业年月',
			                emptyText:'选择结束学业年月',
			                name: 'graduate_date'
				        },{
				        	xtype:'combo',
				        	store: graduateStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'graduate',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                editable:false,
			                fieldLabel: '结束学业状态',
			                emptyText:'选择结束学业状态',
			                name: 'graduateResultCode'
				        },{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '毕业学校名称',
			                emptyText:'填写毕业学校名称',
			                maxLength:30,
			                maxLengthText:'毕业学校名称最大30个字符',
			                name: 'graduating_school'
				        },]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.5,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '通信地址',
			                emptyText:'填写可以联系到学生的地址，具体到村庄、街区门牌号',
			                maxLength:80,
			                maxLengthText:'通信地址最大80个字符',
			                name: 'mailing_address'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.5,
			                fieldLabel: '结束学业原因',
			                emptyText:'填写结束学业原因：毕业结业，提前转学结业等',
			                maxLength:20,
			                maxLengthText:'结束学业原因最大20个字符',
			                name: 'graduate_caus'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 20 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.5,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '现住址',
			                emptyText:'填写学生现在居住的地址，具体到村庄、街区门牌号',
			                maxLength:80,
			                maxLengthText:'现住址最大80个字符',
			                name: 'present_address'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.5,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '家庭住址',
			                emptyText:'填写学生家庭的住址：具体到村庄、街区门牌号',
			                maxLength:80,
			                maxLengthText:'家庭住址最大80个字符',
			                name: 'home_add'
				        }]
			        }]
	    	 },{
	        	    xtype: 'fieldset',
	                title: '学生家庭成员或监护人信息（一）',
	                collapsible: true,
	                width:'99%',
	                margin:'0 0 10px 5px',
	                defaults: {
	                    anchor: '100%',
	                    layout: {
	                        type: 'hbox',
	                        defaultMargins: {top: 10, right: 0, bottom: 0, left: 0}
	                    }
	                },
		            items: [{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: familytiesStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'familyties',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '关系',
			                editable:false,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                emptyText:'选择与被监护人的关系',
			                name: 'familyties'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '姓名',
			                emptyText:'填写监护人的姓名',
			                maxLength:50,
			                maxLengthText:'监护人的姓名最大50个字符',
			                name: 'nameOfGuardian'
				        },{
				        	xtype:'combo',
			                store: isfamilyStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'isfamily',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否监护人',
			                emptyText:'选择是否监护人',
			                name: 'wtguardian'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '移动电话',
			                emptyText:'填写监护人的联系电话',
			                regex: /^\d+$/,
			                regexText: "电话号码只能为数字",
			                name: 'guardian_telphone'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '职务',
			                emptyText:'填写监护人的职务',
			                maxLength:20,
			                maxLengthText:'监护人的职务最大20个字符',
			                name: 'guardian_duty'
				        },{
				        	xtype:'combo',
			                store: certificateStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'certificate',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '证件类型',
			                emptyText:'选择监护人的证件类型',
			                name: 'guardian_id_type'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '证件号码',
			                emptyText:'填写监护人的证件号码',
			                maxLength:18,
			                maxLengthText:'证件号码最大18个字符',
			                regex: /^[A-Za-z0-9]+$/,
			                regexText: "证件号码只能是数字和字母组成",
			                name: 'guardian_id_no'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '现住址',
			                emptyText:'填写监护人的现住址',
			                maxLength:80,
			                maxLengthText:'监护人的现住址最大80个字符',
			                name: 'guardian_add'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '户口所在地',
			                emptyText:'填写监护人户口所在地',
			                maxLength:80,
			                maxLengthText:'户口所在地最大80个字符',
			                name: 'guardian_domicile_place'
				        }]
			        }]
		        },{
	        	    xtype: 'fieldset',
	                title: '学生家庭成员或监护人信息（二）',
	                collapsible: true,
	                width:'99%',
	                margin:'0 0 10px 5px',
	                defaults: {
	                    anchor: '100%',
	                    layout: {
	                        type: 'hbox',
	                        defaultMargins: {top: 10, right: 0, bottom: 0, left: 0}
	                    }
	                },
	                items: [{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'combo',
			                store: familytiesStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'familyties',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                fieldLabel: '关系',
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                emptyText:'选择与被监护人的关系',
			                name: 'family_ties2'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '姓名',
			                emptyText:'填写监护人的姓名',
			                maxLength:50,
			                maxLengthText:'监护人的姓名最大50个字符',
			                name: 'name_of_guardian2'
				        },{
				        	xtype:'combo',
			                store: isfamilyStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'isfamily',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '是否监护人',
			                emptyText:'选择是否监护人',
			                name: 'wtguardian2'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 0 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '移动电话',
			                emptyText:'填写监护人的联系电话',
			                regex: /^\d+$/,
			                regexText: "电话号码只能为数字",
			                name: 'guardian_telphone2'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '职务',
			                emptyText:'填写监护人的职务',
			                maxLength:20,
			                maxLengthText:'监护人的职务最大20个字符',
			                name: 'guardianduty2'
				        },{
				        	xtype:'combo',
			                store: certificateStates,
			                queryMode: 'local',
			                displayField: 'name',
			                valueField: 'certificate',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                editable:false,
			                fieldLabel: '证件类型',
			                emptyText:'选择监护人的证件类型',
			                name: 'guardian_id_type2'
				        }]
			        },{
			        	anchor : '100%',
			    	 	border:false,
			            layout : {  
			                type : 'column'
			            },
			            defaults:{
			            	labelSeparator:'：',
			            	padding : '10 10 10 0'
			            },
			            items: [{
			                xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '证件号码',
			                emptyText:'填写监护人的证件号码',
			                maxLength:18,
			                maxLengthText:'证件号码最大18个字符',
			                regex: /^[A-Za-z0-9]+$/,
			                regexText: "证件号码只能是数字和字母组成",
			                name: 'guardian_id_no2'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '现住址',
			                emptyText:'填写监护人的现住址',
			                maxLength:80,
			                maxLengthText:'监护人的现住址最大80个字符',
			                name: 'guardian_add2'
				        },{
				        	xtype:'textfield',
			                labelAlign:'right',
			                columnWidth: 0.33,
			                afterLabelTextTpl: required,//必填项*号
			                allowBlank:false,
			                fieldLabel: '户口所在地',
			                emptyText:'填写监护人户口所在地',
			                maxLength:10,
			                maxLengthText:'监护人户口所在地最大10个字符',
			                name: 'guardian_domicile_place2'
				        }]
			        }]
		        }]
	     },{
	    	 region:'east',
	    	 split: false, 
	    	 width:'16%',
	    	 height:200,
	    	 border:false,
	    	 autoScroll:true,
	    	 items:[{
		            items: [{
		            	xtype: 'panel',
		            	height:200,
		            	bodyStyle: {
		            	    background: '#dfeaf2',
		            	    padding: '5px 20px 5px 20px'
		            	},
		            	border:false,
			            items:[{
			            	xtype: 'box', //或者xtype: 'component',  
			                width: 130, //图片宽度  
			                height: 150, //图片高度 
			                id:'myphoto',
			                autoEl: {  
			                    tag: 'img',    //指定为img标签  
			                    cls:'studentPhoto',//动态更新只需要获取到刚才建立的box的dom的src  myphoto.getEl().dom.src = newSrc  
			                    src: '/image/man.png'    //指定url路径  
			                }
			            },{
			            	xtype: 'button',
			            	width:150,
			            	margin:'10px 0px 0px -10px',
			            	iconCls:'Camera',
			            	text:'编辑照片',//photo
			            	handler:function(){
			            		var photoPanel = Ext.create('Ext.panel.Panel', {
			            		    width: 650,
			            			height:500,
			            		    html: '<iframe frameborder="no" border=0 height="100%" width="100%" src="/Web-Admin/Jcrop/jcrop.html" scrolling="auto"></iframe>'
			            		});
			            		// 表格配置结束
			            		var photoWin = Ext.create('Ext.window.Window', {
			            			layout:'fit',
			            			width: 650,
			            			height:500,
			            			autoShow:true,
			            			maximized: true,
			            			closeAction:'destroy',
			            			modal: true,
			            			plain: true,
			            			title: "上传编辑照片",
			            			iconCls: "Vcard",
			            			listeners:{'close':{fn:makesure}},
			            			items: [photoPanel]
			            		});
			            		function makesure(){
			            			Ext.Msg.alert('提示', '图片编辑成功，请不要忘记填写完学籍信息保存');
			            			var myphotoSrc = window.localStorage? localStorage.getItem("roll_myphoto"): Cookie.read("roll_myphoto");
			            			Ext.getCmp("myphoto").getEl().dom.src = myphotoSrc;
			            		}  
			            		photoWin.show();
			            	}
			            }]
		            }]
		        },{
		        	items: [{
		            	xtype: 'panel',
		            	bodyStyle: {
		            	    background: '#dfeaf2',
		            	    margin:'0 10 10 10'
		            	},
		            	border:false,
		            	html:'<h3>照片采集注意事项:</h3><p>1、各校要认真对照照片采集要求严格执行；</p><p>2、由于换底色（抠图）比较复杂，各校可准备淡蓝色的布作为背景拍照，拍照时为方便辨认可按采集表的顺序拍照；</p><p>3、为防止处理相处时出错，采集好相片后复制两份，一份作为备份，另一份编辑；</p><p>4、因批处理“批量裁剪”需手工选择，时间较长，所以“批量裁剪”和“缩放尺寸”可分两步执行，即先执行“批量裁剪”完成后，再执行“缩放尺寸”，以防混淆；</p><p>5、注意“缩放尺寸”边长（横边）应为260像素，不是358像素；</p><p>6、数码照片要求为jpg格式。照片文件大小应小于60K。</p><p>'
		            }]
		        }]
	     }],
			buttons: [
			{
				text: '保存学籍',
				iconCls:'Pagesave',
				handler: function() {
					if(form.form.isValid()) {
						form.form.submit({
							url: '/student/saveStudent',
							method: 'POST',
							success: function(form, action) {
								var respText = Ext.JSON.decode(form.responseText);
								if(respText.code != 'OK') {
									Ext.Msg.alert('保存失败', respText.message + "（" + respText.code + "）");
								}else{
									Ext.Msg.alert('系统提示', '学籍信息保存成功！');
									win.hide();
								}
								
							},
							failure: function(form, action) {
								switch (action.failureType) {
									case Ext.form.Action.CLIENT_INVALID:
										Ext.Msg.alert('保存失败', '数据字段格式错误！');
										break;
									case Ext.form.Action.CONNECT_FAILURE:
										Ext.Msg.alert('保存失败', '网络连接异常！');
										break;
									case Ext.form.Action.SERVER_INVALID:
									   Ext.Msg.alert('保存失败', action.result.message);
							   }
							},
							waitMsg: '数据保存中，请稍候...'
						});
					} else {
						Ext.Msg.alert('提示', '请填写完整再提交！');
					}
					//win.hide();
				}
			},{
				text: '打印学籍',
				iconCls:'Printer',
				handler: function() {
					
				} 
			},{
				text: '删除学籍',
				iconCls:'Pagedelete',
				handler: function() {
					
				}
			}]
	});
	
	var win = Ext.create('Ext.window.Window', {
		layout:'fit',
		width: '100%',
		height:450,
		maximized:true,//初始最大化
		closeAction:'close',
		modal: true,
		plain: true,
		title: "学生学籍信息",
		iconCls: "Note",
		items: [form]
	});
	
	form.form.reset();
	win.show();
}