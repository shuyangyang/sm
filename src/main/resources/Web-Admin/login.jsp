<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<html>  
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
<title>欢迎登录学校综合管理平台V1.0</title>  
<link href="../ExtJS4.2/resources/css/ext-all-neptune-rtl.css" rel="stylesheet" id="pifu">  
<link href="../ExtJS4.2/css/icon.css" rel="stylesheet">  
<script src="../ExtJS4.2/ext-all.js"></script>  
<script src="../ExtJS4.2/locale/ext-lang-zh_CN.js"></script>  
<script type="text/javascript" src="../js/func.js"></script>
<script type="text/javascript" src="js/login/login.js"></script>
<script type="text/javascript">
<!--
window.history.forward(1);
//-->
</script>
<style type="text/css">  
.user{background: url(../ExtJS4.2/icons/user.png) no-repeat 2px 2px;}  
.key{background: url(../ExtJS4.2/icons/key.png) no-repeat 2px 2px;}  
.Userkey{background: url(../ExtJS4.2/icons/Userkey.png) no-repeat 2px 2px;}  
.key,.user,.Userkey{  
            background-color: #FFFFFF;  
            padding-left: 20px;  
            font-size: 12px;  
        }  
.bgimage {   
   background:url(images/Login_Banner.jpg ) no-repeat top;  
   position:absolute;  
}
</style>
<!-- 皮肤 -->
<link href="../css/pifu.css" rel="stylesheet">
<script type="text/javascript" src="js/huanfu.js"></script>
</head>  
<body>
<div id="selectStyle">
<select onchange="changeBg()" id="chbg">
<option value="0">✲☞更换皮肤☚✲</option>
<optgroup label="简约风格"></optgroup>
<option value="1">✲✲清爽白云✲✲</option>
<option value="3">✲✲全景星空✲✲</option>
<option value="4">✲✲LOVE甜蜜✲✲</option>
<option value="5">✲✲西西里夜色✲✲</option>
<option value="8">✲✲爱心树✲✲</option>
<option value="9">✲✲可爱阿狸✲✲</option>
<option value="10">✲✲I LOVE YOU✲✲</option>
<option value="11">✲✲简约粉色✲✲</option>
<option value="12">✲✲黄昏篮球✲✲</option>
<option value="13">✲✲流氓兔✲✲</option>
<optgroup label="风景主题"></optgroup>
<option value="2">✲✲可爱小孩✲✲</option>
<option value="6">✲✲荷塘景色✲✲</option>
<option value="7">✲✲热气球景色✲✲</option>
<option value="14">✲✲广阔草原✲✲</option>
<option value="15">✲✲湖岸风景✲✲</option>
<option value="16">✲✲高速公路✲✲</option>
<option value="17">✲✲唯美黄昏✲✲</option>
<option value="18">✲✲回家列车✲✲</option>
<option value="19">✲✲水墨风景✲✲</option>
<option value="20">✲✲水墨江南✲✲</option>
<option value="21">✲✲水墨云海✲✲</option>
<optgroup label="游戏主题"></optgroup>
<option value="22">✲✲暗黑破坏神✲✲</option>
<option value="23">✲✲风暴英雄✲✲</option>
<option value="24">✲✲炉石传说✲✲</option>
<option value="25">✲✲魔兽争霸✲✲</option>
<option value="26">✲✲炮炮兵✲✲</option>
<option value="27">✲✲小羊肖恩✲✲</option>
<option value="28">✲✲星际争霸✲✲</option>
<option value="29">✲✲星际迷航✲✲</option>
</select>
</div>
    <div id="hello-tabs">  
        <!-- <img border="0" width="430" height="60"    src="../demo/image/logo.jpg" /> -->  
    </div>  
    <div id="aboutDiv" class="x-hidden"  
        style='color: black; padding-left: 10px; padding-top: 10px; font-size: 12px'>  
   学校综合管理平台 v1.0<br/><br/>  
    ©2013-2015 思考者日记网|束洋洋个人博客（沪ICP备13005070）<br/><br/>  
    官方网站&nbsp;&nbsp;:&nbsp;&nbsp;<a href="http://www.shuyangyang.com.cn" target="_blank">www.shuyangyang.com.cn</a>  
    </div>
</body>  
</html>  