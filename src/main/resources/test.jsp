<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Chat by Web Sockets</title>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/web_socket.js"></script>
<script type="text/javascript" src="js/jquery.WebSocket.js"></script>
<script type='text/javascript'>
//ws://localhost/servlet/a
var ws;
var WEB_SOCKET_SWF_LOCATION = 'media/WebSocketMain.swf';
var WEB_SOCKET_DEBUG = true;
	$(function() {
		$("#sendBtn").on("click",send);
		 connection();
	});
	
	function connection(){
		   ws = $.websocket({
		        domain:"127.0.0.1", 
		        port:"80",
		        protocol:"messagepush/email", 
		        onOpen:function(event){
		        	showMessage("您已成功登录");  
		        },  
		        onError:function(event){
		         	alert("error:"+ event)
		        },  
		        onMessage:function(result){  
		        	receiveMessage(result);
		        },
		        onClose:function(event){
		        	ws = null;
			    }
		    });  
	}
	function send(){
		if(!ws){
			alert("您已经断开聊天室");
			return;
		}
		 var msg=$.trim($("#messageInput").val());
		 if(msg==""){
			 return;
		}else{
			console.info("===="+msg);
			 ws.send(msg);  
			 $("#messageInput").val("").focus();
		 }
	}
	
	function receiveMessage(result){
		showMessage(result);
	}
 
	function showMessage(msg){
		$("#public").append(msg+"<br>");
	}
</script>
</head>
<body>
<div id="public" style="height:300px;border:1px solid;overflow: auto; margin-bottom: 5px"></div>
<div class="input">
	<textarea rows="5" cols="50" id="messageInput" onKeyDown="if (event.keyCode==13)$('#sendBtn').click()" placeholder="说点什么吧。。"></textarea>
	<input type="button" id="sendBtn" value="提交" > 
</div>
<p>JAVA Jetty for WebSocket</p>
</body>
</html>
