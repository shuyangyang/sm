function messageBox(){
	//初始化编辑器控件
	wirteform.form.reset();
	wirtewin.show();
	wirtewin.hide();
	Ext.getCmp("hiddenFlag").setValue("wirte");
	Ext.getCmp("fujianFlag").setValue("no");
	store.load();
	store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
	//初始化编辑器控件结束
	Ext.create('Ext.window.Window', {
		layout:'border',
		width: '100%',
		height:450,
		maximized:true,//初始最大化
		modal: true,
		plain: true,
		closeAction:'close',
		title: "我的信箱",
		iconCls: "Emailstarted",
		tbar: [
	           { xtype: 'button', text: '收取',iconCls:'Emailopen',handler:function(){
	        	   Ext.MessageBox.progress("收取信件", "正在收取信件...", "0%");
        		   updateProgress(0);
        		   store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
	           }},
		       '-',
		       { xtype: 'button', text: '写消息',iconCls:'Emailedit',handler:function(){
		    	   wirteform.form.reset();
		    	   wirtewin.show();
		    	   editor.setData("");
		    	   Ext.getCmp("hiddenFlag").setValue("wirte");
		    	   Ext.getCmp("fujianFlag").setValue("no");
		       }},
		       '-',
		       { xtype: 'button', text: '回复',iconCls:'MailReply',handler:function(){
		    	   var rec = grid.getSelectionModel().getSelection();
		    	   if(rec.length>0&&rec.length<=1){
		    		   Ext.getCmp("hiddenFlag").setValue("reply");
		    		   var messgeId = rec[0].get('id');
		    		   Ext.Ajax.request({
							url: '/message/readMessage',
							method:'POST',
							waitMsg: '处理中...',
							params: {
								messageId: messgeId
						    },
							success: function(response,options) {
								var json = Ext.JSON.decode(response.responseText);
								//.setValue(json.data.receverid_user)
//								gdCbx2.setSubmitValue(json.data.send_user);
//								$("#shoujianren-inputEl").val(json.data.send_user);
								
								Ext.getCmp('subjectV').setValue("回复："+json.data.message_title);
//								editor.setData("");
								editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
						    	wirtewin.show();
							},
							failure: function(response,options) {
								Ext.Msg.alert('提示','处理失败');
							}
						});
		    	   }else if(rec.length<=0){
		    		   Ext.Msg.alert('提示','请选择一项消息再操作');
		    	   }else{
		    		   Ext.Msg.alert('提示','消息有且只能选择一项');
		    	   }
		       } },
		       '-',
		       { xtype: 'button', text: '转发',iconCls:'Emailgo',handler:function(){
		    	   var rec = grid.getSelectionModel().getSelection();
		    	   if(rec.length>0&&rec.length<=1){
		    		   var messgeId = rec[0].get('id');
		    		   Ext.Ajax.request({
							url: '/message/readMessage',
							method:'POST',
							waitMsg: '处理中...',
							params: {
								messageId: messgeId
						    },
							success: function(response,options) {
								var json = Ext.JSON.decode(response.responseText);
								Ext.getCmp('subjectV').setValue("转发："+json.data.message_title);
//								editor.setData("");
								editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
						    	wirtewin.show();
							},
							failure: function(response,options) {
								Ext.Msg.alert('提示','处理失败');
							}
						});
		    	   }else if(rec.length<=0){
		    		   Ext.Msg.alert('提示','请选择一项消息再操作');
		    	   }else{
		    		   Ext.Msg.alert('提示','消息有且只能选择一项');
		    	   }
		       }  },
		       '-',
		       { xtype: 'button', text: '删除',iconCls:'Emaildelete',handler:function(){
		    		var messgeId = '';
		    		var rec = grid.getSelectionModel().getSelection();
		    		if(rec.length<=0){
		    			Ext.Msg.alert('提示','请选择一项消息再操作');
		    		}else{
		    			for(var i=0; i<rec.length; i++){
			    			if(i!=rec.length-1){
			    				messgeId += rec[i].get('id')+',';
			    			}else{
			    				messgeId += rec[i].get('id');
			    			}
			    		}
		    			Ext.Msg.show({
		    				title:'删除消息',
		    				msg: '确认要删除此条消息？',
		    				buttons: Ext.Msg.YESNOCANCEL,
		    				fn: function(btn, text) {
		    					if(btn == "yes") {
		    						Ext.Ajax.request({
		    							url: '/message/delMessage',
		    							method:'POST',
		    							waitMsg: '处理中...',
		    							params: {
		    								messageId: messgeId
		    						    },
		    							success: function(request) {
		    								Ext.Msg.alert('提示','删除成功，删除的内容可以在垃圾桶里找到');
		    								store.load();
		    					    		store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
		    							},
		    							failure: function(response,options) {
		    								Ext.Msg.alert('提示','处理失败');
		    							}
		    						});
		    					}
		    				},
		    				icon: Ext.MessageBox.QUESTION
		    			});
		    		}
		    		
		       }},
		       '->',
		       {xtype: 'textfield',name: 'name',
	    		   fieldLabel: '搜索邮件',labelAlign:'left',labelWidth:70
	           },
	           {xtype: 'button', text: '搜索',iconCls:'Zoom',listeners:{click:function(){
	        	   //do some thing
	           }}}
		     ],
		items: [grid,tree]
	}).show();
	wirtewin.hide();
}
Ext.define('Item', {
    extend: 'Ext.data.Model',
    fields: ['text', 'canDropOnFirst', 'canDropOnSecond']
});
var editor;
Ext.QuickTips.init();
var required = '<span style="color:red;font-weight:bold" data-qtip="这是一个必填项">*</span>';
var gdCbxSt2 = Ext.create("Ext.data.Store",{
	pageSize:15, //每页显示几条数据  
    proxy:{  
        type:'ajax',  
        url:'/user/showUser',  
        reader:{  
            type:'json',
            totalProperty:'total',  
            root:'data',  
            idProperty:'id'  
        }  
    },  
    fields:[  
       {name:'user_id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
       {name:'user_name'},  
       {name:'login_name'},  
       {name:'user_status'}
    ]  
});
var gdCbx2 = new Ext.form.field.GridComboBox({
	fieldLabel : '收件人',
	id:'shoujianren',
	allowBlank:false,
	emptyText :'点击这里展开、收缩选择收件人',
	afterLabelTextTpl: required,
	multiSelect : true,
	displayField : 'user_name',
	valueField : 'login_name',
	width : '100%',
	editable:false,
	labelWidth : 50,
	store : gdCbxSt2,
	queryMode : 'remote',
	matchFieldWidth : false,
	pickerAlign: 'bl',
	gridCfg : {
		store : gdCbxSt2,
		selModel : new Ext.selection.CheckboxModel({
			checkOnly: true
		}),
		height: 450,
		width: 887,
		columns : [
       	   {xtype: 'rownumberer'},
           {header:'编号',dataIndex:'user_id',hidden: true},
           {header:'用户名',dataIndex:'user_name',width:'50%'},
           {header:'登录名',dataIndex:'login_name',hidden: true},
           {header:'用户状态',dataIndex:'user_status',renderer:function(value){
               if(value=='DISABLED'){
                   return "<span style='color:red;font-weight:bold';>失效</span>";  
               } else {
                   return "<span style='color:green;font-weight:bold';>正常</span>";  
               }
   			},width:'50%'}
	    ],
		tbar: [
			{xtype: 'combo',name: 'name',fieldLabel: '输入即搜索',labelAlign:'left',labelWidth:80, store:gdCbxSt2, hideTrigger:true,displayField: 'user_name',valueField: 'login_name',queryMode:"local",editable:true,triggerAction:"all"}
		],
		bbar : Ext.create('Ext.PagingToolbar', {
			store : gdCbxSt2,
			displayInfo : true,
			plugins: Ext.create('Ext.ux.ProgressBarPager'),
            emptyMsg: "没有记录" //没有数据时显示信息
		})
	}
});
var wirteform = Ext.create('Ext.form.FormPanel',{
    width: 980,
    border : false,
    labelAlign:'left',
    defaults:{  
        margin:'10 10 0 10'  
    }, 
    labelSeparator : "：",
	 	defaultType: 'textfield',
    items: [new Ext.form.Panel(
    {
		border : false,
		layout: 'hbox',
		defaults: {
			xtype: 'button', margin: 2, width: 60
		},
		items : gdCbx2
	}),{
    	anchor: '100%',
    	id:'subjectV',
    	labelWidth:50,
    	fieldLabel: '主题',
    	afterLabelTextTpl: required,
		name: 'messageTitle',
		maxLengthText :'主题长度不能超过50',  
        maxLength : 50,
		allowBlank:false,
		inputType: 'messageTitle'
    },{
    	id:'hiddenFlag',
    	hidden:true,
    	fieldLabel: '是否已回复/转发'
    },{
    	id:'fujianFlag',
    	hidden:true,
    	fieldLabel: '是否已回复/转发'
    },{
    	xtype:'panel',
        contentEl:'ckeditorContent',//获取值：CKEDITOR.instances.textarea的name值.getData();
        listeners:{
        	'render':function(){
        		editor = CKEDITOR.replace('ckeditorContent');
        	}
        }
    }] 
});
//插入附件
var uploadForm = Ext.create('Ext.form.Panel', {
	width: 530,
    bodyPadding: '10 10 0',
    defaults: {
        anchor: '100%',
        allowBlank: false,
        msgTarget: 'side',
        labelWidth: 50
    },
	fileUpload: true,
	items: [
	   {
		xtype: 'filefield',
        emptyText: '请选择一个文件...',
        fieldLabel: '文件',
        name: 'fileName',
        buttonText: '',
        buttonConfig: {
            iconCls: 'Upload'
        }
	},{
		   xtype: 'checkboxfield',
		   //boxLabelAlign:'before',
		   hideLabel: true,
		   checked:true,
		   boxLabel: '<font color="#FF0000">注意：</font>文件大小不得超过20M，目前仅支持：jpg|jpeg|bmp|gif|png|txt|doc|docx|xls|xlsx|csv|ppt|pptx|pdf|wps|et|dps|mp4|swf|mov|mp3格式文件',
	   }],
	buttons: [{
			text: '上传',
			formBind: true,
            disabled: true,
			handler: function(){
                var form = uploadForm.getForm();
                if(form.isValid()){
                	Ext.getCmp("fujianFlag").setValue("attachment");
                    form.submit({
                        url: '/message/uploadFile',
                        method: 'POST',
                        waitMsg: '正在上传附件，请稍后...',
                        success: function(fp, o) {
                        	var respText = Ext.JSON.decode(o.response.responseText).data;
                        	editor.setData(editor.getData()+"附件地址：<a href='"+respText+"' target='_blank'>"+respText+"</a>");
   							Ext.Msg.alert('操作提示', '附件添加成功，您还可以继续添加');
                            uploadwin.hide();
                        },failure: function(form, action){
                        	switch (action.failureType) {
							case Ext.form.Action.CLIENT_INVALID:
								Ext.Msg.alert('操作提示', '文件格式错误！');
								break;
							case Ext.form.Action.CONNECT_FAILURE:
								Ext.Msg.alert('操作提示', '连接异常！');
								break;
							case Ext.form.Action.SERVER_INVALID:
							   Ext.Msg.alert('操作提示', action.result.message);
                        	}
						}
                    });
                }
            }
		},{
            text: '重置',
            handler: function() {
            	uploadForm.form.reset();
            }
        }]
});

var uploadwin = Ext.create('Ext.window.Window', {
	title: "上传附件",
	iconCls: "PageUpload",
	layout:'fit',
	width: 530,
	height:200,
	closeAction:'hide',
	modal: true,
	plain: true,
	items: [uploadForm]
});

var wirtewin = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 980,
	height:600,
	closeAction:'close',
	modal: true,
	plain: true,
	maximizable: true,
	title: "写消息",
	iconCls: "Emailstarted",
	tbar: [
	       { xtype: 'button', text: '发送',iconCls:'MailReply',handler:function(){
	    	   var toName = gdCbx2.getSubmitValue(); //收件人
	    	   var subject = Ext.getCmp("subjectV").getValue();//主题
	    	   var flag = Ext.getCmp("hiddenFlag").getValue();//标记
	    	   var content = editor.getData();//内容
	    	   if(wirteform.form.isValid()&&!Ext.isEmpty(content)) {
	    		   Ext.Ajax.request({
						url: '/message/sendMessage',
						method:'POST',
						waitMsg: '发送中...',
						params: {
					        toUser: toName,
					        subject:subject,
					        flag:flag,
					        content:content
					    },
						success: function(request) {
							var respText = Ext.decode(request.responseText);
							if(respText.code != 'OK') {
								Ext.Msg.alert('提示','发送失败');
							}else{
								Ext.Msg.alert('提示','发送成功');
								ws.send(toName);
								wirtewin.hide();
							}
						},
						failure: function(res,options) {
							var respText = Ext.decode(res.responseText);
							Ext.Msg.alert('发送失败', respText.message+"("+respText.code+")");
						}
					});
	    	   }else{
	    		   Ext.Msg.alert('系统提示', '收件人、主题和内容不能为空！');
	    	   }
	       }  },
	       '-',
	       { xtype: 'button', text: '保存',iconCls:'Emailgo',handler:function(){
	    	   var toName = gdCbx2.getSubmitValue(); //收件人
	    	   var subject = Ext.getCmp("subjectV").getValue();//主题
	    	   var content = editor.getData();//内容
	    	   if(wirteform.form.isValid()&&!Ext.isEmpty(content)) {
	    		   Ext.Ajax.request({
						url: '/message/saveMessage',
						method:'POST',
						waitMsg: '保存草稿中，请稍候...',
						params: {
					        toUser: toName,
					        subject:subject,
					        content:content
					    },
						success: function(request) {
							var respText = Ext.decode(request.responseText);
							if(respText.code != 'OK') {
								Ext.Msg.alert('提示', '保存失败');
							}else{
								Ext.MessageBox.confirm("提示", "草稿保存成功,是否继续写信？", function (btn) {
									if(btn=="no"){
										wirtewin.hide();
									}
							    });
							}
						},
						failure: function(res,options) {
							var respText = Ext.decode(res.responseText);
							Ext.Msg.alert('保存失败',respText.message+"("+respText.code+")");
						}
					});
	    	   }else{
	    		   Ext.Msg.alert('系统提示', '收件人、主题和内容不能为空！');
	    	   }
	       } },
	       '-',
	       {xtype:'button',text:'附件',iconCls:'Attach',handler:function(){
	    	   uploadwin.show();
	       }},
	       {xtype:'button',text:'重置',iconCls:'Arrowrotateclockwise',handler:function(){
	    	   form.form.reset();
	    	   editor.setData("");
	       }},
	       {xtype:'button',text:'帮助',iconCls:'Help',handler:function(){
	    	   Ext.Msg.alert('帮助', '在编辑器内按下键盘的ALT+0数字组合键试试吧<br><font style="color:red">正文内容的文字不宜过多，请尽量不要长篇大论</font>');
	       }}
	     ],
	items: [wirteform]
});
var updateProgress = function (progress) {
    if (progress >= 1) {
    	Ext.MessageBox.updateProgress(1, "收取成功");
    	Ext.defer(function () { Ext.MessageBox.close(); }, 300);
        return;
    }
    Ext.MessageBox.updateProgress(progress, Math.round(progress * 100) + "%");
    Ext.defer(function () {
        updateProgress(progress + 0.1);
    }, 300);
}
//定义右键菜单
var contextMenu = Ext.create("Ext.menu.Menu",{
    plain: true,
    floating: true,  // usually you want this set to True (default)
    renderTo: Ext.getBody(),  // usually rendered by it's containing component
	items : [{
		text : '清空当前所有内容',
		iconCls:'Folderdelete',
		handler : function(){
			//收件箱可以进行此操作
			if(store2.getProxy().extraParams.dirtype=="inBox"){
				Ext.MessageBox.confirm("提示", "确定要清空当前文件夹吗？",function(btn){
					if(btn=='yes'){
						Ext.Ajax.request({
							  url: '/message/delAllRead',
							  success:function(request){
								  var respText = Ext.decode(request.responseText);
								  if(respText.code != 'OK') {
									  Ext.Msg.alert('操作失败', respText.message+"("+respText.code+")");
								  }else{
									  Ext.Msg.alert('操作成功', '清空的消息还可以在垃圾桶里查看消息');
									  store.load();
									  store2.load({params:{start:0,limit:15,dirtype:'inBox'}});
									  $("#unReadMess").html("<img alt='站内消息' src='ExtJS4.2/icons/email.png'>消息(0)");
								  }
							  },
							  failure:function(res){
								  var respText = Ext.decode(res.responseText);
								  if(respText.code=="EMPTY"){
									  Ext.Msg.alert('操作失败', "没有消息可以操作");
								  }else{
									  Ext.Msg.alert('操作失败', respText.message+"("+respText.code+")");
								  }
							  }
						  });
					}
				});
			}else{
				Ext.Msg.alert('提示', '只有收件箱里的消息才可以执行此操作');
			}
		}
	},{
		text : '全部标记为已读',
		iconCls:'Emailstar',
		handler : function(){
			//收件箱
			if(store2.getProxy().extraParams.dirtype=="inBox"){
				Ext.Ajax.request({
					  url: '/message/hasRead',
					  success:function(request){
						  var respText = Ext.decode(request.responseText);
						  if(respText.code != 'OK') {
							  Ext.Msg.alert('操作失败', respText.message+"("+respText.code+")");
						  }else{
							  store.load();
							  store2.load({params:{start:0,limit:15,dirtype:'inBox'}});
							  $("#unReadMess").html("<img alt='站内消息' src='ExtJS4.2/icons/email.png'>消息(0)");
						  }
					  },
					  failure:function(res){
						  var respText = Ext.decode(res.responseText);
						  Ext.Msg.alert('操作失败', respText.message+"("+respText.code+")");
					  }
				  });
			}else{
				Ext.Msg.alert('提示', '只有收件箱里的消息才可以执行此操作');
			}
		}
	}] 
});

// 绑定节点右键菜单功能
function treeContextHandler(view, record, item, index, event, options) {
	event.preventDefault();//一说是组织浏览器的默认右键事件，二说是防止两次出现右键画面（怎么可能出现两次）
	contextMenu.showAt(event.getXY());//显示右键菜单
}

var store = Ext.create("Ext.data.TreeStore",{
    model: 'Item',
    autoLoad : true,
	proxy : {
         type : 'ajax',
         url : '/message/messageDir',
         reader : {
             type : 'json',
             root : 'children'//数据  
         }, 
         //传参  
         extraParams : {  
             id : 'null'  
         }
     },
     root : {
         text : '信箱菜单',
         expanded : true 
     },
});

var tree = Ext.create("Ext.tree.Panel", {
	region: 'west',
	width: 200,
    rootVisible: false,
    lines:false,
    store: store
});
tree.on('itemcontextmenu', treeContextHandler); 
var columns = [
               {header:'编号',dataIndex:'id',hidden: true},
       			{dataIndex:'receverid_status',width:'2%',renderer:function(value){
             	   if(value=="DISABLED"){
             		   return "";
             	   }else{
             		   return "<span><img src='ExtJS4.2/icons/readmail_reply.png' title='已回复' /></span>";
             	   }
                }},
               {header:'发件人',dataIndex:'send_user',width:'10%'},
               {dataIndex:'attachment',width:'2%',renderer:function(value){
            	   if(value==0){
            		   return "";
            	   }else{
            		   return "<span><img src='ExtJS4.2/icons/attach.png' title='有附件' /></span>";
            	   }
       			}},
               {header:'主题',dataIndex:'message_title',width:'30%'},
               {header:'正文',dataIndex:'message_text',width:'30%'},
               {header:'时间',dataIndex:'send_time',width:'20%',renderer:function(value){
            	   return formatDateTime(value);
               }}
           ];
var store2 = Ext.create("Ext.data.Store",{
	pageSize:15, //每页显示几条数据  
    proxy:{  
        type:'ajax',  
        url:'/message/allMessage',  
        reader:{  
            type:'json',  
            totalProperty:'total',  
            root:'data',  
            idProperty:'id'  
        },
        extraParams: {
        	dirtype: 'inBox'
        }
    },  
    fields:[  
       {name:'id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
       {name:'message_title'},  
       {name:'send_time'},
       {name:'attachment'},
       {name:'send_user'},
       {name:'read_status'},
       {name:'receverid_status'},
       {name:'message_text'}
    ]  
});
tree.on('itemclick', function(view,record){
	var dirtype = record.raw.dirtype;
	store2.getProxy().extraParams.dirtype =dirtype;
	store2.load({params:{start:0,limit:15,dirtype:dirtype}}); 
});
var sm = Ext.create('Ext.selection.CheckboxModel');
var grid = Ext.create("Ext.grid.Panel",{
	region: 'center',
	store: store2,
	selModel: sm,
    columns: columns,
    region: 'center', //框架中显示位置，单独运行可去掉此段
    loadMask:true, //显示遮罩和提示功能,即加载Loading……  
    forceFit:true, //自动填满表格  
    columnLines:false, //列的边框
    rowLines:true, //设置为false则取消行的框线样式
    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: store2,   // GridPanel使用相同的数据源
        dock: 'bottom',
        displayInfo: true,
        plugins: Ext.create('Ext.ux.ProgressBarPager'),
        emptyMsg: "没有记录" //没有数据时显示信息
    }],
    viewConfig: {
        stripeRows: false, 
        getRowClass: function(record) { 
            return record.get('read_status') == 'DISABLED' ? 'boy-row' : '';//设置未读消息的行样式
        } 
    }
});
//加载数据  
store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
// 表格配置结束

//表格右键菜单  
var gridcontextmenu = new Ext.menu.Menu({
    items:[{
        text:'回复邮件',
        iconCls:'MailReply',
        handler:function(){  
        	var rec = grid.getSelectionModel().getSelection();
	    	   if(rec.length>0&&rec.length<=1){
	    		   var messgeId = rec[0].get('id');
	    		   Ext.Ajax.request({
						url: '/message/readMessage',
						method:'POST',
						waitMsg: '处理中...',
						params: {
							messageId: messgeId
					    },
						success: function(response,options) {
							var json = Ext.JSON.decode(response.responseText);
							//.setValue(json.data.receverid_user)
//							gdCbx2.setSubmitValue(json.data.send_user);
//							$("#shoujianren-inputEl").val(json.data.send_user);
							
							Ext.getCmp('subjectV').setValue("回复："+json.data.message_title);
//							editor.setData("");
							editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
					    	wirtewin.show();
						},
						failure: function(response,options) {
							Ext.Msg.alert('提示','处理失败');
						}
					});
	    	   }else if(rec.length<=0){
	    		   Ext.Msg.alert('提示','请选择一项消息再操作');
	    	   }else{
	    		   Ext.Msg.alert('提示','消息有且只能选择一项');
	    	   }
        }  
    },{
        text:'转发邮件',
        iconCls:'Emailgo',
        handler:function(){  
        	var rec = grid.getSelectionModel().getSelection();
	    	   if(rec.length>0&&rec.length<=1){
	    		   var messgeId = rec[0].get('id');
	    		   Ext.Ajax.request({
						url: '/message/readMessage',
						method:'POST',
						waitMsg: '处理中...',
						params: {
							messageId: messgeId
					    },
						success: function(response,options) {
							var json = Ext.JSON.decode(response.responseText);
							Ext.getCmp('subjectV').setValue("转发："+json.data.message_title);
//							editor.setData("");
							editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
					    	wirtewin.show();
						},
						failure: function(response,options) {
							Ext.Msg.alert('提示','处理失败');
						}
					});
	    	   }else if(rec.length<=0){
	    		   Ext.Msg.alert('提示','请选择一项消息再操作');
	    	   }else{
	    		   Ext.Msg.alert('提示','消息有且只能选择一项');
	    	   }
        }  
    },{
        text:'删除邮件',
        iconCls:'Emaildelete',
        handler:function(){
    		var messgeId = '';
    		var rec = grid.getSelectionModel().getSelection();
    		if(rec.length<=0){
    			Ext.Msg.alert('提示','请选择一项消息再操作');
    		}else{
    			for(var i=0; i<rec.length; i++){
	    			if(i!=rec.length-1){
	    				messgeId += rec[i].get('id')+',';
	    			}else{
	    				messgeId += rec[i].get('id');
	    			}
	    		}
    			Ext.Msg.show({
    				title:'删除消息',
    				msg: '确认要删除此条消息？',
    				buttons: Ext.Msg.YESNOCANCEL,
    				fn: function(btn, text) {
    					if(btn == "yes") {
    						Ext.Ajax.request({
    							url: '/message/delMessage',
    							method:'POST',
    							waitMsg: '处理中...',
    							params: {
    								messageId: messgeId
    						    },
    							success: function(response,options) {
    								Ext.Msg.alert('提示','删除成功，删除的内容可以在垃圾桶里找到');
    								store.load();
    					    		store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
    							},
    							failure: function(response,options) {
    								Ext.Msg.alert('提示','处理失败');
    							}
    						});
    					}
    				},
    				icon: Ext.MessageBox.QUESTION
    			});
    		}
    		
       }
    }]  
});  

grid.on("itemcontextmenu",function(view,record,item,index,e){  
    e.preventDefault();  
    gridcontextmenu.showAt(e.getXY());  
});
var readForm = Ext.create('Ext.form.Panel', {
	 border : false,
	 defaults:{  
		 margin:'10 10 0 10' 
    }, 
	 defaultType: 'textfield',
	 labelAlign:'left',
	 items: [{
				fieldLabel: 'ID',
				name: 'id',
				hidden:true
			},{
				fieldLabel: 'MessageID',
				id:'messageId',
				name: 'message_id',
				hidden:true
			},{
				anchor: '100%',
				fieldLabel: '发件人',
				disabled:true,
				name: 'send_user'
			},{
				anchor: '100%',
				fieldLabel: '收件人',
				disabled:true,
				name: 'receverid_user'
			},,{
				anchor: '100%',
				id:'messageTime',
				fieldLabel: '发送时间',
				disabled:true,
				name: 'send_time'
			},{
				anchor: '100%',
				fieldLabel: '主题',
				disabled:true,
				name: 'message_title'
			},{
				anchor: '100%',
				xtype: 'htmleditor',
				readOnly:true,
				height: 400,
				name: 'message_text'
			}
		],tbar: [
			       '-',
			       { xtype: 'button', text: '回复',iconCls:'MailReply',handler:function(){
			    	   readwin.hide();
			    	   wirteform.form.reset();
			    	   Ext.getCmp("hiddenFlag").setValue("reply");
			    	   var messgeId = Ext.getCmp('messageId').value;
			    	   Ext.Ajax.request({
							url: '/message/readMessage',
							method:'POST',
							waitMsg: '处理中...',
							params: {
								messageId: messgeId
						    },
							success: function(response,options) {
								var json = Ext.JSON.decode(response.responseText);
								//.setValue(json.data.receverid_user)
//								gdCbx2.setSubmitValue(json.data.send_user);
//								$("#shoujianren-inputEl").val(json.data.send_user);
//								editor.setData("");
								Ext.getCmp('subjectV').setValue("回复："+json.data.message_title);
								editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
						    	wirtewin.show();
							},
							failure: function(response,options) {
								Ext.Msg.alert('提示','处理失败');
							}
						});
			    	   
			       }},
			       '-',
			       { xtype: 'button', text: '转发',iconCls:'Emailgo',handler:function(){
			    	   readwin.hide();
			    	   wirteform.form.reset();
			    	   Ext.getCmp("hiddenFlag").setValue("reply");
			    	   var messgeId = Ext.getCmp('messageId').value;
			    	   Ext.Ajax.request({
							url: '/message/readMessage',
							method:'POST',
							waitMsg: '处理中...',
							params: {
								messageId: messgeId
						    },
							success: function(response,options) {
								var json = Ext.JSON.decode(response.responseText);
								Ext.getCmp('subjectV').setValue("转发："+json.data.message_title);
//								editor.setData("");
								editor.setData("<p>&nbsp;</p><hr />发件人："+json.data.send_user+"<br />发送时间："+formatDateTime(json.data.send_time)+"<br />收件人："+json.data.receverid_user+"<br />主题："+json.data.message_title+"<br />"+json.data.message_text);
						    	wirtewin.show();
							},
							failure: function(response,options) {
								Ext.Msg.alert('提示','处理失败');
							}
						});
			    	   
			       } },
			       '-',
			       { xtype: 'button', text: '删除',iconCls:'Emaildelete',handler:function(){
			    		var messgeId = '';
			    		var rec = grid.getSelectionModel().getSelection();
			    		if(rec.length<=0){
			    			Ext.Msg.alert('提示','请选择一项消息再操作');
			    		}else{
			    			for(var i=0; i<rec.length; i++){
				    			if(i!=rec.length-1){
				    				messgeId += rec[i].get('id')+',';
				    			}else{
				    				messgeId += rec[i].get('id');
				    			}
				    		}
			    			Ext.Msg.show({
			    				title:'删除消息',
			    				msg: '确认要删除此条消息？',
			    				buttons: Ext.Msg.YESNOCANCEL,
			    				fn: function(btn, text) {
			    					if(btn == "yes") {
			    						Ext.Ajax.request({
			    							url: '/message/delMessage',
			    							method:'POST',
			    							waitMsg: '处理中...',
			    							params: {
			    								messageId: messgeId
			    						    },
			    							success: function(response,options) {
			    								Ext.Msg.alert('提示','删除成功，删除的内容可以在垃圾桶里找到');
			    								store.load();
			    					    		store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
			    							},
			    							failure: function(response,options) {
			    								Ext.Msg.alert('提示','处理失败');
			    							}
			    						});
			    					}
			    				},
			    				icon: Ext.MessageBox.QUESTION
			    			});
			    		}
			       } }
			     ]
});
var readwin = Ext.create('Ext.window.Window', {
	layout:'fit',
	width:980,
	height:600,
	maximized:true,//初始最大化
	maximizable:true,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "消息",
	iconCls: "Email",
	items: [readForm],
	listeners:{
    	'close':function(){
    		store.load();
    		store2.load({params:{start:0,limit:15,dirtype:'inBox'}}); 
    	}
    }
});
var fields = [  
              {name:'id'}, //mapping:0 这样的可以指定列显示的位置，0代表第1列，可以随意设置列显示的位置  
              {name:'send_user'},  
              {name:'receverid_user'},  
              {name:'message_id'},
              {name:'message_text'},
              {name:'message_title'},
              {name:'send_time'}
            ];
//双击阅读消息
grid.on("itemdblclick",function(grid, rowindex, e){
	var messageId = rowindex.data.id;
	//如果是草稿箱直接打开可发送的窗口
	if(store2.getProxy().extraParams.dirtype=="draFts"){
	   wirteform.form.reset();
 	   wirtewin.show();
 	  Ext.getCmp("hiddenFlag").setValue("wirte");
 	 Ext.getCmp("fujianFlag").setValue("no");
 	   Ext.Ajax.request({
			url: '/message/readMessage',
			method:'POST',
			waitMsg: '处理中...',
			params: {
				messageId: messageId
		    },
			success: function(response,options) {
				var json = Ext.JSON.decode(response.responseText);
				Ext.getCmp('subjectV').setValue(json.data.message_title);
				editor.setData(json.data.message_text);
		    	wirtewin.show();
			},
			failure: function(response,options) {
				Ext.Msg.alert('提示','处理失败');
			}
		});
	}else{
		readForm.form.reset();
		readwin.show();
		var messageCountText = $("#unReadMess").text();
		messageCountText = messageCountText.substr(3,1);
		var mesNum = parseInt(messageCountText)-1;
		if(mesNum>0){
			console.info("coming"+mesNum);
			$("#unReadMess").html("<img alt='站内消息' src='ExtJS4.2/icons/email_starred.png'>消息("+mesNum+")");
		}else{
			$("#unReadMess").html("<img alt='站内消息' src='ExtJS4.2/icons/email.png'>消息(0)");
		}
		readForm.form.load({
			url: '/message/readMessage',
			params: {
				messageId: messageId
		    },
			method:'POST',
			waitMsg : '正在载入数据...',
			reader: Ext.create('Ext.data.JsonReader', {
				 type  : 'json',
				 root  : 'data'
			 },fields),
			success : function(form,action) {
				var sendtime = Ext.getCmp('messageTime').value;
				sendtime = formatDateTime(sendtime);
				Ext.getCmp('messageTime').setValue(sendtime);
				
			},
			failure : function(form,action) {
				Ext.Msg.alert('提示', '数据读取失败');
				readwin.hide();
			}
		});
	}
});