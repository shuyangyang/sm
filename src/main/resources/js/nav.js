function doChangePwd() {
	form.form.reset();
	win.show();
}
//修改密码
var form = Ext.create('Ext.form.Panel', {
	 border : false,
	 width: 350,
	 defaults:{  
         margin:'10 0 0 20'  
     }, 
	 defaultType: 'textfield',
	 items: [{
			fieldLabel: '旧密码',
			name: 'oldpwd',
			maxLengthText :'密码长度不能超过20',  
            maxLength : 20,
            minLengthText :'密码长度至少6位',  
            minLength : 6, 
			allowBlank:false,
			inputType: 'password'
		},{
			fieldLabel: '新密码',
			name: 'newpwd',
			allowBlank:false,
			maxLengthText :'密码长度不能超过20',  
            maxLength : 20,  
            minLengthText :'密码长度至少6位',  
            minLength : 6,  
			inputType: 'password'
		},{
			fieldLabel: '重复新密码',
			name: 'renewpwd',
			maxLengthText :'密码长度不能超过20',  
            maxLength : 20,
            minLengthText :'密码长度至少6位',  
            minLength : 6,
			allowBlank:false,
			inputType: 'password'
		} 
		],
		buttons: [{
			text: '保&nbsp;存',
			handler: function() {
				if(form.form.isValid()) {
					var values = form.getForm().getValues();
					if(values.newpwd != values.renewpwd) {
						Ext.Msg.alert('输入错误', '两次输入的新密码不一致！');
					} else if(values.oldpwd == values.renewpwd){
						Ext.Msg.alert('输入错误', '新密码和老密码相同！');
					} else {
						form.form.submit({
							url: '/user/password',
							method: 'POST',
							success: function(form, action) {
								var respText = Ext.JSON.decode(form.responseText);
								if(respText.code != 'OK') {
									Ext.Msg.alert('保存失败', respText.message + "（" + respText.code + "）");
								}else{
									Ext.Msg.alert('保存成功', '密码修改成功,请重新登录！');
									doQuit();
								}
							},
							failure: function(form, action) {
								switch (action.failureType) {
									case Ext.form.Action.CLIENT_INVALID:
										Ext.Msg.alert('修改失败', '数据字段格式错误！');
										break;
									case Ext.form.Action.CONNECT_FAILURE:
										Ext.Msg.alert('修改失败', '连接异常！');
										break;
									case Ext.form.Action.SERVER_INVALID:
									   Ext.Msg.alert('修改失败', action.result.message);
							   }
							},
							waitMsg: '数据保存中，请稍候...'
						});
					}
				} else {
					Ext.Msg.alert('系统提示', '请填写完整再提交！');
				}
			}
		},{
			text: '取&nbsp;消',
			handler: function() {
				win.hide();
			}
		}]
});

var win = Ext.create('Ext.window.Window', {
	layout:'fit',
	width: 350,
	height:200,
	closeAction:'hide',
	modal: true,
	plain: true,
	title: "修改密码",
	iconCls: "Applicationedit",
	items: [form]
});

//退出系统
function doQuit() {
	clearCookie('shuyangyang.login.account');
	Ext.Ajax.request({
		url: '/user/logout',
		success: function(response,options) {
		},
		failure: function(response,options) {
		}
	});
	top.document.location.href = 'Web-Admin/login.jsp';
}
