Ext.Loader.setConfig({
	enabled : true
});

Ext.Loader.setPath({
	'Ext.ux' : 'ExtJS4.2/ux'
});

Ext.require(['Ext.ux.TabCloseMenu']);

Ext.onReady(function() {
	var updateProgress = function (progress) {
	    if (progress >= 1) {
	        Ext.MessageBox.updateProgress(1, "加载完成");
	    	Ext.defer(function () { Ext.MessageBox.close(); }, 200);
	        return;
	    }
	    Ext.MessageBox.updateProgress(progress, Math.round(progress * 100) + "%");
	    Ext.defer(function () {
	        updateProgress(progress + 0.1);
	    }, 100);
	}
	var rightPanel = Ext.create('Ext.tab.Panel', {
				activeTab : 0,
				border : false,
				autoScroll : true,
				//iconCls:'Applicationviewlist',
				region : 'center',
				items : [{
							title : '首页',
							iconCls: 'House',
							html: '<iframe frameborder="no" border=0 height="100%" width="100%" src="portal/index.html" scrolling="auto"></iframe>',
						}],
				plugins: [Ext.create('Ext.ux.TabCloseMenu',{
		        		  	closeTabText: '关闭当前',
		        		  	closeOthersTabsText: '关闭其他',
		        		  	closeAllTabsText: '关闭所有'
		        		  })]
			});
	
	rightPanel.on({beforeRender:function(){
		Ext.MessageBox.progress("加载引擎", "加载中，请稍候...", "0%");
	    updateProgress(0);
//		Ext.MessageBox.wait("工程庞大，努力加载中，请稍候...", "系统进程");
//		Ext.defer(function () {
//	        Ext.MessageBox.close();
//	    }, 2000);
	}});
	
	var tree = Ext.create("Ext.panel.Panel", {
		height : '100%',
		region : 'north',
		layout : {
			// layout-specific configs go here
			type : 'accordion',
			titleCollapse : true,
			animate : true,// 动画切换效果
			activeOnTop : false
		// 折叠展开是否改变菜单顺序
		},
		layoutConfig : {
			animate : true
		},
		split : true
	});
	
	var model = Ext.define("TreeModel", { // 定义树节点数据模型
		extend : "Ext.data.Model",
		fields : [{name : "id",type : "string"},
				{name : "text",type : "string"},
				{name : "iconCls",type : "string"},
				{name : 'url',type:"string"},
				{name : 'description',type:"string"}]
	});
	
	//组建树
	var buildTree = function(json) {
		return Ext.create('Ext.tree.Panel', {
					rootVisible : false,
					border : false,
					lines:false,
					autoScroll:true,
					store : Ext.create('Ext.data.TreeStore', {
								model : model,
								root : {
									expanded : true,
									children : json.children
								}
							}),
							listeners : {
								'itemclick' : function(view, record, item, index, e) {
									//var typeId = record.get("TYPE_ID");
									Ext.MessageBox.progress("加载引擎", "正在处理，请稍候...", "0%");
								    updateProgress(0);
//									Ext.MessageBox.wait("界面加载中，请稍候...", "系统进程");
//									Ext.defer(function () {
//								        Ext.MessageBox.close();
//								    }, 1500);
									var text = record.get('text');
									var icon = record.get('iconCls');
									var url = record.get('url');
									if (Ext.getCmp(text)==undefined) { //判断是否已经打开
										var panel = Ext.create('Ext.panel.Panel',{
											title : text,
											id:text,
											closable : true,
											iconCls : icon,
											html : '<iframe width="100%" height="100%" frameborder="0" src='+url+'></iframe>'
										});
										rightPanel.add(panel);
										rightPanel.setActiveTab(panel);
									}else{
										rightPanel.setActiveTab(Ext.getCmp(text));
									}
								},
								scope : this
							}
				});
	};

	// 整体架构容器
	Ext.create("Ext.container.Viewport", {
		layout : 'border',
		id     : 'winView',
		items : [ {
			region : 'north',
			contentEl : 'north-div',
			height : 62,
			bodyStyle : 'background-color:#BBCCEE;'
		}, {
			region : 'south',
			contentEl : 'south-div',
			height : 20,
			bodyStyle : 'background-color:#BBCCEE;'
		}, {
			layout : 'border',
			title : '菜单',
			id : 'layout-browser',
			region : 'west',
			border : false,
			split : true,
			margins : '2 0 5 5',
			width : 200,
			minSize : 100,
			maxSize : 500,
			autoScroll : false,
			collapsible : true, // 是否折叠
			//iconCls : "Application",
			items : [ tree ] //, detailsPanel
		},
			rightPanel
		],
		listeners : {
			afterrender : function() {
				 tree.getEl().mask('正在加载系统菜单....');
				 Ext.Ajax.request({
					 url:'/menu/showMenu',
					 waitMsg: '正在加载系统菜单....',
					 success : function(response) {
						 var json = Ext.JSON.decode(response.responseText);
						 if(json.code == 'OK'){
							 tree.getEl().unmask();
							 Ext.each(json.data, function(el) {
								 var panel = Ext.create('Ext.panel.Panel', {
									 			id : el.id,
									 			title : el.text,
									 			iconCls:el.iconCls,
									 			layout : 'fit'
										 });
								 panel.add(buildTree(el));
								 tree.add(panel);
							 });
						 }else{
							 Ext.Msg.alert('加载失败', respText.message+"("+respText.code+")");
						 }
					 },failure : function(request) {
						 	tree.getEl().unmask();
							Ext.MessageBox.show({
										title : '操作提示',
										msg : "连接服务器失败",
										buttons : Ext.MessageBox.OK,
										icon : Ext.MessageBox.ERROR
									});
						},
						method : 'post'
				 });
			}
		}
	});
	
});