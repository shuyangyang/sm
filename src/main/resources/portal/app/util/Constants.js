Ext.define('ExtMVC.util.Constants', {

    singleton: true,

    bogusMarkup: "<div class='portlet-content'><h3>开发日记</h3><ul>" +
    		"<li style='color: green'>"+
		    "用户管理：完成，待测试</li><li style='color: green'>部门管理：完成，待测试</li>" +
		    "<li style='color: green'>"+
		    "角色管理：完成，待测试</li><li style='color: green'>菜单管理：完成，待测试</li>" +
		    "<li style='color: green'>"+
		    "权限管理：完成，待测试 2016年5月29日17:30:00</li>" +
		    "<li style='color: green'>消息：完成，待测试</li>" +
		    "<li style='color: red'>学籍管理：开发中</li>" +
		    "<li style='color: red'>各功能的启动与禁用功能完善，分配关系一旦被删除，关联关系也需要被删除</li>" +
		    "<li>数据字典</li>" +
		    "<li>其他待完善……</li>"+
		    "</ul><h3>异常待解决</h3>" +
		    "<ul type='circle'>" +
		    "<li style='color: green'>session超时或者为空，页面自动跳到登录页面（已解决）</li>" +
		    "<li>区别关闭、刷新浏览器事件（无法做到）</li>" +
		    "<li>……</li>" +
		    "</ul></div>",
    
    shortBogusMarkup: '<p>啦啦啦</p><p>啦啦啦</p>'
}); 
