package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.Permission;

public interface PermissionService {

	List<Permission> findPermissionByPermission(Permission permission);
	
	boolean savePermission(Context context);
	
	void updatePermission(Context context);
	
	Permission getPermissionByPermissionId(Context context);
	
	void delPermissionByPermissionId(Context context);
	
	public Permission getPermissionByPermissionId(String permissionId);
	
	List<Permission> findPermissionByPID(List<String> list);
}
