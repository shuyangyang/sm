package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.Dept;


/**
 * 部门Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年10月7日下午7:19:39
 *
 */
public interface DeptService {
	
	List<Dept> findDepts();
	
	List<Dept> findDeptByDeptFatherId(String deptFatherId);
	
	List<Dept> findDepts(Dept dept);
	
	void saveDept(Dept dept);
	
	void deleteDeptByDeptId(Dept dept);
	
	void updateDeptByDeptId(Dept dept);
	
	List<Dept> findDeptByUserId(String userId);
	
	long deptCount(String user_id);
}
