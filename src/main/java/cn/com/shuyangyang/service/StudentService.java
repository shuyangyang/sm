package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.Student;


/**
 * 学籍Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年10月7日下午7:19:39
 *
 */
public interface StudentService {
	
	void saveStudent(Student stu);
	
	List<Student> findStudentByPage(int index, int pageSize);
}
