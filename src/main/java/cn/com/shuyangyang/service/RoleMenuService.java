package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.RoleMenu;

public interface RoleMenuService {

	public void saveRoleMenu(RoleMenu roleMenu);
	
	public List<RoleMenu> getRoleMenuByUserId(String roleId);
	
	public void deleteByUserId(String roleId);
}
