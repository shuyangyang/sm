package cn.com.shuyangyang.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.UserMessage;


/**
 * 用户消息Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年10月7日下午7:19:39
 *
 */
public interface UserMessageService {
	
	void saveUserMessage(UserMessage usermessage);

	void updateUserMessageById(UserMessage usermessage);

	void deleteUserMessageById(String id);

	UserMessage findUserMessAge(UserMessage usermessage);

	List<UserMessage> findAll();
	
	/** 分页查询 */
	List<UserMessage> findUserMessAgeByPage(@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 用户未读消息数 */
	int findUnreadCountByUser(String UserId);
	
	/** 用户收件箱消息列表 */
	List<UserMessage> getAllUncollectedMail(String receverid_user,int index,int page);
	
	/** 用户草稿箱消息列表 */
	List<UserMessage> getDraFtsMail(String receverid_user,int index,int page);
	
	/** 用户已发送消息列表 */
	List<UserMessage> getSentMessageMail(String receverid_user,int index,int page);
	
	/** 用户已删除消息列表 */
	List<UserMessage> getDeletedMessageMail(String receverid_user,int index,int page);
	
	/** 查询用户未读消息 */
	List<UserMessage> findUnreadListByUser(UserMessage um);
	
	/** 查询用户收件箱消息 */
	List<UserMessage> findListByUser(String receverid_user);
}
