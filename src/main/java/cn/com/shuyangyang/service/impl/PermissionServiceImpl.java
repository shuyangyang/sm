package cn.com.shuyangyang.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.Permission;
import cn.com.shuyangyang.mappers.PermissionMapper;
import cn.com.shuyangyang.service.PermissionService;

@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
	private PermissionMapper permissionMapper;

	@Override
	public List<Permission> findPermissionByPermission(Permission permission) {
		return permissionMapper.findPermissionByPermission(permission);
	}

	@Override
	public boolean savePermission(Context context) {
		String permissionName = context.getValue("permissionName");
		String kind = context.getValue("kind");
		String value = context.getValue("value");
		String description = context.getValue("description");
		String menuId = context.getValue("menuId");
		Permission permission = new Permission();
		permission.setPermission_name(permissionName);
		permission.setKind(kind);
		permission.setValue(value);
		permission.setStatus(EnumStatus.AVAILABLE);
		permission.setDescription(description);
		permission.setPermission_fatherId(menuId);
		permission.setCreate_time(new Date());
		if(menuId.isEmpty()){
			return false;
		}else{
			permissionMapper.savePermission(permission);
			return true;
		}
	}

	@Override
	public void updatePermission(Context context) {
		String permissionId = context.getValue("permission_id");
		String permissionName = context.getValue("permission_name");
		String kind = context.getValue("kind");
		String value = context.getValue("value");
		String description = context.getValue("description");
		Permission permission = new Permission();
		permission.setPermission_id(permissionId);
		permission.setPermission_name(permissionName);
		permission.setKind(kind);
		permission.setValue(value);
		permission.setStatus(EnumStatus.AVAILABLE);
		permission.setDescription(description);
		permission.setCreate_time(new Date());
		permissionMapper.updatePermissionByPermissionId(permission);
	}

	@Override
	public Permission getPermissionByPermissionId(Context context) {
		String permissionId = context.getValue("permissionId");
		Permission permission = new Permission();
		permission.setPermission_id(permissionId);
		return permissionMapper.findPermission(permission);
	}
	
	@Override
	public Permission getPermissionByPermissionId(String permissionId) {
		Permission permission = new Permission();
		permission.setPermission_id(permissionId);
		return permissionMapper.findPermission(permission);
	}

	@Override
	public void delPermissionByPermissionId(Context context) {
		String permissionId = context.getValue("permissionIds");
		String[] permissionIds = permissionId.split(",");
		for(int i=0;i<permissionIds.length;i++){
			permissionMapper.deletePermissionByPermissionId(permissionIds[i]);
		}
	}

	@Override
	public List<Permission> findPermissionByPID(List<String> list) {
		// TODO Auto-generated method stub
		return permissionMapper.findPermissionByPID(list);
	}

}
