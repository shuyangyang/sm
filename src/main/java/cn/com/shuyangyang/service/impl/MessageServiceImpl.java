package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.MessAge;
import cn.com.shuyangyang.domain.UserMessage;
import cn.com.shuyangyang.mappers.MessageMapper;
import cn.com.shuyangyang.service.MessageService;
import cn.com.shuyangyang.service.UserMessageService;
import cn.com.shuyangyang.util.BaseUtil;

/**消息
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年9月22日 下午2:02:38
 */
@Service("messageService")
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private MessageMapper messageMapper;
	
	@Autowired
	private UserMessageService userMessageService;
	
	@Override
	public void saveMessage(MessAge message) {
		messageMapper.saveMessage(message);
	}

	@Override
	public void updateMessageByMessageId(MessAge message) {
		messageMapper.updateMessageByMessageId(message);
	}

	@Override
	public void deleteMessageBymessageId(Context context) {
		String userId = context.getValue("messageId");
		String[] userIds = userId.split(",");
		for(int i=0;i<userIds.length;i++){
			MessAge msa = new MessAge();
	    	msa.setMessage_id(userIds[i]);
	    	MessAge msage = this.findMessAge(msa);
	    	if(msage!=null){
	    		msage.setMessage_type(EnumStatus.DELETE);
	    		this.updateMessageByMessageId(msage);
	    		
	    		//更新用户-消息
	    		UserMessage usermessage = new UserMessage();
	    		usermessage.setMessage_id(userIds[i]);
	    		String loginName = context.getValue("loginName");
	    		usermessage.setReceverid_user(loginName);
	    		usermessage = userMessageService.findUserMessAge(usermessage);
	    		usermessage.setSend_status(EnumStatus.AVAILABLE);
	    		userMessageService.updateUserMessageById(usermessage);
	    	}
		}
	}

	@Override
	public MessAge findMessAge(MessAge message) {
		return messageMapper.findMessAge(message);
	}

	@Override
	public List<MessAge> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MessAge> findMessAgeByPage(int index, int page) {
		// TODO Auto-generated method stub
		return null;
	}

}

