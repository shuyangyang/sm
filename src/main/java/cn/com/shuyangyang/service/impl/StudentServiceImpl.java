package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.Student;
import cn.com.shuyangyang.mappers.StudentMapper;
import cn.com.shuyangyang.service.StudentService;

/**
 * 学籍接口实现类
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2017年3月9日 下午1:27:07
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentMapper studentMapper;
	
	@Override
	public void saveStudent(Student stu) {
		studentMapper.saveStudent(stu);
	}

	@Override
	public List<Student> findStudentByPage(int index, int pageSize) {
		return studentMapper.findStudentByPage(index, pageSize);
	}

}

