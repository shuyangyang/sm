package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.RoleMenu;
import cn.com.shuyangyang.mappers.RoleMenuMapper;
import cn.com.shuyangyang.service.RoleMenuService;

@Service("roleMenuService")
public class RoleMenuServiceImpl implements RoleMenuService{
	
	@Autowired
	private RoleMenuMapper roleMenuMapper;

	@Override
	public void saveRoleMenu(RoleMenu roleMenu) {
		roleMenuMapper.saveRoleMenu(roleMenu);
	}

	@Override
	public List<RoleMenu> getRoleMenuByUserId(String roleId) {
		RoleMenu roleMenu = new RoleMenu();
		roleMenu.setRole_menu_id(null);
		roleMenu.setRole_id(roleId);
		return roleMenuMapper.findRoleMenu(roleMenu);
	}

	@Override
	public void deleteByUserId(String roleId) {
		RoleMenu roleMenu = new RoleMenu();
		roleMenu.setRole_menu_id(null);
		roleMenu.setRole_id(roleId);
		List<RoleMenu> roleMens = getRoleMenuByUserId(roleId);
		if(roleMens!=null&&roleMens.size()>0){
			roleMenuMapper.deleteRoleMenuByRMID(roleMenu);
		}
	}

}
