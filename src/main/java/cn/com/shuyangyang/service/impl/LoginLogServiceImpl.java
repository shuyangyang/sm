package cn.com.shuyangyang.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.mappers.LoginLogMapper;
import cn.com.shuyangyang.service.LoginLogService;

@Service("loginLogService")
public class LoginLogServiceImpl implements LoginLogService {

	@Autowired
	private LoginLogMapper loginLogMapper;
	
	@Override
	public void writeLoginLog(LoginLog loginLog) {
		loginLog.setUpdateTime(new Date());
		loginLogMapper.saveLoginLog(loginLog);
	}

	@Override
	public void updateLoginLogData(LoginLog loginLog) {
		loginLog.setUpdateTime(new Date());
		loginLogMapper.updateLoginLogByLoginLogtId(loginLog);
	}

	@Override
	public LoginLog searchLoginLog(LoginLog loginLog) {
		LoginLog llg = loginLogMapper.findLoginLog(loginLog);
		return llg;
	}

	/**
	 * 获取上次登录IP、时间
	 * 设置本次登录用户姓名、角色、部门
	 */
	@Override
	public LoginLog findLoginLogInfo(LoginLog loginLog) {
		List<LoginLog> loginLogs = loginLogMapper.findLoginLogInfo(loginLog);
		LoginLog loginLogNew = new LoginLog();
		int loginLogCount = loginLogs.size();
		if(loginLogCount>=2){
			loginLogNew = loginLogs.get(1);
			loginLogNew.setUserName(loginLog.getUserName());
			loginLogNew.setDeptName(loginLog.getDeptName());
			loginLogNew.setRoleName(loginLog.getRoleName());
		}else if(loginLogCount>0){
			loginLogNew = loginLogs.get(0);
			loginLogNew.setUserName(loginLog.getUserName());
			loginLogNew.setDeptName(loginLog.getDeptName());
			loginLogNew.setRoleName(loginLog.getRoleName());
		}else{
			loginLogNew = loginLog;
		}
		return loginLogNew;
	}

}
