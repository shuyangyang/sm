package cn.com.shuyangyang.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.controller.response.ApplicationErrorResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.domain.User;
import cn.com.shuyangyang.mappers.LoginLogMapper;
import cn.com.shuyangyang.mappers.UserMapper;
import cn.com.shuyangyang.service.UserService;

/**
 * 用户业务实现
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月29日下午8:16:26
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private LoginLogMapper loginLogMapper;

	@Override
	public User Login(Context context) {
		String loginName = context.getValue("loginname");
		String loginPassword = context.getValue("password");
		User user = new User();
		user.setUser_id(null);
		user.setLogin_name(loginName);
		user.setLogin_password(loginPassword);
		user.setUser_status(EnumStatus.AVAILABLE);
		return userMapper.findUser(user);
	}
	
	@Override
	public User getUserByLoginName(Context context){
		String loginName = context.getValue("loginName");
		User user = new User();
		user.setUser_id(null);
		user.setLogin_name(loginName);
		return userMapper.findUser(user);
	}

	@Override
	public Response chanPassword(LoginLog loginLogInfo,Context context) {
		String loginName = loginLogInfo.getLoginName();
		String newpwd = context.getValue("newpwd");
		String oldpwd = context.getValue("oldpwd");
		User user = new User();
		user.setUser_id(null);
		user.setLogin_name(loginName);
		user.setLogin_password(oldpwd);
		user = userMapper.findUser(user);
		if(user!=null){
			user.setLogin_password(newpwd);
			user.setUpdate_time(new Date());
			userMapper.updateUserByUserId(user);
			return new NormalResponse();
		}else{
			return new ApplicationErrorResponse("原密码输入错误,请重新输入！");
		}
	}

	@Override
	public void NormalLoginOut(LoginLog loginLogInfo) {
		loginLogInfo.setStatus(EnumStatus.LOGOUTED);
		loginLogInfo.setLogoutTime(new Date());
		loginLogInfo.setUpdateTime(new Date());
		loginLogMapper.updateLoginLogByLoginLogtId(loginLogInfo);
	}

	@Override
	public List<User> FindUsersByPage(int index, int pageSize) {
		return userMapper.findUserByPage(index, pageSize);
	}

	@Override
	public long UserCount() {
		return userMapper.findAll().size();
	}

	@Override
	public void addUser(Context context) {
		String userName = context.getValue("userName");
		String newpwd = context.getValue("newpwd");
		String loginName = context.getValue("loginName");
		User user = new User();
		user.setUser_name(userName);
		user.setLogin_name(loginName);
		user.setLogin_password(newpwd);
		user.setUser_status(EnumStatus.AVAILABLE);
		user.setUpdate_time(new Date());
		userMapper.saveUser(user);
	}

	@Override
	public void crossUser(Context context) {
		String loginNamest = context.getValue("loginName");
		String loginNames[] = loginNamest.split(",");
		for(int i = 0; i<loginNames.length;i++){
			User user = new User();
			user.setUser_id(null);
			user.setLogin_name(loginNames[i]);
			user.setUser_status(EnumStatus.DISABLED);
			user.setUpdate_time(new Date());
			userMapper.updateUserByLoginName(user);
		}
	}

	@Override
	public void tickUser(Context context) {
		String loginNamest = context.getValue("loginName");
		String loginNames[] = loginNamest.split(",");
		for(int i = 0; i<loginNames.length;i++){
			User user = new User();
			user.setUser_id(null);
			user.setLogin_name(loginNames[i]);
			user.setUser_status(EnumStatus.AVAILABLE);
			user.setUpdate_time(new Date());
			userMapper.updateUserByLoginName(user);
		}
	}

	@Override
	public void updateUser(Context context) {
		String userId = context.getValue("user_id");
		String userName = context.getValue("user_name");
		String loginName = context.getValue("loginName");
		String loginPassword = context.getValue("login_password");
		String userStatus = context.getValue("user_status");
		User user = new User();
		user.setUser_id(userId);
		user.setUser_name(userName);
		user.setLogin_name(loginName);
		user.setLogin_password(loginPassword);
		user.setUser_status(EnumStatus.AVAILABLE.toString().equals(userStatus)?EnumStatus.AVAILABLE:EnumStatus.DISABLED);
		user.setUpdate_time(new Date());
		userMapper.updateUserByUserId(user);
	}

	@Override
	public void delUserByUserId(Context context) {
		String userId = context.getValue("userIds");
		String[] userIds = userId.split(",");
		for(int i=0;i<userIds.length;i++){
			userMapper.deleteUserByUserId(userIds[i]);
		}
	}

	@Override
	public User getUserByLoginName(String loginName) {
		User user = new User();
		user.setUser_id(null);
		user.setLogin_name(loginName);
		return userMapper.findUser(user);
	}

	@Override
	public void addUser(User user) {
		user.setUpdate_time(new Date());
		user.setCreate_time(new Date());
		userMapper.saveUser(user);
	}
}
