package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.MenuPermission;
import cn.com.shuyangyang.mappers.MenuPermissionMapper;
import cn.com.shuyangyang.service.MenuPermissionService;

@Service("menuPermissionService")
public class MenuPermissionServiceImpl implements MenuPermissionService {
	
	@Autowired
	private MenuPermissionMapper menuPermissionMapper;

	@Override
	public void saveMenuPermission(MenuPermission menuPermission) {
		menuPermissionMapper.saveMenuPermission(menuPermission);
	}

	@Override
	public List<MenuPermission> findMenuPermission(MenuPermission menuPermission) {
		return menuPermissionMapper.findMenuPermission(menuPermission);
	}

	@Override
	public void deleteMenuPermissionByParams(String menId, String roleId) {
		MenuPermission menuPermission = new MenuPermission();
		menuPermission.setMenu_id(menId);
		menuPermission.setRole_id(roleId);
		menuPermissionMapper.deleteMenuPermissionByParams(menuPermission);
	}

	@Override
	public List<MenuPermission> findMenuPermissionBypar(List<String> list, List<String> list2) {
		return menuPermissionMapper.findMenuPermissionBypar(list, list2);
	}

}
