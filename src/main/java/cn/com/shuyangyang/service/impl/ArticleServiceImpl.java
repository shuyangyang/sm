package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.Article;
import cn.com.shuyangyang.mappers.ArticleMapper;
import cn.com.shuyangyang.service.ArticleService;

/**微博
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年10月10日 下午6:13:16
 */
@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleMapper articleMapper;
	
	@Override
	public List<Article> findAll(int index, int page) {
		return articleMapper.findAll(index, page);
	}

	@Override
	public int findAllCount() {
		return articleMapper.findAllCount();
	}

	@Override
	public void deleteArticle(String articleId) {
		articleMapper.deleteArticle(articleId);
	}

}

