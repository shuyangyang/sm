package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.UserDept;
import cn.com.shuyangyang.mappers.UserDeptMapper;
import cn.com.shuyangyang.service.UserDeptService;

@Service("userDeptService")
public class UserDeptServiceImpl implements UserDeptService {
	
	@Autowired
	private UserDeptMapper userDeptMapper;

	@Override
	public void saveUserDept(UserDept userDept) {
		userDeptMapper.saveUserDept(userDept);
	}

	@Override
	public List<UserDept> getUserDeptByUserId(String userId) {
		UserDept userDept = new UserDept();
		userDept.setUser_dept_id(null);
		userDept.setUser_id(userId);
		return userDeptMapper.findUserDept(userDept);
	}

	@Override
	public void deleteByUserId(String userId) {
		UserDept userDept = new UserDept();
		userDept.setUser_dept_id(null);
		userDept.setUser_id(userId);
		List<UserDept> userDepts = getUserDeptByUserId(userId);
		if(userDepts!=null&&userDepts.size()>0){
			userDeptMapper.deleteUserDeptByUIDorDptID(userDept);
		}
	}

}
