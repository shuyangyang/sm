package cn.com.shuyangyang.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.Menu;
import cn.com.shuyangyang.domain.MenuTree;
import cn.com.shuyangyang.mappers.MenuMapper;
import cn.com.shuyangyang.service.MenuService;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuMapper menuMapper;
	
	@Override
	public List<Menu> FindMenus() {
		List<Menu> menus = menuMapper.menuFather();
		List<Menu> menusNew = new ArrayList<Menu>();
		for(Menu menuNode:menus){
			List<Menu> menusNodes = menuMapper.findMenuByFatherId(menuNode.getMenu_id());
			menuNode.setChildren(menusNodes);
			menusNew.add(menuNode);
		}
		
		return menusNew;
	}

	@Override
	public Menu findMenuByMenuId(Context context) {
		String menuName = context.getValue("menuId");
		return menuMapper.findMenuByMenuId(menuName);
	}

	@Override
	public void updateMenuByMenuId(Context context) {
		String menuId = context.getValue("menu_id");
		String menuName = context.getValue("text");
		String menuUrl = context.getValue("url");
		String iconCls = context.getValue("iconCls");
		String description = context.getValue("description");
		Menu menu = new Menu();
		menu.setMenu_id(menuId);
		menu.setText(menuName);
		menu.setUrl(menuUrl);
		menu.setIconCls(iconCls);
		menu.setDescription(description);
		menu.setUpdate_time(new Date());
		menuMapper.updateMenuByMenuId(menu);
	}

	@Override
	public boolean addMenu(Context context) {
		String menuId = context.getValue("menuId");
		String menuFatherId = context.getValue("menuFatherId");
		String menuName = context.getValue("text");
		boolean leaf = true;
		String nodeType = context.getValue("nodeType");
		Menu meu = menuMapper.findMenuByMenuText(menuName);
		if(meu!=null){
			return false;
		}else{
			Menu menu = new Menu();
			if(nodeType.equals("father")){
				menuFatherId = "-1";
				leaf =false;
				menu.setText(menuName);
				menu.setFatherId(menuFatherId);
				menu.setStatus(EnumStatus.AVAILABLE);
				menu.setLeaf(leaf);
				menu.setCreate_time(new Date());
				menu.setUpdate_time(new Date());
				menuMapper.saveMenu(menu);
				return true;
			}else{
				//只允许到2级菜单下，不支持3级或者更高
//				if(menuFatherId!="-1"){
//					return false;
//				}else{
					menuFatherId=menuId;
					leaf =true;
					menu.setText(menuName);
					menu.setFatherId(menuFatherId);
					menu.setStatus(EnumStatus.AVAILABLE);
					menu.setLeaf(leaf);
					menu.setCreate_time(new Date());
					menu.setUpdate_time(new Date());
					menuMapper.saveMenu(menu);
					return true;
//				}
			}
			
		}
	}

	@Override
	public void deleteMenuByMenuId(Context context) {
		String menuId = context.getValue("menu_id");
		menuMapper.deleteMenuByMenuId(menuId);
	}

	@Override
	public List<MenuTree> FindMenusTree() {
		List<MenuTree> menus = menuMapper.menuTreeFather();
		List<MenuTree> menusNew = new ArrayList<MenuTree>();
		for(MenuTree menuNode:menus){
			List<MenuTree> menusNodes = menuMapper.findMenuTreeByFatherId(menuNode.getMenu_id());
			menuNode.setChildren(menusNodes);
			menusNew.add(menuNode);
		}
		
		return menusNew;
	}

	@Override
	public List<Menu> FindMenusAll() {
		return menuMapper.findAllByPage();
	}

	@Override
	public long MenusCount() {
		return menuMapper.findAll().size();
	}

	@Override
	public List<Menu> findMenuByPermiParams(String userId) {
		return menuMapper.findMenuByPermiParams(userId);
	}
}
