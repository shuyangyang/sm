package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.DeptRole;
import cn.com.shuyangyang.mappers.DeptRoleMapper;
import cn.com.shuyangyang.service.DeptRoleService;

@Service("deptRoleService")
public class DeptRoleServiceImpl implements DeptRoleService {

	@Autowired
	private DeptRoleMapper deptRoleMapper;
	
	@Override
	public void saveDeptRole(DeptRole deptRole) {
		deptRoleMapper.saveDeptRole(deptRole);
	}

	@Override
	public List<DeptRole> getDeptRoleByDeptId(String deptId) {
		DeptRole deptRole = new DeptRole();
		deptRole.setDept_id(deptId);
		deptRole.setDept_role_id(null);
		List<DeptRole> deptRoles = deptRoleMapper.findDeptRole(deptRole);
		return deptRoles;
	}

	@Override
	public void deleteByDeptId(String deptId) {
		DeptRole deptRole = new DeptRole();
		deptRole.setDept_id(deptId);
		deptRole.setDept_role_id(null);
		List<DeptRole> deptRoles = getDeptRoleByDeptId(deptId);
		if(deptRoles!=null&&deptRoles.size()>0){
			deptRoleMapper.deleteDeptRoleByDeptRoleID(deptRole);
		}
	}

	@Override
	public List<DeptRole> findDeptRoleByDeptId(List<String> list) {
		// TODO Auto-generated method stub
		return deptRoleMapper.findDeptRoleByDeptId(list);
	}

}
