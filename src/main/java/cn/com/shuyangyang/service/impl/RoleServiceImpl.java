package cn.com.shuyangyang.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.Role;
import cn.com.shuyangyang.mappers.RoleMapper;
import cn.com.shuyangyang.service.RoleService;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleMapper roleMapper;

	@Override
	public List<Role> FindRolesByPage(int index, int pageSize) {
		return roleMapper.findRoleByPage(index, pageSize);
	}

	@Override
	public long getRolesCount() {
		return roleMapper.findAll().size();
	}

	@Override
	public void saveRole(Context context) {
		Role role = parameterFactory(context,"save");
		roleMapper.saveRole(role);
	}

	@Override
	public Role getRoleByRoleName(String roleName) {
		Role role = new Role();
		role.setRole_id(null);
		role.setRole_name(roleName);
		return roleMapper.findRole(role);
	}

	@Override
	public void updateRoleByRoleId(Context context) {
		Role role = parameterFactory(context,"update");
		roleMapper.updateRoleByRoleId(role);
	}

	@Override
	public Role findRole(Context context) {
		Role role = parameterFactory(context,"find");
		return roleMapper.findRole(role);
	}
	
	@Override
	public void delRoleByRoleIds(Context context) {
		String roleId = context.getValue("roleIds");
		String[] roleIds = roleId.split(",");
		for(int i=0;i<roleIds.length;i++){
			roleMapper.deleteRoleByRoleId(roleIds[i]);
		}
	}
	
	@Override
	public List<Role> findRoleByDeptId(String deptId) {
		return roleMapper.findRoleByDeptId(deptId);
	}

	/**
	 * 参数工厂，入参进来，自动包装成对象供外部使用
	 * @param context
	 * @return
	 */
	public Role parameterFactory(Context context,String method){
		String roleId = context.getValue("roleId")==null?context.getValue("role_id"):context.getValue("roleId");
		String roleName = context.getValue("roleName")==null?context.getValue("role_name"):context.getValue("roleName");
		String roleStatus = context.getValue("roleStatus")==null?context.getValue("status"):context.getValue("roleStatus");
		String description = context.getValue("description");
		String createTime = context.getValue("createTime")==null?context.getValue("create_time"):context.getValue("createTime");
		String updateTime = context.getValue("updateTime");
		Role role = new Role();
		if(StringUtils.hasLength(roleId)){
			role.setRole_id(roleId);
		}
		if(StringUtils.hasLength(roleName)){
			role.setRole_name(roleName);
		}
		if(StringUtils.hasLength(roleStatus)){
			if(roleStatus.equals("1")||EnumStatus.AVAILABLE.toString().equals(roleStatus)){
				role.setStatus(EnumStatus.AVAILABLE);
			}else{
				role.setStatus(EnumStatus.DISABLED);
			}
		}
		if(StringUtils.hasLength(description)){
			role.setDescription(description);
		}
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(StringUtils.hasLength(createTime)){
			try {
				Long longtime = Long.parseLong(createTime);
				role.setCreate_time(sd.parse(sd.format(longtime)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(!StringUtils.pathEquals(method, "find")){
			role.setCreate_time(new Date());
		}
		if(StringUtils.hasLength(updateTime)){
			try {
				Long longtime = Long.parseLong(updateTime);
				role.setUpdate_time(sd.parse(sd.format(longtime)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(!StringUtils.pathEquals(method, "find")){
			role.setUpdate_time(new Date());
		}
		return role;
	}
}
