package cn.com.shuyangyang.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.UserMessage;
import cn.com.shuyangyang.mappers.UserMessageMapper;
import cn.com.shuyangyang.service.UserMessageService;
import cn.com.shuyangyang.util.BaseUtil;
import cn.com.shuyangyang.util.StringControllerUtil;

/**
 * 用户消息
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年9月13日 下午1:44:12
 */
@Service("userMessageService")
public class UserMessageServiceImpl implements UserMessageService {

	@Autowired
	private UserMessageMapper userMessageMapper;
	
	@Override
	public void saveUserMessage(UserMessage usermessage) {
		userMessageMapper.saveUserMessage(usermessage);
	}

	@Override
	public void updateUserMessageById(UserMessage usermessage) {
		userMessageMapper.updateUserMessageById(usermessage);
	}

	@Override
	public void deleteUserMessageById(String id) {
		userMessageMapper.deleteUserMessageById(id);
	}

	@Override
	public UserMessage findUserMessAge(UserMessage usermessage) {
		return userMessageMapper.findUserMessAge(usermessage);
	}

	@Override
	public List<UserMessage> findAll() {
		return userMessageMapper.findAll();
	}

	@Override
	public List<UserMessage> findUserMessAgeByPage(int index, int page) {
		return userMessageMapper.findUserMessAgeByPage(index, page);
	}

	@Override
	public int findUnreadCountByUser(String UserId) {
		int unReadCount = 0;
		unReadCount = userMessageMapper.findUnreadCountByUser(UserId);
		return unReadCount;
	}

	@Override
	public List<UserMessage> getAllUncollectedMail(String receverid_user,int index,int page) {
		List<UserMessage> userMessages = userMessageMapper.getAllUncollectedMail(receverid_user,index,page);
		List<UserMessage> newuserMessages = new ArrayList<UserMessage>();
		if(!BaseUtil.isSpace(userMessages)){
			for(UserMessage us:userMessages){
				String htmlcontent = us.getMessage_text();
				String txtcontent = htmlcontent.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
				txtcontent = txtcontent.replaceAll("\\s*|\t|\r|\n", "");//去除字符串中的空格,回车,换行符,制表符
				txtcontent = StringControllerUtil.left(txtcontent,30);
				us.setMessage_text(txtcontent);
				newuserMessages.add(us);
			}
			return newuserMessages;
		}else{
			return null;
		}
	}

	@Override
	public List<UserMessage> getDraFtsMail(String receverid_user, int index, int page) {
		List<UserMessage> userMessages = userMessageMapper.getDraFtsMail(receverid_user, index, page);
		List<UserMessage> newuserMessages = new ArrayList<UserMessage>();
		if(!BaseUtil.isSpace(userMessages)){
			for(UserMessage us:userMessages){
				String htmlcontent = us.getMessage_text();
				String txtcontent = htmlcontent.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
				txtcontent = txtcontent.replaceAll("\\s*|\t|\r|\n", "");//去除字符串中的空格,回车,换行符,制表符
				txtcontent = StringControllerUtil.left(txtcontent,30);
				us.setMessage_text(txtcontent);
				newuserMessages.add(us);
			}
			return newuserMessages;
		}else{
			return null;
		}
	}

	@Override
	public List<UserMessage> getSentMessageMail(String receverid_user, int index, int page) {
		List<UserMessage> userMessages = userMessageMapper.getSentMessageMail(receverid_user, index, page);
		List<UserMessage> newuserMessages = new ArrayList<UserMessage>();
		if(!BaseUtil.isSpace(userMessages)){
			for(UserMessage us:userMessages){
				String htmlcontent = us.getMessage_text();
				String txtcontent = htmlcontent.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
				txtcontent = txtcontent.replaceAll("\\s*|\t|\r|\n", "");//去除字符串中的空格,回车,换行符,制表符
				txtcontent = StringControllerUtil.left(txtcontent,30);
				us.setMessage_text(txtcontent);
				us.setRead_status(EnumStatus.AVAILABLE);
				newuserMessages.add(us);
			}
			return newuserMessages;
		}else{
			return null;
		}
	}

	@Override
	public List<UserMessage> getDeletedMessageMail(String receverid_user, int index, int page) {
		List<UserMessage> userMessages = userMessageMapper.getDeletedMessageMail(receverid_user, index, page);
		List<UserMessage> newuserMessages = new ArrayList<UserMessage>();
		if(!BaseUtil.isSpace(userMessages)){
			for(UserMessage us:userMessages){
				String htmlcontent = us.getMessage_text();
				String txtcontent = htmlcontent.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
				txtcontent = txtcontent.replaceAll("\\s*|\t|\r|\n", "");//去除字符串中的空格,回车,换行符,制表符
				txtcontent = StringControllerUtil.left(txtcontent,30);
				us.setMessage_text(txtcontent);
				newuserMessages.add(us);
			}
			return newuserMessages;
		}else{
			return null;
		}
	}

	@Override
	public List<UserMessage> findUnreadListByUser(UserMessage um) {
		return userMessageMapper.findUnreadListByUser(um);
	}

	@Override
	public List<UserMessage> findListByUser(String receverid_user) {
		return userMessageMapper.findListByUser(receverid_user);
	}

}

