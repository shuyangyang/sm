package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.Dept;
import cn.com.shuyangyang.mappers.DeptMapper;
import cn.com.shuyangyang.service.DeptService;

@Service("deptService")
public class DeptServiceImpl implements DeptService {
	
	@Autowired
	private DeptMapper deptMapper;

	@Override
	public List<Dept> findDepts() {
		return deptMapper.findAll();
	}

	@Override
	public List<Dept> findDeptByDeptFatherId(String deptFatherId) {
		return deptMapper.findDeptByFatherId(deptFatherId);
	}

	@Override
	public List<Dept> findDepts(Dept dept) {
		return deptMapper.findDept(dept);
	}

	@Override
	public void saveDept(Dept dept) {
		deptMapper.saveDept(dept);
	}

	@Override
	public void deleteDeptByDeptId(Dept dept) {
		deptMapper.deleteDeptByDeptId(dept);
	}

	@Override
	public void updateDeptByDeptId(Dept dept) {
		deptMapper.updateDeptByDeptId(dept);
	}

	@Override
	public List<Dept> findDeptByUserId(String user_id) {
		return deptMapper.findDeptByUserId(user_id);
	}

	@Override
	public long deptCount(String user_id) {
		return deptMapper.findDeptByUserIdCount(user_id).size();
	}
}
