package cn.com.shuyangyang.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.GradeClass;
import cn.com.shuyangyang.mappers.GradeClassMapper;
import cn.com.shuyangyang.service.GradeClassService;
import cn.com.shuyangyang.util.BaseUtil;
import cn.com.shuyangyang.util.DoNumberUtil;

/** 年级班级
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月4日 下午4:16:34
 */
@Service("gradeClassService")
public class GradeClassServiceImpl implements GradeClassService {

	@Autowired
	private GradeClassMapper gradeClassMapper;
	 
	@Override
	public void autoGenerate(List<GradeClass> gcs) {
		gradeClassMapper.autoGenerate(gcs);
	}

	@Override
	public void updateInfo(GradeClass gradeClass) {
		gradeClassMapper.updateInfo(gradeClass);
	}

	@Override
	public List<GradeClass> findAll(int index, int page) {
		return gradeClassMapper.findAll(index, page);
	}

	@Override
	public int findAllCount() {
		return gradeClassMapper.findAllCount();
	}

	@Override
	public void deleteGradeClassById(String id) {
		String[] userIds = id.split(",");
		for(int i=0;i<userIds.length;i++){
			gradeClassMapper.deleteGradeClassById(DoNumberUtil.intNullDowith(userIds[i]));
		}
	}

	@Override
	public GradeClass findInfoById(int id) {
		if(!BaseUtil.isSpace(id)){
			return gradeClassMapper.findInfoById(id);
		}
		return null;
	}

	@Override
	public List<GradeClass> findAllNotPage() {
		return gradeClassMapper.findAllNotPage();
	}

}

