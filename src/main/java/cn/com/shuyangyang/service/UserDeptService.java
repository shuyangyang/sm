package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.UserDept;

public interface UserDeptService {

	public void saveUserDept(UserDept userDept);
	
	public List<UserDept> getUserDeptByUserId(String userId);
	
	public void deleteByUserId(String userId);
}
