package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.GradeClass;

/**班级年级
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月4日 下午4:15:24
 */
public interface GradeClassService {
	
	public void autoGenerate(List<GradeClass> gcs);
	
	public void updateInfo(GradeClass gradeClass);
	
	public List<GradeClass> findAll(int index,int page);
	
	public List<GradeClass> findAllNotPage();
	
	public int findAllCount();
	
	public void deleteGradeClassById(String id);
	
	public GradeClass findInfoById(int id);
}

