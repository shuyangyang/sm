package cn.com.shuyangyang.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.Menu;
import cn.com.shuyangyang.domain.MenuTree;

public interface MenuService {

	List<Menu> FindMenus();
	
	List<MenuTree> FindMenusTree();
	
	Menu findMenuByMenuId(Context context);
	
	void updateMenuByMenuId(Context context);
	
	boolean addMenu(Context context);
	
	void deleteMenuByMenuId(Context context);
	
	List<Menu> FindMenusAll();
	
	long MenusCount();
	
	List<Menu> findMenuByPermiParams(String userId);
}
