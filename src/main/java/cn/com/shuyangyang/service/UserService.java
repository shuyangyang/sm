package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.domain.User;

/**
 * 用户Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月29日下午8:08:34
 *
 */
public interface UserService {

	User Login(Context context);
	
	Response chanPassword(LoginLog loginLogInfo,Context context);
	
	void NormalLoginOut(LoginLog loginLogInfo);
	
	List<User> FindUsersByPage(int index, int pageSize);
	
	long UserCount();
	
	void addUser(Context context);
	
	void addUser(User user);

	User getUserByLoginName(Context context);
	
	User getUserByLoginName(String loginName);
	
	void crossUser(Context context);
	
	void tickUser(Context context);
	
	void updateUser(Context context);
	
	void delUserByUserId(Context context);
	
}
