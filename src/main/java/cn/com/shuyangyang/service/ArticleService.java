package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.Article;

public interface ArticleService {
	
	List<Article> findAll(int index,int page);
	
	int findAllCount();
	
	void deleteArticle(String articleId);
}
