package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.MenuPermission;

public interface MenuPermissionService {

	void saveMenuPermission(MenuPermission menuPermission);
	
	List<MenuPermission> findMenuPermission(MenuPermission menuPermission);
	
	void deleteMenuPermissionByParams(String menId,String roleId);
	
	List<MenuPermission> findMenuPermissionBypar(List<String> list,List<String> list2);
}
