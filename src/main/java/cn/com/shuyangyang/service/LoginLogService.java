package cn.com.shuyangyang.service;

import cn.com.shuyangyang.domain.LoginLog;

public interface LoginLogService {

	void writeLoginLog(LoginLog loginLog);
	
	void updateLoginLogData(LoginLog loginLog);
	
	LoginLog searchLoginLog(LoginLog loginLog);
	
	LoginLog findLoginLogInfo(LoginLog loginLog);
}
