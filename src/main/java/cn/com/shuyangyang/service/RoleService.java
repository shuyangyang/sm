package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.Role;

/**
 * 角色Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年8月2日下午3:48:16
 *
 */
public interface RoleService {
	
	long getRolesCount();
	
	List<Role> FindRolesByPage(int index, int pageSize);
	
	void saveRole(Context context);
	
	Role getRoleByRoleName(String roleName);
	
	void updateRoleByRoleId(Context context);
	
	Role findRole(Context context);
	
	void delRoleByRoleIds(Context context);
	
	List<Role> findRoleByDeptId(String deptId);
	
}
