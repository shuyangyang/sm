package cn.com.shuyangyang.service;

import java.util.List;

import cn.com.shuyangyang.domain.DeptRole;

public interface DeptRoleService {

	public void saveDeptRole(DeptRole deptRole);
	
	public List<DeptRole> getDeptRoleByDeptId(String deptId);
	
	public void deleteByDeptId(String deptId);
	
	public List<DeptRole> findDeptRoleByDeptId(List<String> list);
}
