package cn.com.shuyangyang.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.MessAge;
import cn.com.shuyangyang.domain.UserMessage;


/**
 * 用户消息Service
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年10月7日下午7:19:39
 *
 */
public interface MessageService {
	
	void saveMessage(MessAge message);

	void updateMessageByMessageId(MessAge message);

	void deleteMessageBymessageId(Context context);

	MessAge findMessAge(MessAge message);

	List<MessAge> findAll();
	
	/** 分页查询 */
	List<MessAge> findMessAgeByPage(int index,int page);
}
