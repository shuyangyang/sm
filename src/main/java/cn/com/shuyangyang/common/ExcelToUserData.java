package cn.com.shuyangyang.common;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.shuyangyang.domain.User;
import cn.com.shuyangyang.mappers.UserMapper;

@Service("excelToUserData")
public class ExcelToUserData {
	
	Logger	logger	= LoggerFactory.getLogger(ExcelToUserData.class);
	
	@Autowired
	private UserMapper userMapper;
	
	public HSSFWorkbook exportUserData(){
		// 第一步，创建一个webbook，对应一个Excel文件  
        HSSFWorkbook wb = new HSSFWorkbook();  
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet  
        HSSFSheet sheet = wb.createSheet("用户信息表一");  
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short  
        HSSFRow row = sheet.createRow((int) 0);
        
        HSSFFont f  = wb.createFont();    
        f.setFontHeightInPoints((short) 12);//字号    
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//加粗   
        
        // 第四步，创建单元格，并设置值表头 设置表头居中  
        HSSFCellStyle style = wb.createCellStyle();  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式  
        style.setFont(f);
        
        HSSFCell cell = row.createCell(0);
        sheet.setColumnWidth(0, 20*256);//设置列宽，20个字符宽
        cell.setCellValue("用户名");
        cell.setCellStyle(style);
        cell = row.createCell(1);
        sheet.setColumnWidth(1, 20*256);//设置列宽，20个字符宽
        cell.setCellValue("登录名");  
        cell.setCellStyle(style);  
        cell = row.createCell(2);
        sheet.setColumnWidth(2, 20*256);//设置列宽，20个字符宽
        cell.setCellValue("登录密码");  
        cell.setCellStyle(style);  
        cell = row.createCell(3);
        sheet.setColumnWidth(3, 15*256);//设置列宽，20个字符宽
        cell.setCellValue("用户状态");
        cell.setCellStyle(style);
        cell = row.createCell(4); 
        sheet.setColumnWidth(4, 20*256);//设置列宽，20个字符宽
        cell.setCellValue("创建时间");  
        cell.setCellStyle(style);
        cell = row.createCell(5);
        sheet.setColumnWidth(5, 20*256);//设置列宽，20个字符宽
        cell.setCellValue("最后更新时间");  
        cell.setCellStyle(style);  
  
        // 第五步，写入实体数据 实际应用中这些数据从数据库得到，  
        List<User> list = userMapper.findAll();
  
        for (int i = 0; i < list.size(); i++)  
        {  
            row = sheet.createRow((int) i + 1);  
            User user = list.get(i);  
            // 第四步，创建单元格，并设置值  
            row.createCell(0).setCellValue(user.getUser_name());  
            row.createCell(1).setCellValue(user.getLogin_name());  
            row.createCell(2).setCellValue(user.getLogin_password());  
            cell = row.createCell(3);  
            cell.setCellValue(user.getUser_status().toString());  
            cell = row.createCell(4);
            cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(user.getCreate_time())); 
            cell = row.createCell(5);  
            cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(user.getUpdate_time()));  
        }
        return wb;
	}
}
