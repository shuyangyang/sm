package cn.com.shuyangyang.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

public class MyBeanFactory implements BeanFactoryAware {
	
private static BeanFactory beanFactory = null;
	
	private static MyBeanFactory instance = null;
	
	public static MyBeanFactory getInstance() {
		if(instance == null) {
			instance = beanFactory.getBean("myBeanFactory", MyBeanFactory.class);
		}
		return instance;
	}
	
	/**
    * 根据提供的bean名称得到相应的服务类     
    * @param name bean名称     
    */
	public Object getBean(String name) {
		return beanFactory.getBean(name);
	}
	
	/**
    * 根据提供的bean名称得到对应于指定类型的服务类
    * @param name bean名称
    * @param clazz 返回的bean类型,若类型不匹配,将抛出异常
    */
	public Object getBean(String name, Class<?> clazz) {
		return beanFactory.getBean(name, clazz);
	}

	@Override
	public void setBeanFactory(BeanFactory bf) throws BeansException {
		beanFactory = bf;
	}

}
