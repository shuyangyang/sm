package cn.com.shuyangyang.common.util;

import java.util.List;

public class HandleValue {
	public static void main(String[] args) {
//		Integer[] ints = {1, 2, 4, 6};
		String[] vars = {"A", "B", "C", "K"};
		System.out.println(toString(vars));
	}
	
	public static boolean isEmpty(String val){
		return (val == null || val.isEmpty());
	}

	public static boolean isEmpty(Integer val){
		return (val == null);
	}

	public static boolean isEmpty(String[] val){
		if(val == null || val.length == 0) 
			return true;
		
		for(int i=0; i<val.length; i++) {
			if(!isEmpty(val[i]))
				return false;
		}
		
		return true;
	}

	public static boolean isEmpty(Integer[] val){
		return (val == null || val.length == 0);
	}

	public static boolean isEmpty(List<?> val){
		return (val == null || val.size() == 0);
	}

	public static boolean isEmpty(Object val){
		return (val == null);
	}
	
	/**
	 * 将数组转换为字符串
	 * @param ints
	 * @return
	 */
	public static String toString(Integer[] vals) {
		StringBuffer str = null;
		
		if(isEmpty(vals)) return null;
		
		for(Integer val : vals) {
			if(str == null) {
				str = new StringBuffer(String.valueOf(val));
			} else {
				str.append(",").append(val);
			}
		}
		
		return str.toString();
	}
		
	/**
	 * 将数组转换为字符串
	 * @param ints
	 * @return
	 */
	public static String toString(String[] vals) {
		StringBuffer str = null;
		
		if(isEmpty(vals)) return null;
		
		for(String val : vals) {
			if(str == null) {
				str = new StringBuffer("'").append(val).append("'");
			} else {
				str.append(",").append("'").append(val).append("'");
			}
		}
		
		return str.toString();
	}
	
	/**
	 * 将数组转换为字符串
	 * @param ints
	 * @return
	 */
	public static String toString(List<String> vals) {
		StringBuffer str = null;
		
		if(isEmpty(vals)) return null;
		
		for(String val : vals) {
			if(str == null) {
				str = new StringBuffer("'").append(val).append("'");
			} else {
				str.append(",").append("'").append(val).append("'");
			}
		}
		
		return str.toString();
	}
}
