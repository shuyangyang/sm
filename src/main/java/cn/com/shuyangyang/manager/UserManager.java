package cn.com.shuyangyang.manager;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.domain.Dept;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.domain.Role;
import cn.com.shuyangyang.domain.User;
import cn.com.shuyangyang.service.DeptService;
import cn.com.shuyangyang.service.LoginLogService;
import cn.com.shuyangyang.service.RoleService;

/**
 * 用户相关信息存储类
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月29日下午9:06:01
 *
 */
@Repository("userManager")
public class UserManager {
	
	private String userName;
	
	private String deptName;
	
	private String roleName;
	
	private String sid;
	
	private String ip;
	
	@Autowired
	private LoginLogService loginLogService;
	
	@Autowired
	private DeptService deptService;
	
	@Autowired
	private RoleService roleService;

	public LoginLog InitUserManager(User user,Context context){
		
		List<Dept> depts = deptService.findDeptByUserId(user.getUser_id());
		StringBuffer deptNames = new StringBuffer();
		StringBuffer RoleNames = new StringBuffer();
		if(depts.size()>=0){
			for(int i=0;i<depts.size();i++){
				List<Role> roles = roleService.findRoleByDeptId(depts.get(i).getDept_id());
				if(roles.size()>=0){
					for(int j=0;j<roles.size();j++){
						if(i!=depts.size()-1){
							RoleNames.append("["+roles.get(j).getRole_name()+"]，");
						}else{
							RoleNames.append("["+roles.get(j).getRole_name()+"]");
						}
					}
					this.setDeptName(deptNames.toString());
				}
				if(i!=depts.size()-1){
					deptNames.append("["+depts.get(i).getDept_name()+"]，");
				}else{
					deptNames.append("["+depts.get(i).getDept_name()+"]");
				}
			}
			this.setDeptName(deptNames.toString());
			if(RoleNames.toString().length()>0){
				this.setRoleName(RoleNames.toString());
			}else{
				this.setRoleName("此用户暂未分配角色");
			}
		}else{
			this.setDeptName("此用户暂未分配部门");
			this.setRoleName("此用户暂未分配角色");
		}
		
		this.setUserName(user.getUser_name());
		this.setIp(context.getValue("_ip"));
		this.setSid(context.getValue("_sid"));
		LoginLog loginLog = new LoginLog();
		loginLog.setLoginName(user.getLogin_name());
		loginLog.setSid(context.getValue("_sid"));
		loginLog.setUserName(user.getUser_name());
		loginLog.setDeptName(this.getDeptName());
		loginLog.setRoleName(this.getRoleName());
		loginLog.setIp(context.getValue("_ip"));
		loginLog.setStatus(EnumStatus.LOGINNED);
		loginLog.setLoginTime(new Date());
		loginLog.setLogoutTime(null);
		loginLogService.writeLoginLog(loginLog);
		return loginLog;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}
}
