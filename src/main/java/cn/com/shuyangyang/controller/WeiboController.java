package cn.com.shuyangyang.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.Article;
import cn.com.shuyangyang.service.ArticleService;

@Controller
@RequestMapping("/weibo/")
public class WeiboController {
	Logger logger = LoggerFactory.getLogger(WeiboController.class);

	@Autowired
	private ArticleService articleService;

	@RequestMapping("showWeibos")
	@ResponseBody
	public Response showRoles(HttpServletRequest req, HttpServletResponse resp) {
		String start = req.getParameter("start");// 起始页
		String limit = req.getParameter("limit");// 每页数量
		int index = Integer.parseInt(start);
		int pageSize = Integer.parseInt(limit);
		List<Article> roles = articleService.findAll(index, pageSize);
		long total = roles.size();
		if (total > 0) {
			logger.debug("now {}", "返回微博素材数据。。。");
			return new NormalResponse(roles, (long) articleService.findAllCount());
		} else {
			logger.debug("now {}", "微博素材为空！");
			return new EmptyResponse("微博素材");
		}
	}
	
	@RequestMapping("delSucai")
	@ResponseBody
	public Response delSucai(HttpServletRequest req, HttpServletResponse resp) {
		String articleId = req.getParameter("articleId");// 素材ID
		try{
			articleService.deleteArticle(articleId);
		}catch(Exception e){
			logger.debug("now {}", "删除素材出现问题");
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		return new NormalResponse();
	}
}
