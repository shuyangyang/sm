package cn.com.shuyangyang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年9月5日 下午5:36:25
 */
@Controller
@RequestMapping("/")
public class HomeController {
	/**
	 * 欢迎页跳转到首页
	 * @return
	 */
	@RequestMapping("home")
	@ResponseBody
	public ModelAndView goHome() {
		return new ModelAndView("redirect:/Web-Admin/login.jsp");
	}
}

