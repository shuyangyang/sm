package cn.com.shuyangyang.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.RoleMenu;
import cn.com.shuyangyang.service.RoleMenuService;

@Controller
@RequestMapping("/roleMenu/")
public class RoleMenuController {

	Logger	logger	= LoggerFactory.getLogger(RoleMenuController.class);

	@Autowired
	private RoleMenuService	roleMenuService;
	
	/**
	 * 添加角色-菜单
	 * 
	 * @return
	 */
	@RequestMapping("addRoleMenu")
	@ResponseBody
	public Response addRoleMenu(HttpServletRequest req, HttpServletResponse resp) {
		try{
			String roleId = req.getParameter("roleId");
			String menuIds = req.getParameter("menuIds");
			String[] menuId =menuIds.split(",");
			//先删除之前保存的关联，重新保存勾选的
			roleMenuService.deleteByUserId(roleId);
			for(int i=0;i<menuId.length;i++){
				RoleMenu roleMenu = new RoleMenu();
				roleMenu.setRole_id(roleId);
				roleMenu.setMenu_id(menuId[i]);
				roleMenu.setCreate_time(new Date());
				roleMenu.setStatus(EnumStatus.AVAILABLE);
				roleMenuService.saveRoleMenu(roleMenu);
			}
			return new NormalResponse();
		}catch (Exception e) {
			logger.error("添加角色-菜单数据错误:" + e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		
	}
}
