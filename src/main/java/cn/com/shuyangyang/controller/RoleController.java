package cn.com.shuyangyang.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.common.RequestContext;
import cn.com.shuyangyang.controller.response.ApplicationErrorResponse;
import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.Role;
import cn.com.shuyangyang.service.RoleService;

@Controller
@RequestMapping("/role/")
public class RoleController {
	Logger	logger	= LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping("showRoles")
	@ResponseBody
	public Response showRoles(HttpServletRequest req, HttpServletResponse resp){
		String start = req.getParameter("start");//起始页
        String limit = req.getParameter("limit");//每页数量
        int index = Integer.parseInt(start);
        int pageSize = Integer.parseInt(limit);
        List<Role> roles = roleService.FindRolesByPage(index, pageSize);
        long total = roles.size();
        if(total>0){
			logger.debug("now {}" , "返回角色数据。。。");
			return new NormalResponse(roles,roleService.getRolesCount());
		}else{
			logger.debug("now {}" , "角色数据为空！");
			return new EmptyResponse("角色数据");
		}
	}
	
	@RequestMapping("addRole")
	@ResponseBody
	public Response addRole(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			Role role = roleService.getRoleByRoleName(context.getValue("roleName"));
			if(role!=null){
				return new ApplicationErrorResponse("已存在的角色名，请重新填写！");
			}
			roleService.saveRole(context);
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("添加角色环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("updateRole")
	@ResponseBody
	public Response updateRole(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			roleService.updateRoleByRoleId(context);
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("更新角色环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("loadRole")
	@ResponseBody
	public Response loadRole(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			Role role = roleService.findRole(context);
			return new NormalResponse(role);
		} catch (Exception e) {
			logger.error("读取角色环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("delRole")
	@ResponseBody
	public Response delRole(HttpServletRequest request, HttpServletResponse response){
		try {
			Context context = new RequestContext(request);
			roleService.delRoleByRoleIds(context);
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("删除角色环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
}
