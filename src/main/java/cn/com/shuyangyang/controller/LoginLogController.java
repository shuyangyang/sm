package cn.com.shuyangyang.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.service.LoginLogService;

@Controller
@RequestMapping("/loginLog/")
public class LoginLogController {
	
	Logger	logger	= LoggerFactory.getLogger(LoginLogController.class);
	
	@Autowired
	private LoginLogService loginLogService;

	@RequestMapping("info")
	@ResponseBody
	public Response showLoginLogByLoginName(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			LoginLog loginLogInfo = (LoginLog) request.getSession().getAttribute("loginLogInfo");
			loginLogInfo = loginLogService.findLoginLogInfo(loginLogInfo);
			return new NormalResponse(loginLogInfo);
		} catch (Exception e) {
			logger.error("获取用户日志相关信息环节出错：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
}
