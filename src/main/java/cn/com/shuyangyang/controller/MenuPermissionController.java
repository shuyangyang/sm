package cn.com.shuyangyang.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.DeptRole;
import cn.com.shuyangyang.domain.LoginLog;
import cn.com.shuyangyang.domain.Menu;
import cn.com.shuyangyang.domain.MenuPermission;
import cn.com.shuyangyang.domain.Permission;
import cn.com.shuyangyang.domain.User;
import cn.com.shuyangyang.domain.UserDept;
import cn.com.shuyangyang.service.DeptRoleService;
import cn.com.shuyangyang.service.MenuPermissionService;
import cn.com.shuyangyang.service.MenuService;
import cn.com.shuyangyang.service.PermissionService;
import cn.com.shuyangyang.service.UserDeptService;
import cn.com.shuyangyang.service.UserService;

@Controller
@RequestMapping("/menuPer/")
public class MenuPermissionController {
	
	Logger	logger	= LoggerFactory.getLogger(MenuPermissionController.class);
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserDeptService userDeptService;
	
	@Autowired
	private DeptRoleService deptRoleService;
	
	@Autowired
	private MenuPermissionService menuPermissionService;
	
	@Autowired
	private PermissionService permissionService;
	
	/**
	 * 获取当前用户是否具有菜单按钮权限
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getMenuPer")
	@ResponseBody
	public Response getMenuPermissionByParams(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			HttpSession session = request.getSession();
			LoginLog loginLog = (LoginLog) session.getAttribute("loginLogInfo");
			String loginName = loginLog.getLoginName();
			User user = userService.getUserByLoginName(loginName);
			List<Menu> menus = menuService.findMenuByPermiParams(user.getUser_id()); //当前用户的菜单
			if(menus!=null||menus.size()>0){
				List<UserDept> dpu = userDeptService.getUserDeptByUserId(user.getUser_id());
				if(dpu!=null||dpu.size()>0){
					List<String> list = new ArrayList<String>();
					List<String> menulist = new ArrayList<String>();
					List<String> rolelist = new ArrayList<String>();
					for(UserDept up:dpu){
						list.add(up.getDept_id());
					}
					List<DeptRole> dt = deptRoleService.findDeptRoleByDeptId(list); //当前用户的部门与角色关系
					for(Menu mu:menus){
						menulist.add(mu.getMenu_id());
					}
					for(DeptRole dr:dt){
						rolelist.add(dr.getRole_id());
					}
					//菜单角色权限
					List<MenuPermission> menuP = menuPermissionService.findMenuPermissionBypar(menulist, rolelist);
					List<String> menuPerlist = new ArrayList<String>();
					for(MenuPermission menuPermission:menuP){
						menuPerlist.add(menuPermission.getPermission_id());
					}
					List<Permission> ps = permissionService.findPermissionByPID(menuPerlist);
					return new NormalResponse(ps);
				}
			}else{
				return new EmptyResponse("当前用户菜单数据");
			}
		} catch (Exception e) {
			logger.error("获取当前用户是否具有菜单按钮权限环节出错：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		return null;
	}
}
