package cn.com.shuyangyang.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.GradeClass;
import cn.com.shuyangyang.domain.service.GradeClassVo;
import cn.com.shuyangyang.service.GradeClassService;
import cn.com.shuyangyang.util.BaseUtil;
import cn.com.shuyangyang.util.DateUtil;
import cn.com.shuyangyang.util.DoNumberUtil;

/**
 * 班级年级管理
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月4日 下午4:14:38
 */
@Controller
@RequestMapping("/gradeTheClass/")
public class GradeTheClassController {
	Logger logger = LoggerFactory.getLogger(GradeTheClassController.class);

	@Autowired
	private GradeClassService gradeClassService;

	/**
	 * 分页显示所有年级班级信息
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("showAllInfo")
	@ResponseBody
	public Response showAllInfo(HttpServletRequest req, HttpServletResponse resp) {
		String start = req.getParameter("start");// 起始页
		String limit = req.getParameter("limit");// 每页数量
		int index = Integer.parseInt(start);
		int pageSize = Integer.parseInt(limit);
		List<GradeClass> roles = gradeClassService.findAll(index, pageSize);
		long total = roles.size();
		if (total > 0) {
			logger.debug("now {}", "返回班级年级数据。。。");
			return new NormalResponse(roles, (long) gradeClassService.findAllCount());
		} else {
			logger.debug("now {}", "班级年级数据为空！");
			return new EmptyResponse("班级年级数据");
		}
	}
	
	/**
	 * 根据给定的数量自动生成班级年级
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("autoGenerate")
	@ResponseBody
	public Response autoGenerate(HttpServletRequest req, HttpServletResponse resp) {
		String grade = req.getParameter("grade");//共多少年级
		String theClass = req.getParameter("theClass");//每个年级有多少个班级
		try{
			if(!BaseUtil.isSpace(grade)&&!BaseUtil.isSpace(theClass)){
				int gradeCount = DoNumberUtil.intNullDowith(grade);//年级数量
				int theClassCount = DoNumberUtil.intNullDowith(theClass);//班级数量
				List<GradeClass> gdcs = new ArrayList<GradeClass>();
				for(int i=1;i<=gradeCount;i++){
					GradeClass gdc = null;
					for(int j=1;j<=theClassCount;j++){
						gdc =new GradeClass();
						gdc.setGrade(i+"年级");
						gdc.setTheClass(j+"班");
						gdcs.add(gdc);
					}
				}
				if(!BaseUtil.isSpace(gdcs)){
					gradeClassService.autoGenerate(gdcs);
				}else{
					return new EmptyResponse("生成数据");
				}
			}else{
				return new EmptyResponse("设置参数");
			}
		}catch(Exception e){
			logger.debug("now {}", "自动分配年级班级出现问题");
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		return new NormalResponse();
	}
	
	/**
	 * 更新年级班级信息
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("updateInfo")
	@ResponseBody
	public Response updateInfo(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("id");
		String grade = req.getParameter("grade");//年级
		String theClass = req.getParameter("theClass");//班级
		String remarks = req.getParameter("remarks");//更新备注
		try{
			if(!BaseUtil.isSpace(id)&&!BaseUtil.isSpace(grade)&&!BaseUtil.isSpace(theClass)&&!BaseUtil.isSpace(remarks)){
				GradeClass gdc = new GradeClass();
				gdc.setId(id);
				gdc.setGrade(grade+"年级");
				gdc.setTheClass(theClass+"班");
				gdc.setRemarks(remarks);
				gdc.setUpdateTime(DateUtil.getCurrentDateTimestamp());
				gradeClassService.updateInfo(gdc);
			}else{
				return new EmptyResponse("参数");
			}
		}catch(Exception e){
			logger.debug("now {}", "更新年级班级出现问题");
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		return new NormalResponse();
	}
	
	/**
	 * 删除年级班级信息
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("delInfo")
	@ResponseBody
	public Response deleteGradeClassById(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("ids");
		try{
			if(!BaseUtil.isSpace(id)){
				gradeClassService.deleteGradeClassById(id);
			}else{
				return new EmptyResponse("参数");
			}
		}catch(Exception e){
			logger.debug("now {}", "自动分配年级班级出现问题");
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		return new NormalResponse();
	}
	
	/**
	 * 根据id读取年级班级信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("loadGradeClass")
	@ResponseBody
	public Response loadGradeClass(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			String id = request.getParameter("id");
			GradeClass gradeClass = gradeClassService.findInfoById(DoNumberUtil.intNullDowith(id));
			if(gradeClass!=null){
				return new NormalResponse(gradeClass);
			}else{
				return new EmptyResponse("年级班级信息数据");
			}
		} catch (Exception e) {
			logger.error("读取年级班级信息环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	/**
	 * 显示所有班级年级信息，提供给学籍下拉框使用
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("showGradeClass")
	@ResponseBody
	public Response showGradeClass(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			List<GradeClass> gradeClass = gradeClassService.findAllNotPage();
			if(gradeClass!=null){
				List<GradeClassVo> gcvos = new ArrayList<GradeClassVo>();
				for(GradeClass gc:gradeClass){
					GradeClassVo vo = new GradeClassVo();
					vo.setGradeClass(gc.getGrade()+"("+gc.getTheClass()+")");
					vo.setpValue(gc.getGrade()+gc.getTheClass());
					gcvos.add(vo);
				}
				return new NormalResponse(gcvos);
			}else{
				return new EmptyResponse("显示所有班级年级信息");
			}
		} catch (Exception e) {
			logger.error("显示所有班级年级信息环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
}
