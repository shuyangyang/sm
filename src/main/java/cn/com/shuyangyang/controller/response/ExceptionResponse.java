package cn.com.shuyangyang.controller.response;


public class ExceptionResponse extends Response {

	public ExceptionResponse(Exception e) {
		this.setCode(ResponseCode.EXCEPTION);
		this.setMessage(e.getMessage());
		this.setData(null);
		this.setSuccess(false);
	}
}
