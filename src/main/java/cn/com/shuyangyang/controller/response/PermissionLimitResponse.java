package cn.com.shuyangyang.controller.response;


public class PermissionLimitResponse extends Response {

	public PermissionLimitResponse(String message) {
		this.setCode(ResponseCode.PERMISSIONLIMIT);
		this.setMessage(message);
		this.setData(null);
		this.setSuccess(false);
	}

	public PermissionLimitResponse() {
		this.setCode(ResponseCode.PERMISSIONLIMIT);
		this.setMessage("权限不足");
		this.setData(null);
		this.setSuccess(false);
	}
}
