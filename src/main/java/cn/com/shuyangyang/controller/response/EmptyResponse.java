package cn.com.shuyangyang.controller.response;


public class EmptyResponse extends Response {

	public EmptyResponse(String message) {
		this.setCode(ResponseCode.EMPTY);
		this.setMessage(message);
		this.setData(null);
		this.setSuccess(true);
	}
}
