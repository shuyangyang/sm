package cn.com.shuyangyang.controller.response;


public class NomalResponse extends Response {

	public NomalResponse(Object data) {
		this.setCode(ResponseCode.OK);
		this.setMessage("");
		this.setData(data);
		this.setSuccess(true);
	}
	
	public NomalResponse(Object data, Long total) {
		this.setCode(ResponseCode.OK);
		this.setMessage("");
		this.setData(data);
		this.setSuccess(true);
		this.setTotal(total);
	}

	public NomalResponse() {
		this.setCode(ResponseCode.OK);
		this.setMessage("");
		this.setData(null);
		this.setSuccess(true);
	}
}
