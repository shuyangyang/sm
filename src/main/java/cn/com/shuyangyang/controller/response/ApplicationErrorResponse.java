package cn.com.shuyangyang.controller.response;


public class ApplicationErrorResponse extends Response {

	public ApplicationErrorResponse(String message) {
		this.setCode(ResponseCode.APPERROR);
		this.setMessage(message);
		this.setData(null);
		this.setSuccess(false);
	}

	public ApplicationErrorResponse() {
		this.setCode(ResponseCode.APPERROR);
		this.setMessage("应用错误");
		this.setData(null);
		this.setSuccess(false);
	}
}
