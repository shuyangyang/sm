package cn.com.shuyangyang.controller.response;

public enum ResponseCode {

	OK,
	
	EXCEPTION,
	
	EMPTY, 
	
	PERMISSIONLIMIT, 
	
	APPERROR
}
