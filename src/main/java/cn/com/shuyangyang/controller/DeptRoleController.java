package cn.com.shuyangyang.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.DeptRole;
import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.Role;
import cn.com.shuyangyang.service.DeptRoleService;
import cn.com.shuyangyang.service.RoleService;

@Controller
@RequestMapping("/deptRole/")
public class DeptRoleController {

	Logger	logger	= LoggerFactory.getLogger(DeptRoleController.class);

	@Autowired
	private DeptRoleService		deptRoleService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping("showRoles")
	@ResponseBody
	public Response showRoles(HttpServletRequest req, HttpServletResponse resp){
		String start = req.getParameter("start");//起始页
        String limit = req.getParameter("limit");//每页数量
        String deptId = req.getParameter("deptId");
        int index = Integer.parseInt(start);
        int pageSize = Integer.parseInt(limit);
        List<Role> roles = roleService.FindRolesByPage(index, pageSize);
        List<DeptRole> deptRoles =  deptRoleService.getDeptRoleByDeptId(deptId);
        if(deptRoles!=null&&deptRoles.size()>0){
        	for(Role role:roles){
        		for(DeptRole deptRole:deptRoles){
        			String roleId = deptRole.getRole_id();
        			String roleId2 = role.getRole_id();
        			if(roleId.equals(roleId2)){
        				role.setSelected(true);
        			}
        		}
        	}
        }
        long total = roles.size();
        if(total>0){
			logger.debug("now {}" , "返回角色数据。。。");
			return new NormalResponse(roles,roleService.getRolesCount());
		}else{
			logger.debug("now {}" , "角色数据为空！");
			return new EmptyResponse("角色数据");
		}
	}

	/**
	 * 分配部门-角色关系
	 * 
	 * @return
	 */
	@RequestMapping("addDeptRole")
	@ResponseBody
	public Response addDeptRole(HttpServletRequest req,
			HttpServletResponse resp) {
		try {
			String deptId = req.getParameter("deptId");
			String roleIds = req.getParameter("roleIds");
			String[] roleId = roleIds.split(",");
			deptRoleService.deleteByDeptId(deptId);
			for(int i=0;i<roleId.length;i++){
				DeptRole deptRole = new DeptRole();
				deptRole.setDept_id(deptId);
				deptRole.setRole_id(roleId[i]);
				deptRole.setCreate_time(new Date());
				deptRole.setStatus(EnumStatus.AVAILABLE);
				deptRoleService.saveDeptRole(deptRole);
			}
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("添加部门-角色数据错误:" + e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
		
	}
}
