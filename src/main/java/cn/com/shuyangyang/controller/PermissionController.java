package cn.com.shuyangyang.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.common.Context;
import cn.com.shuyangyang.common.RequestContext;
import cn.com.shuyangyang.controller.response.ApplicationErrorResponse;
import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.Permission;
import cn.com.shuyangyang.service.PermissionService;

@Controller
@RequestMapping("/permission/")
public class PermissionController {
	
	Logger	logger	= LoggerFactory.getLogger(PermissionController.class);
	
	@Autowired
	private PermissionService permissionService;

	@RequestMapping("showPermissionByMenuId")
	@ResponseBody
	public Response showLoginLogByLoginName(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			String menuId = request.getParameter("menuId");
			if(menuId.equals(null)){
				System.out.println("我是空");
			}
			Permission permission = new Permission();
			permission.setPermission_id(null);
			permission.setPermission_fatherId(menuId);
			List<Permission> perList = permissionService.findPermissionByPermission(permission);
			return new NormalResponse(perList);
		} catch (Exception e) {
			logger.error("获取权限相关信息环节出错：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("addPermission")
	@ResponseBody
	public Response addUser(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			boolean flag = permissionService.savePermission(context);
			if(flag){
				return new NormalResponse();
			}else{
				return new ApplicationErrorResponse("请点击菜单后再添加权限！");
			}
		} catch (Exception e) {
			logger.error("添加用户环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("updaetPermission")
	@ResponseBody
	public Response updaetPermission(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			permissionService.updatePermission(context);
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("修改权限环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("loadPermission")
	@ResponseBody
	public Response loadPermission(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			Permission permission = permissionService.getPermissionByPermissionId(context);
			if(permission!=null){
				return new NormalResponse(permission);
			}else{
				return new EmptyResponse("权限数据");
			}
		} catch (Exception e) {
			logger.error("读取权限环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
	@RequestMapping("delPermission")
	@ResponseBody
	public Response delPermission(HttpServletRequest request, HttpServletResponse response){
		response.setCharacterEncoding("utf-8");
		try {
			Context context = new RequestContext(request);
			permissionService.delPermissionByPermissionId(context);
			return new NormalResponse();
		} catch (Exception e) {
			logger.error("删除权限环节出现异常：",e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
}
