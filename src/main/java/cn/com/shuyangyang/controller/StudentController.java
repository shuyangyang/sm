package cn.com.shuyangyang.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.shuyangyang.controller.response.EmptyResponse;
import cn.com.shuyangyang.controller.response.ExceptionResponse;
import cn.com.shuyangyang.controller.response.NormalResponse;
import cn.com.shuyangyang.controller.response.Response;
import cn.com.shuyangyang.domain.Student;
import cn.com.shuyangyang.service.StudentService;

/**
 * 学籍
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2017年3月9日 下午1:41:38
 */
@Controller
@RequestMapping("/student")
public class StudentController {

	Logger	logger	= LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/saveStudent")
	@ResponseBody
	public Response saveStudentInfo(Student stu){
		try{
			if(stu!=null){
				stu.setCreate_date_time(new Date());
				stu.setUpdate_date_time(new Date());
				studentService.saveStudent(stu);
				return new NormalResponse();
			}else{
				return new EmptyResponse("不能保存空的信息");
			}
		}catch(Exception e){
			logger.error("保存学籍信息错误:" + e);
			e.printStackTrace();
			return new ExceptionResponse(e);
		}
	}
	
}

