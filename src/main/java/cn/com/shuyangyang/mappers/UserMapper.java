package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.User;

/**
 * 用户接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface UserMapper {

	void saveUser(User user);

	void updateUserByUserId(User user);

	void deleteUserByUserId(String userId);

	User findUser(User user);

	List<User> findAll();
	
	void updateUserByLoginName(User user);
	
	/** 分页查询 */
	List<User> findUserByPage(@Param(value="index")int index,@Param(value="pageSize")int page);

}
