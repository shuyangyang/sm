package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.Article;

public interface ArticleMapper {
	List<Article> findAll(@Param(value="index")int index,@Param(value="pageSize")int page);
	
	int findAllCount();
	
	void deleteArticle(@Param(value="articleId")String articleId);
}
