package cn.com.shuyangyang.mappers;

import java.util.List;

import cn.com.shuyangyang.domain.RoleMenu;

/**
 * 角色菜单映射接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:52:42
 *
 */
public interface RoleMenuMapper {

	void saveRoleMenu(RoleMenu roleMenu);
	
	void updateRoleMenuByRMId(RoleMenu roleMenu);
	
	void deleteRoleMenuByRMID(RoleMenu roleMenu);
	
	List<RoleMenu> findRoleMenu(RoleMenu roleMenu);
	
}
