package cn.com.shuyangyang.mappers;

import java.util.List;

//import org.apache.ibatis.annotations.Param;


import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.Menu;
import cn.com.shuyangyang.domain.MenuTree;

/**
 * 菜单接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface MenuMapper {

	void saveMenu(Menu menu);

	void updateMenuByMenuId(Menu menu);
	
	void updateMenuByMenuName(Menu menu);

	void deleteMenuByMenuId(Menu menu);

	List<Menu> findMenu(Menu menu);
	
	List<Menu> findMenuByFatherId(String fatherId);

	List<Menu> findAllByPage();
	
	List<Menu> findAll();

	List<Menu> menuFather();
	
	List<MenuTree> menuTreeFather();
	List<MenuTree> findMenuTreeByFatherId(String fatherId);
	
	Menu findMenuByMenuId(String menuId);
	
	Menu findMenuByMenuText(String menuText);
	
	void deleteMenuByMenuId(String menuId);
	
	List<Menu> findMenuByPermiParams(@Param(value="userId")String userId);
}
