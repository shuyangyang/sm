package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.Dept;

/**
 * 部门接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface DeptMapper {

	void saveDept(Dept dept);

	void updateDeptByDeptId(Dept dept);

	void deleteDeptByDeptId(Dept dept);

	List<Dept> findDept(Dept dept);
	
	List<Dept> findDeptByFatherId(String deptFatherId);

	List<Dept> findAll();

	List<Dept> findDeptByUserId(@Param(value="user_id")String user_id);
	
	List<Dept> findDeptByUserIdCount(@Param(value="user_id")String user_id);
}
