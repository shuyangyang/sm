package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.Student;

/**
 * 学籍接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface StudentMapper {

	void saveStudent(Student stu);
	
	/** 分页查询 */
	List<Student> findStudentByPage(@Param(value="index")int index,@Param(value="pageSize")int page);
}
