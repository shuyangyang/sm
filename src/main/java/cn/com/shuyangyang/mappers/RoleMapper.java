package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.Role;

/**
 * 角色接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface RoleMapper {

	void saveRole(Role role);

	void updateRoleByRoleId(Role role);

	void deleteRoleByRoleId(String roleId);

	Role findRole(Role role);

	List<Role> findAll();
	
	/** 分页查询 */
	List<Role> findRoleByPage(@Param(value="index")int index,@Param(value="pageSize")int page);

	List<Role> findRoleByDeptId(String deptId);
}
