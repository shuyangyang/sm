package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.GradeClass;

public interface GradeClassMapper {
	void autoGenerate(List<GradeClass> gcs);
	
	void updateInfo(GradeClass gradeClass);
	
	List<GradeClass> findAll(@Param(value="index")int index,@Param(value="pageSize")int page);
	
	List<GradeClass> findAllNotPage();
	
	int findAllCount();
	
	void deleteGradeClassById(int id);
	
	GradeClass findInfoById(int id);
}
