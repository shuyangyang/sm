package cn.com.shuyangyang.mappers;

import java.util.List;

import cn.com.shuyangyang.domain.DeptRole;

/**
 * 部门角色映射接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:52:42
 *
 */
public interface DeptRoleMapper {

	void saveDeptRole(DeptRole deptRole);
	
	void updateDeptRoleByDeptRoleId(DeptRole deptRole);
	
	void deleteDeptRoleByDeptRoleID(DeptRole deptRole);
	
	List<DeptRole> findDeptRole(DeptRole deptRole);
	
	List<DeptRole> findDeptRoleByDeptId(List<String> list);
}
