package cn.com.shuyangyang.mappers;

import java.util.List;

import cn.com.shuyangyang.domain.Permission;

/**
 * 权限接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface PermissionMapper {

	void savePermission(Permission permission);

	void updatePermissionByPermissionId(Permission permission);

	void deletePermissionByPermissionId(String permissionId);

	Permission findPermission(Permission permission);

	List<Permission> findAll();
	
	List<Permission> findPermissionByPermission(Permission permission);
	
	List<Permission> findPermissionByPID(List<String> list);

}
