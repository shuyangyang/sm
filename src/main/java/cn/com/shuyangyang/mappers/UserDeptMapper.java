package cn.com.shuyangyang.mappers;

import java.util.List;

import cn.com.shuyangyang.domain.UserDept;

/**
 * 用户部门映射接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:52:42
 *
 */
public interface UserDeptMapper {

	void saveUserDept(UserDept userDept);
	
	void updateUserDeptByUdpId(UserDept userDept);
	
	void deleteUserDeptByUIDorDptID(UserDept userDept);
	
	List<UserDept> findUserDept(UserDept userDept);
	
}
