package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.UserMessage;

/**
 * 用户-消息接口
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年9月5日 下午4:43:03
 */
public interface UserMessageMapper {
	
	void saveUserMessage(UserMessage usermessage);

	void updateUserMessageById(UserMessage usermessage);

	void deleteUserMessageById(String id);

	UserMessage findUserMessAge(UserMessage usermessage);

	List<UserMessage> findAll();
	
	/** 分页查询 */
	List<UserMessage> findUserMessAgeByPage(@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 用户未读消息数 */
	int findUnreadCountByUser(String UserId);
	
	/** 分页查询用户收件箱*/
	List<UserMessage> getAllUncollectedMail(@Param(value="receverid_user")String receverid_user,@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 用户草稿箱消息列表 */
	List<UserMessage> getDraFtsMail(@Param(value="receverid_user")String receverid_user,@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 用户已发送消息列表 */
	List<UserMessage> getSentMessageMail(@Param(value="receverid_user")String receverid_user,@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 用户已删除消息列表 */
	List<UserMessage> getDeletedMessageMail(@Param(value="receverid_user")String receverid_user,@Param(value="index")int index,@Param(value="pageSize")int page);
	
	/** 查询用户未读消息 */
	List<UserMessage> findUnreadListByUser(UserMessage um);
	
	/** 查询用户收件箱消息 */
	List<UserMessage> findListByUser(String receverid_user);
}

