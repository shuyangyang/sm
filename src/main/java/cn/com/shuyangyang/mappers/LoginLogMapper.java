package cn.com.shuyangyang.mappers;

import java.util.List;

import cn.com.shuyangyang.domain.LoginLog;

/**
 * 登录操作日志接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:21:08
 *
 */
public interface LoginLogMapper {

	void saveLoginLog(LoginLog loginLog);

	void updateLoginLogByLoginLogtId(LoginLog loginLog);

	void deleteLoginLogByLoginLogId(LoginLog loginLog);

	LoginLog findLoginLog(LoginLog loginLog);

	List<LoginLog> findAll();
	
	List<LoginLog> findLoginLogInfo(LoginLog loginLog);

}
