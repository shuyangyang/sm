package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.MenuPermission;

/**
 * 菜单权限映射接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月27日下午8:52:42
 *
 */
public interface MenuPermissionMapper {

	void saveMenuPermission(MenuPermission menuPermission);
	
	void updateMenuPermissionByMenuPermissionId(MenuPermission menuPermission);
	
	void deleteMenuPermissionByMenuPermissionID(MenuPermission menuPermission);
	
	void deleteMenuPermissionByParams(MenuPermission menuPermission);
	
	List<MenuPermission> findMenuPermission(MenuPermission menuPermission);
	
	List<MenuPermission> findMenuPermissionBypar(@Param(value="list")List<String> list,@Param(value="list2")List<String> list2);
	
}
