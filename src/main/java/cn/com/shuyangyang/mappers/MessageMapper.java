package cn.com.shuyangyang.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.shuyangyang.domain.MessAge;

/**
 * 消息接口
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2016年9月2日下午8:21:08
 *
 */
public interface MessageMapper {

	void saveMessage(MessAge message);

	void updateMessageByMessageId(MessAge message);

	void deleteMessageBymessageId(String messageId);

	MessAge findMessAge(MessAge message);

	List<MessAge> findAll();
	
	/** 分页查询 */
	List<MessAge> findMessAgeByPage(@Param(value="index")int index,@Param(value="pageSize")int page);
}
