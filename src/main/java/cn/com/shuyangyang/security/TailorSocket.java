package cn.com.shuyangyang.security;

import java.io.IOException;
import org.eclipse.jetty.websocket.WebSocket.OnTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.com.shuyangyang.common.MyBeanFactory;
import cn.com.shuyangyang.service.UserMessageService;
import cn.com.shuyangyang.util.BaseUtil;

public class TailorSocket implements OnTextMessage{

	Logger	logger	= LoggerFactory.getLogger(TailorSocket.class);
	
	/**
	 * CopyOnWrite容器即写时复制的容器。
	 * 通俗的理解是当我们往一个容器添加元素的时候，不直接往当前容器添加，而是先将当前容器进行Copy，复制出一个新的容器，
	 * 然后新的容器里添加元素，添加完元素之后，再将原容器的引用指向新的容器。
	 * 这样做的好处是我们可以对CopyOnWrite容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。
	 * 所以CopyOnWrite容器也是一种读写分离的思想，读和写不同的容器。
	 */
	private Connection _connection;
	
	//在一个客户端连上来的时候调用
	@Override
	public void onOpen(Connection connection) {
		this._connection = connection;
		InitServlet.get_members().add(this);
	}

	//在客户端断开时调用
	@Override
	public void onClose(int closeCode, String message) {
		InitServlet.get_members().remove(this);
		logger.debug("用户下线了");
	}

	@Override
	public void onMessage(String userName) {
		if(!BaseUtil.isSpace(userName)){
			if(!userName.equals("keepAlive999")){
				logger.debug("接收的登陆账号信息{}",userName);
				UserMessageService userMessageService = (UserMessageService) MyBeanFactory.getInstance().getBean("userMessageService");
				for (TailorSocket socket : InitServlet.get_members()) {
					try {
						if(!BaseUtil.isSpace(userName)){
							String[] users = userName.split(",");
							for(int i=0;i<users.length;i++){
								int unReadCount = userMessageService.findUnreadCountByUser(users[i]);
								if(unReadCount>0){
									//查询用户有多少条未读消息，返回给前端显示
					   				socket.get_connection().sendMessage(users[i]+":"+unReadCount);
								}
				   				logger.debug("登陆账号：{}的未读消息数量：{}",users[i],unReadCount);
							}
						}
		   			} catch (IOException e) {
		   				e.printStackTrace();
		   			}
		        }
			}else{
				logger.debug("保持心跳……");
			}
		}
	}

	public Connection get_connection() {
		return _connection;
	}

	public void set_connection(Connection _connection) {
		this._connection = _connection;
	}
	
	
}
