package cn.com.shuyangyang.security;

import java.io.File;
import java.util.Locale;

import org.springframework.web.servlet.view.InternalResourceView;

/**html视图解析器
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月2日 下午6:08:26
 */
public class HtmlResourceView extends InternalResourceView{
	 @Override  
     public boolean checkResource(Locale locale) {  
	      File file = new File(this.getServletContext().getRealPath("/") + getUrl());  
	      return file.exists();// 判断该页面是否存在  
     }  
}

