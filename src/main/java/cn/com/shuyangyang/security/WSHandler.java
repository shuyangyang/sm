package cn.com.shuyangyang.security;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

/**
 * WebScoket
 * @author shuyangyang
 *
 */
@SuppressWarnings("serial")
public class WSHandler extends WebSocketServlet {
	
	@Override
    public WebSocket doWebSocketConnect(HttpServletRequest request, String protocol) {
//		System.out.println("url=" + request.getRequestURL() + ",protocol=" + protocol);
        return new TailorSocket();
    }
}
