package cn.com.shuyangyang.security;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

@SuppressWarnings("serial")
public class InitServlet extends HttpServlet{
	
	private static Set<TailorSocket> _members;
	
	public void init(ServletConfig config) throws ServletException {  
        InitServlet._members = new CopyOnWriteArraySet<TailorSocket>();
        super.init(config);
        System.out.println("Server start============");  
    }

	public static Set<TailorSocket> get_members() {
		return InitServlet._members;
	}
	
}
