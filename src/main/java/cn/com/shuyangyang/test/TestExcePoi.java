package cn.com.shuyangyang.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import cn.com.shuyangyang.domain.EnumStatus;
import cn.com.shuyangyang.domain.User;

public class TestExcePoi {

	public static void main(String[] args) throws IOException {
		TestExcePoi xlsMain = new TestExcePoi();
        User user = null;
        List<User> list = xlsMain.readXls();
        for (int i = 0; i < list.size(); i++) {
        	user = (User) list.get(i);
            System.out.println(user.getUser_name() + "    " + user.getLogin_name() + "    "
                    + user.getLogin_password() + "    " + user.getUser_status());
        }
	}

	/**
     * 读取xls文件内容
     * 
     * @return List<User>对象
     * @throws IOException
     *             输入/输出(i/o)异常
     */
    private List<User> readXls() throws IOException {
        InputStream is = new FileInputStream("C:\\Users\\Administrator\\Desktop\\uploadUserExample.xls");
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
        User user = null;
        List<User> list = new ArrayList<User>();
        // 循环工作表Sheet
        for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
            HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
            if (hssfSheet == null) {
                continue;
            }
            // 循环行Row
            for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
                HSSFRow hssfRow = hssfSheet.getRow(rowNum);
                if (hssfRow == null) {
                    continue;
                }
                user = new User();
                // 循环列Cell
                // 0用户名 1登录名 2登录密码 3用户状态
                // for (int cellNum = 0; cellNum <=4; cellNum++) {
                HSSFCell userName = hssfRow.getCell(0);
                if (userName == null) {
                    continue;
                }
                user.setUser_name(getValue(userName));
                HSSFCell loginName = hssfRow.getCell(1);
                if (loginName == null) {
                    continue;
                }
                user.setLogin_name(getValue(loginName));
                HSSFCell loginPwd = hssfRow.getCell(2);
                if (loginPwd == null) {
                    continue;
                }
                user.setLogin_password(getValue(loginPwd));
                HSSFCell userStatus = hssfRow.getCell(3);
                if (userStatus == null) {
                    continue;
                }
                if(getValue(userStatus).equals("AVAILABLE")){
                	user.setUser_status(EnumStatus.AVAILABLE);
                }else{
                	user.setUser_status(EnumStatus.DISABLED);
                }
                list.add(user);
            }
        }
        return list;
    }
 
    /**
     * 得到Excel表中的值
     * 
     * @param hssfCell
     *            Excel中的每一个格子
     * @return Excel中每一个格子中的值
     */
    @SuppressWarnings("static-access")
    private String getValue(HSSFCell hssfCell) {
        if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) {
            // 返回布尔类型的值
            return String.valueOf(hssfCell.getBooleanCellValue());
        } else if (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
            // 返回数值类型的值
            return String.valueOf(hssfCell.getNumericCellValue());
        } else {
            // 返回字符串类型的值
            return String.valueOf(hssfCell.getStringCellValue());
        }
    }
 
}
