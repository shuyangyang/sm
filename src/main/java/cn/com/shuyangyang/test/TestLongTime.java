package cn.com.shuyangyang.test;

import java.text.SimpleDateFormat;

public class TestLongTime {

	public static void main(String[] args) {
		String time = "1440422300000";
		Long longtime = Long.parseLong(time);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sd.format(longtime));
	}

}
