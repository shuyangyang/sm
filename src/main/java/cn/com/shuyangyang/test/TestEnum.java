package cn.com.shuyangyang.test;

public class TestEnum {
	
	public enum UserType{
		GENERAL(1),
		ADMIN(2);
		
		private int status;
		
		UserType(int status){
			this.status = status;
		}

		public int getStatus() {
			return status;
		}
	}

	public static void main(String[] args) {
		System.out.println(UserType.GENERAL.status);
	}

}
