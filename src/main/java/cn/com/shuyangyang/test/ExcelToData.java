package cn.com.shuyangyang.test;

import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.shuyangyang.domain.User;
import cn.com.shuyangyang.mappers.UserMapper;

public class ExcelToData {
	
	@Autowired
	private static UserMapper userMapper;

	public static void main(String[] args) {
		// 第一步，创建一个webbook，对应一个Excel文件  
        HSSFWorkbook wb = new HSSFWorkbook();  
        // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet  
        HSSFSheet sheet = wb.createSheet("用户表一");  
        // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short  
        HSSFRow row = sheet.createRow((int) 0);  
        // 第四步，创建单元格，并设置值表头 设置表头居中  
        HSSFCellStyle style = wb.createCellStyle();  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式  
        
        HSSFCell cell = row.createCell(0);  
        cell.setCellValue("用户名");  
        cell.setCellStyle(style);  
        cell = row.createCell(1);  
        cell.setCellValue("登录名");  
        cell.setCellStyle(style);  
        cell = row.createCell(2); 
        cell.setCellValue("登录密码");  
        cell.setCellStyle(style);  
        cell = row.createCell(3); 
        cell.setCellValue("用户状态");  
        cell.setCellStyle(style);
        cell = row.createCell(4); 
        cell.setCellValue("创建时间");  
        cell.setCellStyle(style);
        cell = row.createCell(5); 
        cell.setCellValue("最后更新时间");  
        cell.setCellStyle(style);  
  
        // 第五步，写入实体数据 实际应用中这些数据从数据库得到，  
        List<User> list = userMapper.findAll();
  
        for (int i = 0; i < list.size(); i++)  
        {  
            row = sheet.createRow((int) i + 1);  
            User user = list.get(i);  
            // 第四步，创建单元格，并设置值  
            row.createCell(0).setCellValue(user.getUser_name());  
            row.createCell(1).setCellValue(user.getLogin_name());  
            row.createCell(2).setCellValue(user.getLogin_password());  
            cell = row.createCell(3);  
            cell.setCellValue(user.getUser_status().toString());  
            cell = row.createCell(4);  
            cell.setCellValue(user.getCreate_time()); 
            cell = row.createCell(5);  
            cell.setCellValue(user.getUpdate_time());  
        }  
        // 第六步，将文件存到指定位置  
        try  
        {  
            FileOutputStream fout = new FileOutputStream("E:/users.xls");  
            wb.write(fout);  
            fout.close();  
        }  
        catch (Exception e)  
        {  
            e.printStackTrace();  
        }  
	}

}
