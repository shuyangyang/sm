package cn.com.shuyangyang.domain;

import java.util.Date;

/**学籍信息实体类
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月16日 下午2:47:28
 */
public class Student {

	private int student_id;//序号
	
	private String auxiliary_no;//学籍辅号
	
	private String student_no;//学号
	
	private String student_name;//学生姓名
	
	private int sex;//学生性别
	
	private String student_name_py;//学生姓名拼音
	
	private String former_name;//学生曾用名
	
	private String photo;//学生照片
	
	private String blood;//学生血型
	
	private String dates_attended;//入学年月
	
	private String nation;//民族
	
	private String grade_class;//年级班级
	
	private String healthy_status;//健康状况
	
	private String dateOfBirth;//出生年月
	
	private String zip_code;//邮政编码
	
	private int account;//户口性质
	
	private String nationality;//国籍地区
	
	private int id_type;//学生证件类型
	
	private String id_number;//学生证件号码
	
	private int studying_way;//就读方式
	
	private int matriculation;//入学方式
	
	private String domicile_place;//户口所在地
	
	private String birth_place;//出生地
	
	private String native_place;//籍贯
	
	private String com_from_school;//来源学校
	
	private String com_from_prefecture;//来源地区
	
	private String fgraduate_date;//来源学校结束学业年月
	
	private String phone;//学生联系电话
	
	private int traling_status;//是否进城务工人员随迁子女
	
	private int best_student_status;//是否烈士或优抚子女
	
	private int outside_status;//港澳台侨外
	
	private int onlychild_status;//是否独生子女
	
	private int enjoy_fill;//是否享受一补
	
	private int orphan_status;//是否孤儿
	
	private int leftBehindChild_status;//是否留守儿童
	
	private int prescholl_edu_status;//是否受过学前教育
	
	private int subsidization;//是否需要申请资助
	
	private int floating_population;//是否流动人口
	
	private String graduate_date;//结束学业年月
	
	private int graduateResultCode;//结束学业状态
	
	private String graduating_school;//毕业学校名称
	
	private String mailing_address;//通信地址
	
	private String graduate_caus;//结束学业原因
	
	private String present_address;//现住址
	
	private String home_add;//现住址
	
	private int familyties;//监护人一与被监护人的关系
	
	private String nameOfGuardian;//监护人一的姓名
	
	private int wtguardian;//是否监护人一
	
	private String guardian_telphone;//监护人一的联系电话
	
	private String guardian_duty;//监护人一的职务
	
	private int guardian_id_type;//监护人一的证件类型
	
	private String guardian_id_no;//监护人一的证件号码
	
	private String guardian_add;//监护人一的现住址
	
	private String guardian_domicile_place;//监护人一户口所在地
	
	private int family_ties2;//监护人二与被监护人的关系
	
	private String name_of_guardian2;//监护人二的姓名
	
	private int wtguardian2;//是否监护人二
	
	private String guardian_telphone2;//监护人二的联系电话
	
	private String guardianduty2;//监护人二的职务
	
	private int guardian_id_type2;//监护人二的证件类型
	
	private String guardian_id_no2;//监护人二的证件号码
	
	private String guardian_add2;//监护人二的现住址
	
	private String guardian_domicile_place2;//监护人二户口所在地
	
	private Date create_date_time;//学籍创建时间
	
	private Date update_date_time;//学籍更新时间
	
	
	public Student(){}


	public int getStudent_id() {
		return student_id;
	}


	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}


	public String getAuxiliary_no() {
		return auxiliary_no;
	}


	public void setAuxiliary_no(String auxiliary_no) {
		this.auxiliary_no = auxiliary_no;
	}


	public String getStudent_no() {
		return student_no;
	}


	public void setStudent_no(String student_no) {
		this.student_no = student_no;
	}


	public String getStudent_name() {
		return student_name;
	}


	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}


	public int getSex() {
		return sex;
	}


	public void setSex(int sex) {
		this.sex = sex;
	}


	public String getStudent_name_py() {
		return student_name_py;
	}


	public void setStudent_name_py(String student_name_py) {
		this.student_name_py = student_name_py;
	}


	public String getFormer_name() {
		return former_name;
	}


	public void setFormer_name(String former_name) {
		this.former_name = former_name;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getBlood() {
		return blood;
	}


	public void setBlood(String blood) {
		this.blood = blood;
	}


	public String getDates_attended() {
		return dates_attended;
	}


	public void setDates_attended(String dates_attended) {
		this.dates_attended = dates_attended;
	}


	public String getNation() {
		return nation;
	}


	public void setNation(String nation) {
		this.nation = nation;
	}


	public String getGrade_class() {
		return grade_class;
	}


	public void setGrade_class(String grade_class) {
		this.grade_class = grade_class;
	}


	public String getHealthy_status() {
		return healthy_status;
	}


	public void setHealthy_status(String healthy_status) {
		this.healthy_status = healthy_status;
	}


	public String getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getZip_code() {
		return zip_code;
	}


	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}


	public int getAccount() {
		return account;
	}


	public void setAccount(int account) {
		this.account = account;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public int getId_type() {
		return id_type;
	}


	public void setId_type(int id_type) {
		this.id_type = id_type;
	}


	public String getId_number() {
		return id_number;
	}


	public void setId_number(String id_number) {
		this.id_number = id_number;
	}


	public int getStudying_way() {
		return studying_way;
	}


	public void setStudying_way(int studying_way) {
		this.studying_way = studying_way;
	}


	public int getMatriculation() {
		return matriculation;
	}


	public void setMatriculation(int matriculation) {
		this.matriculation = matriculation;
	}


	public String getDomicile_place() {
		return domicile_place;
	}


	public void setDomicile_place(String domicile_place) {
		this.domicile_place = domicile_place;
	}


	public String getBirth_place() {
		return birth_place;
	}


	public void setBirth_place(String birth_place) {
		this.birth_place = birth_place;
	}


	public String getNative_place() {
		return native_place;
	}


	public void setNative_place(String native_place) {
		this.native_place = native_place;
	}


	public String getCom_from_school() {
		return com_from_school;
	}


	public void setCom_from_school(String com_from_school) {
		this.com_from_school = com_from_school;
	}


	public String getCom_from_prefecture() {
		return com_from_prefecture;
	}


	public void setCom_from_prefecture(String com_from_prefecture) {
		this.com_from_prefecture = com_from_prefecture;
	}


	public String getFgraduate_date() {
		return fgraduate_date;
	}


	public void setFgraduate_date(String fgraduate_date) {
		this.fgraduate_date = fgraduate_date;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getTraling_status() {
		return traling_status;
	}


	public void setTraling_status(int traling_status) {
		this.traling_status = traling_status;
	}


	public int getBest_student_status() {
		return best_student_status;
	}


	public void setBest_student_status(int best_student_status) {
		this.best_student_status = best_student_status;
	}


	public int getOutside_status() {
		return outside_status;
	}


	public void setOutside_status(int outside_status) {
		this.outside_status = outside_status;
	}


	public int getOnlychild_status() {
		return onlychild_status;
	}


	public void setOnlychild_status(int onlychild_status) {
		this.onlychild_status = onlychild_status;
	}


	public int getEnjoy_fill() {
		return enjoy_fill;
	}


	public void setEnjoy_fill(int enjoy_fill) {
		this.enjoy_fill = enjoy_fill;
	}


	public int getOrphan_status() {
		return orphan_status;
	}


	public void setOrphan_status(int orphan_status) {
		this.orphan_status = orphan_status;
	}


	public int getLeftBehindChild_status() {
		return leftBehindChild_status;
	}


	public void setLeftBehindChild_status(int leftBehindChild_status) {
		this.leftBehindChild_status = leftBehindChild_status;
	}


	public int getPrescholl_edu_status() {
		return prescholl_edu_status;
	}


	public void setPrescholl_edu_status(int prescholl_edu_status) {
		this.prescholl_edu_status = prescholl_edu_status;
	}


	public int getSubsidization() {
		return subsidization;
	}


	public void setSubsidization(int subsidization) {
		this.subsidization = subsidization;
	}


	public int getFloating_population() {
		return floating_population;
	}


	public void setFloating_population(int floating_population) {
		this.floating_population = floating_population;
	}


	public String getGraduate_date() {
		return graduate_date;
	}


	public void setGraduate_date(String graduate_date) {
		this.graduate_date = graduate_date;
	}


	public int getGraduateResultCode() {
		return graduateResultCode;
	}


	public void setGraduateResultCode(int graduateResultCode) {
		this.graduateResultCode = graduateResultCode;
	}


	public String getGraduating_school() {
		return graduating_school;
	}


	public void setGraduating_school(String graduating_school) {
		this.graduating_school = graduating_school;
	}


	public String getMailing_address() {
		return mailing_address;
	}


	public void setMailing_address(String mailing_address) {
		this.mailing_address = mailing_address;
	}


	public String getGraduate_caus() {
		return graduate_caus;
	}


	public void setGraduate_caus(String graduate_caus) {
		this.graduate_caus = graduate_caus;
	}


	public String getPresent_address() {
		return present_address;
	}


	public void setPresent_address(String present_address) {
		this.present_address = present_address;
	}


	public String getHome_add() {
		return home_add;
	}


	public void setHome_add(String home_add) {
		this.home_add = home_add;
	}


	public int getFamilyties() {
		return familyties;
	}


	public void setFamilyties(int familyties) {
		this.familyties = familyties;
	}


	public String getNameOfGuardian() {
		return nameOfGuardian;
	}


	public void setNameOfGuardian(String nameOfGuardian) {
		this.nameOfGuardian = nameOfGuardian;
	}


	public int getWtguardian() {
		return wtguardian;
	}


	public void setWtguardian(int wtguardian) {
		this.wtguardian = wtguardian;
	}


	public String getGuardian_telphone() {
		return guardian_telphone;
	}


	public void setGuardian_telphone(String guardian_telphone) {
		this.guardian_telphone = guardian_telphone;
	}


	public String getGuardian_duty() {
		return guardian_duty;
	}


	public void setGuardian_duty(String guardian_duty) {
		this.guardian_duty = guardian_duty;
	}


	public int getGuardian_id_type() {
		return guardian_id_type;
	}


	public void setGuardian_id_type(int guardian_id_type) {
		this.guardian_id_type = guardian_id_type;
	}


	public String getGuardian_id_no() {
		return guardian_id_no;
	}


	public void setGuardian_id_no(String guardian_id_no) {
		this.guardian_id_no = guardian_id_no;
	}


	public String getGuardian_add() {
		return guardian_add;
	}


	public void setGuardian_add(String guardian_add) {
		this.guardian_add = guardian_add;
	}


	public String getGuardian_domicile_place() {
		return guardian_domicile_place;
	}


	public void setGuardian_domicile_place(String guardian_domicile_place) {
		this.guardian_domicile_place = guardian_domicile_place;
	}


	public int getFamily_ties2() {
		return family_ties2;
	}


	public void setFamily_ties2(int family_ties2) {
		this.family_ties2 = family_ties2;
	}


	public String getName_of_guardian2() {
		return name_of_guardian2;
	}


	public void setName_of_guardian2(String name_of_guardian2) {
		this.name_of_guardian2 = name_of_guardian2;
	}


	public int getWtguardian2() {
		return wtguardian2;
	}


	public void setWtguardian2(int wtguardian2) {
		this.wtguardian2 = wtguardian2;
	}


	public String getGuardian_telphone2() {
		return guardian_telphone2;
	}


	public void setGuardian_telphone2(String guardian_telphone2) {
		this.guardian_telphone2 = guardian_telphone2;
	}


	public String getGuardianduty2() {
		return guardianduty2;
	}


	public void setGuardianduty2(String guardianduty2) {
		this.guardianduty2 = guardianduty2;
	}


	public int getGuardian_id_type2() {
		return guardian_id_type2;
	}


	public void setGuardian_id_type2(int guardian_id_type2) {
		this.guardian_id_type2 = guardian_id_type2;
	}


	public String getGuardian_id_no2() {
		return guardian_id_no2;
	}


	public void setGuardian_id_no2(String guardian_id_no2) {
		this.guardian_id_no2 = guardian_id_no2;
	}


	public String getGuardian_add2() {
		return guardian_add2;
	}


	public void setGuardian_add2(String guardian_add2) {
		this.guardian_add2 = guardian_add2;
	}


	public String getGuardian_domicile_place2() {
		return guardian_domicile_place2;
	}


	public void setGuardian_domicile_place2(String guardian_domicile_place2) {
		this.guardian_domicile_place2 = guardian_domicile_place2;
	}


	public Date getCreate_date_time() {
		return create_date_time;
	}


	public void setCreate_date_time(Date create_date_time) {
		this.create_date_time = create_date_time;
	}


	public Date getUpdate_date_time() {
		return update_date_time;
	}


	public void setUpdate_date_time(Date update_date_time) {
		this.update_date_time = update_date_time;
	}
	
}

