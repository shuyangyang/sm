package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 角色菜单映射实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:20:31
 * 
 */
public class RoleMenu {
	/** @pdOid 7945b73c-9979-47d2-8e3f-6c02881fc571 */
	private String		role_menu_id;
	/** @pdOid d491d300-dd13-4e84-b78a-0f47b639064d */
	private String		role_id;
	/** @pdOid b013d11b-d68b-4782-b7a3-2d44ab18afed */
	private String		menu_id;
	/** @pdOid b00390e2-4b55-4ea5-acb8-8d90a07eaf84 */
	private EnumStatus	status;
	/** @pdOid 2ac55beb-969e-4056-80f5-607ad960cc2f */
	private Date		create_time;
	/** @pdOid 03ca7cf7-54d1-4656-9649-0b59be26f948 */
	private Date		update_time;

	public RoleMenu() {
		this.role_menu_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public RoleMenu(String role_menu_id, String role_id, String menu_id,
			EnumStatus status, Date create_time, Date update_time) {
		super();
		this.role_menu_id = role_menu_id;
		this.role_id = role_id;
		this.menu_id = menu_id;
		this.status = status;
		this.create_time = create_time;
		this.update_time = update_time;
	}

	public String getRole_menu_id() {
		return role_menu_id;
	}

	public void setRole_menu_id(String role_menu_id) {
		this.role_menu_id = role_menu_id;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "RoleMenu [role_menu_id=" + role_menu_id + ", role_id="
				+ role_id + ", menu_id=" + menu_id + ", status=" + status
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + "]";
	}
}
