package cn.com.shuyangyang.domain;

/**
 * 常用枚举类型
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月29日下午10:05:02
 *
 */
public enum EnumStatus {

	/** 无效0*/
	DISABLED(0),
	/** 有效1 */
	AVAILABLE(1),
	/** 删除2 */
	DELETE(2),
	/** 登录3 */
	LOGINNED(3),
	/** 登出4 */
	LOGOUTED(4),
	/** 离线5 */
	INVAVILED(5),
	/** 非正常退出6(掉线、直接关闭浏览器等行为) */
	NON_NORMAL_EXIT(6);
	
	private int status;
	
	EnumStatus(int status){
		this.status = status;
	}

	public int getStatus() {
		return status;
	}
	
}
