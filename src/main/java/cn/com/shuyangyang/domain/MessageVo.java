package cn.com.shuyangyang.domain;

import java.util.List;

/**
 * 消息目录实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:28:13
 * 
 */
public class MessageVo {
	private String		dirtype;//目录类型
	private String		text;
	private boolean		candroponfirst;
	private boolean		canDropOnSecond;
	private String		iconCls;		// 图标css类
	private boolean		leaf;

	private List<MessageVo>	children;

	public MessageVo(){}
	
	public MessageVo(String text, boolean candroponfirst, boolean canDropOnSecond, String iconCls, boolean leaf,
			List<MessageVo> children,String dirtype) {
		super();
		this.text = text;
		this.candroponfirst = candroponfirst;
		this.canDropOnSecond = canDropOnSecond;
		this.iconCls = iconCls;
		this.leaf = leaf;
		this.children = children;
		this.dirtype = dirtype;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isCandroponfirst() {
		return candroponfirst;
	}

	public void setCandroponfirst(boolean candroponfirst) {
		this.candroponfirst = candroponfirst;
	}

	public boolean isCanDropOnSecond() {
		return canDropOnSecond;
	}

	public void setCanDropOnSecond(boolean canDropOnSecond) {
		this.canDropOnSecond = canDropOnSecond;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public List<MessageVo> getChildren() {
		return children;
	}

	public void setChildren(List<MessageVo> children) {
		this.children = children;
	}

	public String getDirtype() {
		return dirtype;
	}

	public void setDirtype(String dirtype) {
		this.dirtype = dirtype;
	}

	@Override
	public String toString() {
		return "MessageVo [dirtype=" + dirtype + ", text=" + text + ", candroponfirst=" + candroponfirst
				+ ", canDropOnSecond=" + canDropOnSecond + ", iconCls=" + iconCls + ", leaf=" + leaf + ", children="
				+ children + "]";
	}
}
