package cn.com.shuyangyang.domain;

/**
 * 部门实体类
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年1月25日下午5:11:23
 *
 */
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Dept {
	/** 部门ID */
	private String		dept_id;
	/** 部门名称 */
	private String		dept_name;
	/** 部门父节点 */
	private String		dept_father_id;
	/** 部门状态 */
	private EnumStatus	dept_status;
	/** 描述 */
	private String		description;
	/** 创建部门时间 */
	private Date		create_time;
	/** 更新时间 */
	private Date		update_time;
	
	private boolean		leaf; //true代表下面没有子节点，false表示下面有子节点
	
	private boolean 	expanded;//true代表展开子节点，false代表不展开
	
	private List<Dept>  children;
	
	private boolean 	checked;

	public Dept() {
		this.dept_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public Dept(String dept_id, String dept_name, String dept_father_id,
			EnumStatus dept_status, String description, Date create_time,
			Date update_time, boolean leaf, boolean expanded, List<Dept>  children,
			boolean checked) {
		super();
		this.dept_id = dept_id;
		this.dept_name = dept_name;
		this.dept_father_id = dept_father_id;
		this.dept_status = dept_status;
		this.description = description;
		this.create_time = create_time;
		this.update_time = update_time;
		this.leaf		= leaf;
		this.expanded	= expanded;
		this.children = children;
		this.checked = checked;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getDept_father_id() {
		return dept_father_id;
	}

	public void setDept_father_id(String dept_father_id) {
		this.dept_father_id = dept_father_id;
	}

	public EnumStatus getDept_status() {
		return dept_status;
	}

	public void setDept_status(EnumStatus dept_status) {
		this.dept_status = dept_status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public List<Dept> getChildren() {
		return children;
	}

	public void setChildren(List<Dept> children) {
		this.children = children;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	@Override
	public String toString() {
		return "Dept [dept_id=" + dept_id + ", dept_name=" + dept_name
				+ ", dept_father_id=" + dept_father_id + ", dept_status="
				+ dept_status + ", description=" + description
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + ", leaf=" + leaf + ", expanded=" + expanded
				+ ", children=" + children + ", checked=" + checked + "]";
	}
	
	
	
}
