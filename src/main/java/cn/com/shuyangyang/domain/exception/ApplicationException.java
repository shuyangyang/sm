package cn.com.shuyangyang.domain.exception;

public class ApplicationException extends Exception {

	private static final long serialVersionUID = -2368779913079477132L;

	private String message;
	
	public ApplicationException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return this.message;
	}
}
