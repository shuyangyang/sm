package cn.com.shuyangyang.domain.service;


/**年级班级VO
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年11月5日 下午4:20:16
 */
public class GradeClassVo {

	private String gradeClass;//显示的年级班级名称
	private String pValue;//选中年级班级的值
	
	public GradeClassVo(){}

	public String getGradeClass() {
		return gradeClass;
	}

	public void setGradeClass(String gradeClass) {
		this.gradeClass = gradeClass;
	}

	public String getpValue() {
		return pValue;
	}

	public void setpValue(String pValue) {
		this.pValue = pValue;
	}
	
}

