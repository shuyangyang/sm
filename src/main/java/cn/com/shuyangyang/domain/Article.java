package cn.com.shuyangyang.domain;

import java.util.Date;

public class Article {
	private String article_id;
	private String content;
	private String picUrl;
	private String article_url;
	private String source;
	private Date createTime;
	
	public Article(){}
	
	public Article(String article_id, String content, String picUrl, String article_url, String source,
			Date createTime) {
		super();
		this.article_id = article_id;
		this.content = content;
		this.picUrl = picUrl;
		this.article_url = article_url;
		this.source = source;
		this.createTime = createTime;
	}
	
	public String getArticle_id() {
		return article_id;
	}
	public void setArticle_id(String article_id) {
		this.article_id = article_id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getArticle_url() {
		return article_url;
	}
	public void setArticle_url(String article_url) {
		this.article_url = article_url;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
	
}
