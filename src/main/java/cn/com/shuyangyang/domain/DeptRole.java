package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 部门角色映射实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:30:06
 * 
 */
public class DeptRole {
	/**
	 * 部门角色表ID
	 * 
	 * @pdOid bde5ae8d-ad03-48fb-b061-83a5eff7e84d
	 */
	private String		dept_role_id;
	/**
	 * 部门ID
	 * 
	 * @pdOid 9f9351c5-0f98-409a-9521-e576c75ba068
	 */
	private String		dept_id;
	/**
	 * 角色ID
	 * 
	 * @pdOid 472f9881-f3f9-4af2-81ee-90d441938a0f
	 */
	private String		role_id;
	/**
	 * 状态，1启用，0禁用
	 * 
	 * @pdOid 1374bcf5-9f79-4f76-a394-4a32e5c2cf14
	 */
	private EnumStatus	status;
	/**
	 * 创建时间
	 * 
	 * @pdOid 6e651f48-fdbe-4a58-894c-0bc10bded526
	 */
	private Date		create_time;
	/**
	 * 更新时间
	 * 
	 * @pdOid 3d2c886b-dddd-4722-9cca-67a1d90012de
	 */
	private Date		update_time;

	public DeptRole() {
		this.dept_role_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public DeptRole(String dept_role_id, String dept_id, String role_id,
			EnumStatus status, Date create_time, Date update_time) {
		super();
		this.dept_role_id = dept_role_id;
		this.dept_id = dept_id;
		this.role_id = role_id;
		this.status = status;
		this.create_time = create_time;
		this.update_time = update_time;
	}

	public String getDept_role_id() {
		return dept_role_id;
	}

	public void setDept_role_id(String dept_role_id) {
		this.dept_role_id = dept_role_id;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "DeptRole [dept_role_id=" + dept_role_id + ", dept_id="
				+ dept_id + ", role_id=" + role_id + ", status=" + status
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + "]";
	}
}
