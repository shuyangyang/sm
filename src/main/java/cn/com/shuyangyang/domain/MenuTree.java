package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 菜单树
 * @author ShuYangYang
 * E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn
 * Date:2015年11月8日下午5:08:45
 *
 */
public class MenuTree {
	/** @pdOid 6deead97-4c96-4edd-a67d-53976c246e29 */
	private String		menu_id;
	/** @pdOid 1c7b8666-f96b-4f40-97ce-94c3f2019313 */
	private String		text;
	/** @pdOid 2c44d222-885d-4161-a6cc-8b4736ddde06 */
	private String		fatherId;
	/** @pdOid 9f2a55d9-ac71-405a-8a75-2239f4315a4d */
	private String		url;
	/** @pdOid e6a90bd2-331d-46a9-9cb4-ee8092d5d4ef */
	private EnumStatus	status;
	private short		sort;			// 排序数字
	private String		iconCls;		// 图标css类
	/** @pdOid 37cf7272-2fde-4c4c-b171-cb1d9463d0b8 */
	private String		description;
	/** @pdOid 997f766f-dcf3-4298-a96f-538085e8ccb0 */
	private Date		create_time;
	/** @pdOid 0f197429-bc26-4b9a-8d7e-9805ef0437dc */
	private Date		update_time;
	
	private boolean		leaf;

	private List<MenuTree>	children;
	
	private boolean 	checked;

	public MenuTree() {
		this.menu_id = UUID.randomUUID().toString().replace("-", "");
	}

	public MenuTree(String menu_id, String text, String fatherId, String url,
			EnumStatus status, short sort, String iconCls, String description,
			Date create_time, Date update_time, List<MenuTree> children, 
			boolean leaf, boolean checked) {
		super();
		this.menu_id = menu_id;
		this.text = text;
		this.fatherId = fatherId;
		this.url = url;
		this.status = status;
		this.sort = sort;
		this.iconCls = iconCls;
		this.leaf = leaf;
		this.description = description;
		this.create_time = create_time;
		this.update_time = update_time;
		this.children = children;
		this.checked = checked;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getFatherId() {
		return fatherId;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public short getSort() {
		return sort;
	}

	public void setSort(short sort) {
		this.sort = sort;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public List<MenuTree> getChildren() {
		return children;
	}

	public void setChildren(List<MenuTree> menus) {
		this.children = menus;
	}
	
	public boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	@Override
	public String toString() {
		return "MenuTree [menu_id=" + menu_id + ", text=" + text
				+ ", fatherId=" + fatherId + ", url=" + url + ", status="
				+ status + ", sort=" + sort + ", iconCls=" + iconCls
				+ ", description=" + description + ", create_time="
				+ create_time + ", update_time=" + update_time + ", leaf="
				+ leaf + ", children=" + children + ", checked=" + checked
				+ "]";
	}

}
