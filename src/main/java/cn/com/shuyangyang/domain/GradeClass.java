package cn.com.shuyangyang.domain;

import java.util.Date;

public class GradeClass {
	private String id;
	private String grade;
	private String theClass;
	private String remarks;
	private Date createTime;
	private Date updateTime;
	
	public GradeClass(){}

	public GradeClass(String id, String grade, String theClass, Date createTime, Date updateTime,String remarks) {
		super();
		this.id = id;
		this.grade = grade;
		this.theClass = theClass;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.remarks = remarks;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getTheClass() {
		return theClass;
	}

	public void setTheClass(String theClass) {
		this.theClass = theClass;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
