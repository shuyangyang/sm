package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 菜单权限映射实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:26:25
 * 
 */
public class MenuPermission {
	/** @pdOid 5a831eb0-5df8-42ad-b835-b06a52e26950 */
	private String			menuPermission_id;
	/** @pdOid 68b1af0b-67b5-4007-a451-dd72fbdf9d18 */
	private String			menu_id;
	/** @pdOid a687b00a-4e32-4162-aa22-d98f9253f0d1 */
	private String			permission_id;
	/** @pdOid 7dbf092c-40bd-4f1d-b3d9-42d2b81bb862 */
	private String			role_id;
	/** @pdOid 65c345f4-3a64-473f-a8fb-b3e5262983e0 */
	private EnumStatus		status;
	/** @pdOid 68d4ea93-8a21-4a0e-82f6-e9555fbde88c */
	private java.util.Date	create_time;
	/** @pdOid e7df8f9e-5b92-40fb-b101-8d5f145d66d2 */
	private java.util.Date	update_time;

	public MenuPermission() {
		this.menuPermission_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public MenuPermission(String menuPermission_id, String menu_id,
			String permission_id, String role_id, EnumStatus status,
			Date create_time, Date update_time) {
		super();
		this.menuPermission_id = menuPermission_id;
		this.menu_id = menu_id;
		this.permission_id = permission_id;
		this.role_id = role_id;
		this.status = status;
		this.create_time = create_time;
		this.update_time = update_time;
	}

	public String getMenuPermission_id() {
		return menuPermission_id;
	}

	public void setMenuPermission_id(String menuPermission_id) {
		this.menuPermission_id = menuPermission_id;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(String permission_id) {
		this.permission_id = permission_id;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public java.util.Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(java.util.Date create_time) {
		this.create_time = create_time;
	}

	public java.util.Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(java.util.Date update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "MenuPermission [menuPermission_id=" + menuPermission_id
				+ ", menu_id=" + menu_id + ", permission_id=" + permission_id
				+ ", role_id=" + role_id + ", status=" + status + ", create_time="
				+ create_time + ", update_time=" + update_time + "]";
	}
}
