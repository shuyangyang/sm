package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 消息实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 * http://www.shuyangyang.com.cn Date:2016年9月2日下午5:22:21
 * 
 */
public class MessAge {
	private String		message_id;//消息ID
	private String		message_title;//消息标题
	private String		message_text;//消息内容
	private Date		send_time;//发送时间
	private String		attachment;//附件
	private EnumStatus	message_type;//消息类型，为后续做草稿箱做准备（0正常消息，1草稿消息，2已删除消息）

	public MessAge() {
		this.message_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public MessAge(String message_id, String message_title, String message_text, Date send_time, String attachment,
			EnumStatus message_type) {
		super();
		this.message_id = message_id;
		this.message_title = message_title;
		this.message_text = message_text;
		this.send_time = send_time;
		this.attachment = attachment;
		this.message_type = message_type;
	}

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public String getMessage_title() {
		return message_title;
	}

	public void setMessage_title(String message_title) {
		this.message_title = message_title;
	}

	public String getMessage_text() {
		return message_text;
	}

	public void setMessage_text(String message_text) {
		this.message_text = message_text;
	}

	public Date getSend_time() {
		return send_time;
	}

	public void setSend_time(Date send_time) {
		this.send_time = send_time;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public EnumStatus getMessage_type() {
		return message_type;
	}

	public void setMessage_type(EnumStatus message_type) {
		this.message_type = message_type;
	}

	@Override
	public String toString() {
		return "MessAge [message_id=" + message_id + ", message_title=" + message_title + ", message_text="
				+ message_text + ", send_time=" + send_time + ", attachment=" + attachment + ", message_type="
				+ message_type + "]";
	}
}
