package cn.com.shuyangyang.domain;

/**
 * 用户实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:03:13
 * 
 */
import java.util.Date;
import java.util.UUID;

public class User {
	/** 用户ID */
	private String		user_id;
	/** 用户姓名 */
	private String		user_name;
	/** 用户登录名 */
	private String		login_name;
	/** 登录密码 */
	private String		login_password;
	/** 用户状态，1代表启用，0代表禁用 */
	private EnumStatus	user_status;
	/** 用户创建时间 */
	private Date		create_time;
	/** 最后更新时间 */
	private Date		update_time;

	public User() {
		this.user_id = UUID.randomUUID().toString().replace("-", "");
	}

	public User(String user_id, String user_name, String login_name,
			String login_password, EnumStatus user_status, Date create_time,
			Date update_time) {
		super();
		this.user_id = user_id;
		this.user_name = user_name;
		this.login_name = login_name;
		this.login_password = login_password;
		this.user_status = user_status;
		this.create_time = create_time;
		this.update_time = update_time;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getLogin_name() {
		return login_name;
	}

	public void setLogin_name(String login_name) {
		this.login_name = login_name;
	}

	public String getLogin_password() {
		return login_password;
	}

	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}

	public EnumStatus getUser_status() {
		return user_status;
	}

	public void setUser_status(EnumStatus user_status) {
		this.user_status = user_status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_name=" + user_name
				+ ", login_name=" + login_name + ", login_password="
				+ login_password + ", user_status=" + user_status
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + "]";
	}
}
