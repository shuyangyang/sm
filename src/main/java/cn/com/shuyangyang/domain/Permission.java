package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 权限实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:23:53
 * 
 */
public class Permission {
	/** @pdOid 40c48b2e-6f83-4fe2-a50b-9d7c9d63c32c */
	private String		permission_id;
	/** @pdOid 2f39ad5b-12a2-4fd6-9060-262f912b081e */
	private String		permission_name;
	/** @pdOid 31495843-e51b-4a88-99de-610ea4aceede */
	private String		kind;
	/** @pdOid 55605c18-69cb-4983-9cf1-549cdb2c2151 */
	private String		value;
	/** @pdOid 68fe8423-9fa8-4a41-8464-a547ca772aaa */
	private EnumStatus	status;
	/** @pdOid e2b4d3a8-1e7c-406e-becf-8f607f6ae1e6 */
	private String		permission_fatherId;
	/** @pdOid 36169452-67f5-461a-951e-6e43d9fe86c0 */
	private Date		create_time;
	/** @pdOid e0857407-a62f-44a9-ac30-29bb9db65605 */
	private Date		update_time;
	/** @pdOid fe34f125-1884-4ee7-8737-f2bd1c896ff6 */
	private String		description;

	public Permission() {
		this.permission_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public Permission(String permission_id, String permission_name,
			String kind, String value, EnumStatus status,
			String permission_fatherId, Date create_time, Date update_time,
			String description) {
		super();
		this.permission_id = permission_id;
		this.permission_name = permission_name;
		this.kind = kind;
		this.value = value;
		this.status = status;
		this.permission_fatherId = permission_fatherId;
		this.create_time = create_time;
		this.update_time = update_time;
		this.description = description;
	}

	public String getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(String permission_id) {
		this.permission_id = permission_id;
	}

	public String getPermission_name() {
		return permission_name;
	}

	public void setPermission_name(String permission_name) {
		this.permission_name = permission_name;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public String getPermission_fatherId() {
		return permission_fatherId;
	}

	public void setPermission_fatherId(String permission_fatherId) {
		this.permission_fatherId = permission_fatherId;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Permission [permission_id=" + permission_id
				+ ", permission_name=" + permission_name + ", kind=" + kind
				+ ", value=" + value + ", status=" + status
				+ ", permission_fatherId=" + permission_fatherId
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + ", description=" + description + "]";
	}
}
