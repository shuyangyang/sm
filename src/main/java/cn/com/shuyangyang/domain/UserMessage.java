package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 用户-消息实体类
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @http://www.shuyangyang.com.cn
 * @date 2016年9月5日 下午4:41:19
 */
public class UserMessage {
	private String		id;//用户消息ID
	private String		send_user;//发送人
	private String		receverid_user;//收件人
	private String		message_id;//消息ID
	private EnumStatus	send_status;//发送状态（0未发送，1已发送，2草稿）
	private EnumStatus	receverid_status;//回复状态（0未回复，1已回复）
	private EnumStatus	read_status;//阅读状态（0未读，1已读，2已删除）
	private String message_title;//消息主题
	private String attachment;//附件
	private Date   send_time;//发送时间
	private String message_text;//内容
	
	public UserMessage() {
		this.id  = UUID.randomUUID().toString().replace("-", "");
	}
	
	public UserMessage(String id, String send_user, String receverid_user, String message_id, EnumStatus send_status,
			EnumStatus receverid_status, EnumStatus read_status,String message_title,String attachment,Date	send_time,
			String message_text) {
		super();
		this.id = id;
		this.send_user = send_user;
		this.receverid_user = receverid_user;
		this.message_id = message_id;
		this.send_status = send_status;
		this.receverid_status = receverid_status;
		this.read_status = read_status;
		this.message_title = message_title;
		this.attachment = attachment;
		this.send_time = send_time;
		this.message_text = message_text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSend_user() {
		return send_user;
	}

	public void setSend_user(String send_user) {
		this.send_user = send_user;
	}

	public String getReceverid_user() {
		return receverid_user;
	}

	public void setReceverid_user(String receverid_user) {
		this.receverid_user = receverid_user;
	}

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public EnumStatus getSend_status() {
		return send_status;
	}

	public void setSend_status(EnumStatus send_status) {
		this.send_status = send_status;
	}

	public EnumStatus getReceverid_status() {
		return receverid_status;
	}

	public void setReceverid_status(EnumStatus receverid_status) {
		this.receverid_status = receverid_status;
	}

	public EnumStatus getRead_status() {
		return read_status;
	}

	public void setRead_status(EnumStatus read_status) {
		this.read_status = read_status;
	}

	public String getMessage_title() {
		return message_title;
	}

	public void setMessage_title(String message_title) {
		this.message_title = message_title;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Date getSend_time() {
		return send_time;
	}

	public void setSend_time(Date send_time) {
		this.send_time = send_time;
	}

	public String getMessage_text() {
		return message_text;
	}

	public void setMessage_text(String message_text) {
		this.message_text = message_text;
	}

	@Override
	public String toString() {
		return "UserMessage [id=" + id + ", send_user=" + send_user + ", receverid_user=" + receverid_user
				+ ", message_id=" + message_id + ", send_status=" + send_status + ", receverid_status="
				+ receverid_status + ", read_status=" + read_status + ", message_title=" + message_title
				+ ", attachment=" + attachment + ", send_time=" + send_time + ", message_text=" + message_text + "]";
	}
	
}
