package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 角色实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:22:21
 * 
 */
public class Role {
	/** @pdOid c337e97e-e3dc-473a-840b-7790e53b10f7 */
	private String		role_id;
	/** @pdOid a06c416f-9509-4af3-ae38-59f1d6885d36 */
	private String		role_name;
	/** @pdOid 0e8bcaf3-6d08-4f20-b059-197a688639f0 */
	private EnumStatus	status;
	/** @pdOid ca07b8ad-56a0-4c88-8d29-edd4a8386fb5 */
	private Date		create_time;
	/** @pdOid 4771ccd7-cefe-4198-b678-41005c92d04f */
	private Date		update_time;
	/** @pdOid bbab91f2-a5ec-4860-9906-a80c88e9d7f3 */
	private String		description;
	
	private boolean 	selected;

	public Role() {
		this.role_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public Role(String role_id, String role_name, EnumStatus status,
			Date create_time, Date update_time, String description,boolean selected) {
		super();
		this.role_id = role_id;
		this.role_name = role_name;
		this.status = status;
		this.create_time = create_time;
		this.update_time = update_time;
		this.description = description;
		this.selected = selected;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "Role [role_id=" + role_id + ", role_name=" + role_name
				+ ", status=" + status + ", create_time=" + create_time
				+ ", update_time=" + update_time + ", description="
				+ description + ", selected=" + selected + "]";
	}
	
	
}
