package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 用户部门映射实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月25日下午5:12:45
 */
public class UserDept {
	/**
	 * 用户部门表ID
	 * 
	 * @pdOid 177ca514-a867-4e5c-87fb-9ee702e954f4
	 */
	private String		user_dept_id;
	/**
	 * 用户ID
	 * 
	 * @pdOid 76abcf27-6755-4426-8d1a-5c24bb893cde
	 */
	private String		user_id;
	/**
	 * 部门ID
	 * 
	 * @pdOid 8cf451bc-cec7-4f41-bf38-708a14cbd0b8
	 */
	private String		dept_id;
	/**
	 * 状态
	 * 
	 * @pdOid 82c92adb-f265-42ce-85e5-f18adb27869e
	 */
	private EnumStatus	status;
	/**
	 * 创建时间
	 * 
	 * @pdOid a1eb5e22-5403-40cc-b709-833fd83b487f
	 */
	private Date		create_time;
	/**
	 * 更新时间
	 * 
	 * @pdOid 88663f56-a1af-4a77-bd1d-e39b7d777792
	 */
	private Date		update_time;

	public UserDept() {
		this.user_dept_id  = UUID.randomUUID().toString().replace("-", "");
	}

	public UserDept(String user_dept_id, String user_id, String dept_id,
			EnumStatus status, Date create_time, Date update_time) {
		super();
		this.user_dept_id = user_dept_id;
		this.user_id = user_id;
		this.dept_id = dept_id;
		this.status = status;
		this.create_time = create_time;
		this.update_time = update_time;
	}

	public String getUser_dept_id() {
		return user_dept_id;
	}

	public void setUser_dept_id(String user_dept_id) {
		this.user_dept_id = user_dept_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "UserDept [user_dept_id=" + user_dept_id + ", user_id="
				+ user_id + ", dept_id=" + dept_id + ", status=" + status
				+ ", create_time=" + create_time + ", update_time="
				+ update_time + "]";
	}
}
