package cn.com.shuyangyang.domain;

import java.util.Date;
import java.util.UUID;

/**
 * 登录相关日志实体类
 * 
 * @author ShuYangYang E-Mail:shuyangyang@aliyun.com
 *         http://www.shuyangyang.com.cn Date:2015年1月29日下午9:47:48
 * 
 */
public class LoginLog {
	/**
	 * 登录日志ID
	 * 
	 * @pdOid 395b8f87-397f-40d7-9c28-3745255480ca
	 */
	public String		loginLogId;
	/**
	 * COOKIE_ID
	 * 
	 * @pdOid 6c1eefea-5ac3-4b4e-bca1-e1d5fc47f97b
	 */
	public String		sid;
	/**
	 * 登录名
	 * 
	 * @pdOid 1caf213e-ed63-4489-be0f-d0077f0a36d4
	 */
	public String		loginName;
	/**
	 * 登录用户名
	 * 
	 * @pdOid cf640342-f01f-424a-8788-0aa19e88558b
	 */
	public String		userName;
	/**
	 * 登录用户所属部门名称
	 * 
	 * @pdOid 28abeb54-460d-4dfd-bc6d-fd6d1f8e537c
	 */
	public String		deptName;
	/**
	 * 登录用户所属角色名称
	 * 
	 * @pdOid adacd4b2-93ab-4f1e-8c12-d9daa4273620
	 */
	public String		roleName;
	/**
	 * 登录用户ID
	 * 
	 * @pdOid 9a5a54b4-92fd-4bbe-aa75-cf02f743afc9
	 */
	public String		ip;
	/**
	 * 状态
	 * 
	 * @pdOid 522eb4a5-a34c-4822-9f26-8c12e821393f
	 */
	public EnumStatus	status;
	/**
	 * 登录时间
	 * 
	 * @pdOid 87f6d3fa-9d51-42e2-aa4c-b51b3adc08d2
	 */
	public Date			loginTime;
	/**
	 * 登出时间
	 * 
	 * @pdOid 7861cc56-78ba-4703-a250-2acc5d3cec8c
	 */
	public Date			logoutTime;
	/**
	 * 最后更新时间
	 * 
	 * @pdOid 42dc9c52-f47c-4e57-8567-3ecf600db15e
	 */
	public Date			updateTime;

	public LoginLog() {
		this.loginLogId = UUID.randomUUID().toString().replace("-", "");
	}

	public LoginLog(String loginLogId, String sid, String loginName,
			String userName, String deptName, String roleName, String ip,
			EnumStatus status, Date loginTime, Date logoutTime, Date updateTime) {
		super();
		this.loginLogId = loginLogId;
		this.sid = sid;
		this.loginName = loginName;
		this.userName = userName;
		this.deptName = deptName;
		this.roleName = roleName;
		this.ip = ip;
		this.status = status;
		this.loginTime = loginTime;
		this.logoutTime = logoutTime;
		this.updateTime = updateTime;
	}

	public String getLoginLogId() {
		return loginLogId;
	}

	public void setLoginLogId(String loginLogId) {
		this.loginLogId = loginLogId;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public EnumStatus getStatus() {
		return status;
	}

	public void setStatus(EnumStatus status) {
		this.status = status;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
