package cn.com.shuyangyang.util;

/**字符串操作工具类
 * @author shuyangyang
 * @E-Mail:shuyangyang@aliyun.com
 * @Bolg:http://www.shuyangyang.com.cn
 * @date 2016年9月22日 下午6:04:02
 */
public class StringControllerUtil {
	/**
	 * JAVA截取字符串左侧指定长度的字符串
	 * @param input输入字符串
	 * @param count截取长度
	 * @return截取字符串
	 */
	public static String left(String input, int count) {
		if (BaseUtil.isSpace(input)) {
		return "";
		}
		count = (count > input.length()) ? input.length() : count;
		return input.substring(0, count);
	}
}

