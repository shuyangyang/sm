package cn.com.shuyangyang.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtil
{
  //一天 , 
  public static long ONE_DAY = 24 * 60 * 60 * 1000;
  // 一分钟
  public static long ONE_MINITE =  60 * 1000; 
	
  public static final String DYYYYMMDD = "DYYYYMMDD";
  public static final String DYYYYMM = "DYYYYMM";
  public static final String YY = "yy";
  public static final String YYYY = "yyyy";
  public static final String MM = "MM";
  public static final String DD = "dd";
  public static final String MM_DD = "MM/dd";
  public static final String YYYYMM = "yyyyMM";
  public static final String YYYYMMDD = "yyyyMMdd";
  public static final String YYYY_MM = "yyyy/MM";
  public static final String YY_MM_DD = "yy/MM/dd";
  public static final String YYYY_MM_DD = "yyyy/MM/dd";
  public static final String OYYYY_MM_DD = "yyyy-MM-dd";
  public static final String OYYYY_MM_DD_HH_MI = "yyyy-MM-dd hh:mm";
  public static final String HH_MI = "HH:mm";
  public static final String HHMI = "HHmm";
  public static final String YY_MM_DD_HH_MI = "yy/MM/dd HH:mm";
  public static final String YYYY_MM_DD_HH_MI = "yyyy/MM/dd HH:mm";
  public static final String YYYY_MM_DD_HH_MI_SS = "yyyy/MM/dd HH:mm:ss";
  public static final String HH_MI_SS = "HH:mm:ss";
  public static final String YYYYMMDDHHMI = "yyyyMMddHHmm";
  public static final String YYYYMMDDHHMISS = "yyyyMMddHHmmss";
  public static final String YYYYMMDD_HHMISS = "yyyyMMdd-HHmmss";
  public static final String YYYY$MM$DD$ = "yyyy年MM月dd日";
  public static final String YYYY$MM$ = "yyyy年MM月";
  public static final String MM$DD$ = "MM月dd日";
  public static final String DD$ = "dd日";
  public static final String ERAYYMMDD = "ERAYYMMDD";
  public static final String ERAYYMM = "ERAYYMM";
  public static final String HH = "HH";
  public static final String MI = "mm";
  public static final int YEAR = 1;
  public static final int MONTH = 2;
  public static final int DAY = 3;
  public static final int HOUR = 10;
  public static final int HOUR_OF_DAY = 11;
  public static final int MINUTE = 12;
  public static final int SECOND = 13;
  /**
	* 获取当前时间
	*/
  public static Date getCurrent()
  {
    Date currentDate = new Date();
    return currentDate;
  }
  /**
	* 获取当天起始时间
	*/  
  public static Date getCurrentdate()
  {
    String currentDate = getFormatDate("yyyyMMdd", 
      new Date());
    Date date = getDate("yyyyMMddHHmmss", currentDate + 
      "000000");
    return date;
  }
  /**
	* 获取当天结束时间
	*/ 
  public static Date getCurrentdateEnd()
  {
    String currentDate = getFormatDate("yyyyMMdd", 
      new Date());
    Date date = getDate("yyyyMMddHHmmss", currentDate + 
      "235959");
    return date;
  }

  /**
	* 获取date所在的月的星期数
	* @param date 日期 
	*/ 
  public static int getMonthWeekCount(Date date)
  {
    Date monthenddate = getMonthEndDay(date);
    Calendar cal = Calendar.getInstance();
    cal.setTime(monthenddate);
    return cal.get(4);
  }

  /**
	* 获取当前月份
	* @param date 日期 
	*/ 
  public static Date getCurrentMonth()
  {
    return getMonthFirstDay(new Date());
  }
  /**
	* 获取date所在的月的第一天的日期
	* @param date 日期 
	*/ 
  public static Date getMonthFirstDay(Date date)
  {
    String month = getFormatDate("yyyyMM", date) + "01";
    Date firstday = null;

    firstday = getDate("yyyyMMdd", month);

    return firstday;
  }
  /**
	* 获取date所在的月的第一天的日期
	* @param date 日期 
	*/ 
public static Date getMonthFirstDayTime(Date date)
{
  String month = getFormatDate("yyyyMM", date) + "01"+"23:59:59";
  Date firstday = null;

  firstday = getDate("yyyy-MM-dd HH:mm:ss", month);

  return firstday;
}
  
  /**
	* 获取date所在的星期的星期一的日期
	* @param date 日期 
	*/
  public static Date getFirstDayOfWeek(Date date)
  {
    Calendar c = new GregorianCalendar();
    c.setFirstDayOfWeek(2);
    c.setTime(date);
    c.set(7, c.getFirstDayOfWeek());
    return c.getTime();
  }
  /**
	* 获取date所在的星期的星期天的日期
	* @param date 日期 
	*/
	public static Date getFirstDayOfWeekSunDay(Date date)
	{
	  Calendar c = new GregorianCalendar();
	  c.setFirstDayOfWeek(1);
	  c.setTime(date);
	  c.set(7, c.getFirstDayOfWeek());
	  return c.getTime();
	}
  /**
	* 获取date所在的星期的起始时间字符串,格式为yyyy-MM-dd 00:00:00
	* @param date 日期 
	*/
  public static String getWeekFirstDayStr(Date date)
  {
    String str = getFormatDate("yyyy/MM/dd", getFirstDayOfWeek(date));
    String strArr = str.replace("/", "-");
    return strArr + " 00:00:00";
  }
  /**
	* 获取date日的起始时间字符串,格式为yyyy/MM/dd 00:00:00
	* @param date 日期 
	*/
	public static String getDayStartStr(Date date)
	{
	  String str = getFormatDate("yyyy-MM-dd", date);
	  return str + " 00:00:00";
	}
	  /**
	* 获取date日的结束时间字符串,格式为yyyy/MM/dd 00:00:00
	* @param date 日期 
	*/
	public static String getDayEndStr(Date date)
	{
	  String str = getFormatDate("yyyy-MM-dd", date);
	  return str + " 23:59:59";
	}
  /**
	* 获取date所在的星期的星期天的日期
	* @param date 日期 
	*/
  public static Date getLastDayOfWeek(Date date)
  {
    Calendar c = new GregorianCalendar();
    c.setFirstDayOfWeek(2);
    c.setTime(date);
    c.set(7, c.getFirstDayOfWeek() + 6);
    return c.getTime();
  }
  /**
	* 获取date所在的星期的星期天的日期
	* @param date 日期 
	*/
public static Date getLastDayOfWeekSunDay(Date date)
{
  Calendar c = new GregorianCalendar();
  c.setFirstDayOfWeek(1);
  c.setTime(date);
  c.set(7, c.getFirstDayOfWeek() + 6);
  return c.getTime();
}
  /**
	* 获取date所在的星期结束时间字符串,格式为yyyy-MM-dd 23:59:59
	* @param date 日期 
	*/
  public static String getWeekEndDayStr(Date date)
  {
    String str = getFormatDate("yyyy/MM/dd", getFirstDayOfWeek(date));
    String strArr = str.replace("/", "-");
    return strArr + " 23:59:59";
  }
  /**
	* 获取date所在的月的最后一天的日期
	* @param date 日期 
	*/
  public static Date getMonthEndDay(Date date)
  {
    Date endday = dateAdd(3, -1, dateAdd(
      2, 1, getMonthFirstDay(date)));
    return endday;
  }
  /**
	* 获取最后一天的日期 系统设定为29991231
	*/
  public static Date getEndDate()
  {
    return getDate("yyyyMMdd", "29991231");
  }

  /**
	* 获取服务的最后一天的日期 系统设定为99991231
	*/
  public static Date getServiceEndDate()
  {
    return getDate("yyyyMMdd", "99991231");
  }
  /**
	* 获取起始天的日期 系统设定为19900101
	*/
  public static Date getStartDate()
  {
    return getDate("yyyyMMdd", "19900101");
  }
  //获取本周第一天（周日为第一天）
  public static Date getWeekStarDay(Date date,int week){
	  Calendar calendar = Calendar.getInstance();
	  calendar.setTime(date);
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,1-dayOfWeek-7*week);
	  return calendar.getTime();
  }
  //获取本周最后一天（周日为第一天，周六为最后一天）
  public static Date getWeekEndDay(Date date,int week){
	  Calendar calendar = Calendar.getInstance();
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,7-dayOfWeek-7*week);
	  return calendar.getTime();
  }
  //获取上周最后一天（周日为第一天，周六为最后一天）
  public static Date getLastWeekEndDay(){
	  Calendar calendar = Calendar.getInstance();
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,-dayOfWeek);
	  return calendar.getTime();
  }
//获取上周第一天（周日为第一天，周六为最后一天）
  public static Date getLastWeekStarDay(){
	  Calendar calendar = Calendar.getInstance();
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,1-dayOfWeek-7);
	  return calendar.getTime();
  }
  //获取上上周最后一天（周日为第一天，周六为最后一天）
  public static Date getBeforeLastWeekEndDay(){
	  Calendar calendar = Calendar.getInstance();
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,-dayOfWeek-7);
	  return calendar.getTime();
  }
//获取上上周第一天（周日为第一天，周六为最后一天）
  public static Date getBeforeLastWeekStarDay(){
	  Calendar calendar = Calendar.getInstance();
	  int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK);
	  calendar.add(Calendar.DATE,1-dayOfWeek-14);
	  return calendar.getTime();
  }
//获取上上个月时间
	public static Date getBeforLastMonth(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -2);
		return calendar.getTime(); 
	}
	//获取上个月时间
	public static Date getLastMonth(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return calendar.getTime(); 
	}
	//获取上个月时间
	public static Date getAboutMonth(Date date,int i){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, i);
		return calendar.getTime(); 
	}
	//获取上个月时间
	public static Date getAboutDay(int i,Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, i);
		return calendar.getTime(); 
	}
	//获取月结束的时间
	public static String getEndOfMonthTime(Date date){
		return getEndOfMonthDate(date)+" 23:59:59";
	}
	//获取月开始的时间
	public static String getStarOfMonthTime(Date date){
		return getStarOfMonthDate(date)+" 00:00:00";
	}
	//获取月的最后一天
	public static String getEndOfMonthDate(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));	
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		return df.format(calendar.getTime());
	}
	//获取月的第一天
	public static String getStarOfMonthDate(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH,1);	
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		return df.format(calendar.getTime());
	}
	public static Date addOneSecond(Date date,int i) {    
	    Calendar calendar = Calendar.getInstance();    
	    calendar.setTime(date);    
	    calendar.add(Calendar.SECOND, i);    
	    return calendar.getTime();    
	} 
  /**
	* 获取指定月份的起始时间
	* @param month 月份
	*/
  public static Date getSelectMonth(String month)
  {
    Date time = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    String date = sdf.format(time);
    date = date + "-" + month + "-01 00:00:00.0";
    sdf.applyPattern("yyyy-MM");
    Date curDate = null;
    try {
      curDate = sdf.parse(date);
    }
    catch (ParseException e) {
    }
    return curDate;
  }
  /**
	* 获取指定月份的起始时间
	* @param month 月份
	*/
public static String  getaboutstarMonth(String month)
{
  month = month + "-01 00:00:00";
  return month;
}
/**
* 获取指定月份的起始时间
* @param month 月份
*/
public static Date getToDate(String time){
	Date date=null;
	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
	try {
		date = df.parse(time);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return date;
}
  /**
	* 根据生日获取年龄
	* @param birthday 生日日期字符串
	*/
  public static String getAge(String birthday)
  {
    if ((birthday == null) || ("".equals(birthday)))
      return "0";
    Date timenow = new Date();
    Date birth = null;

    birth = getDate("yyyyMMdd", birthday);
    int byear = Integer.parseInt(getFormatDate("yyyy", birth));
    int nyear = Integer.parseInt(getFormatDate("yyyy", timenow));
    int bmonth = Integer.parseInt(getFormatDate("MM", birth));
    int nmonth = Integer.parseInt(getFormatDate("MM", timenow));
    int age = nyear - byear;
    if (age < 0)
      return "0";
    if (nmonth < bmonth)
      --age;
    return String.valueOf(age);
  }
  /**
	* 根据生日获取年龄
	* @param birthday 生日日期
	*/
  public static String getAge(Date birthday)
  {
    if (birthday == null)
      return "0";
    Date timenow = new Date();
    int byear = Integer.parseInt(getFormatDate("yyyy", birthday));
    int nyear = Integer.parseInt(getFormatDate("yyyy", timenow));
    int bmonth = Integer.parseInt(getFormatDate("MM", birthday));
    int nmonth = Integer.parseInt(getFormatDate("MM", timenow));
    int age = nyear - byear;
    if (age < 0)
      return "0";
    if (nmonth < bmonth)
      --age;
    return String.valueOf(age);
  }
  /**
	* 根据年龄获取生日年份
	* @param age 年龄
	*/
  public static String getBirthday(String age)
  {
    if (age == null)
      return null;

    String nowyear = getFormatDate("yyyy", new Date());

    Long birthday = Long.parseLong(nowyear) - Long.parseLong(age);

    return birthday.toString();
  }
  /**
	* 根据年龄获取生日年份
	* @param age 年龄
	*/
	public static String getYesTerday()
	{
	  return getFormatDate("yyyy-MM-dd",dateAdd(3, -1, new Date()));
	}
	
	/**
	 * 取得日期
	 */
	public static String getMonthNow()
	{
		return getFormatDate("yyyy年MM月",new Date());
	}
	public static String getLastMonth()
	{
		return getFormatDate("yyyy年MM月",dateAdd(2, -1, new Date()));
	}
	public static String getToLastMonth()
	{
		return getFormatDate("yyyy-MM",dateAdd(2, -1, new Date()));
	}
	public static String getLastMonthTime(Date date)
	{
		return getFormatDate("yyyy-MM-dd",dateAdd(2, -1, date));
	}
	public static String getBeforeLastMonth()
	{
		return getFormatDate("yyyy年MM月",dateAdd(2, -2, new Date()));
	}
	public static String getToday()
	{
		return getFormatDate("yyyy-MM-dd",new Date());
	}
	public static String getNow()
	{
		return getFormatDate("yyyy-MM-dd HH:mm:ss",new Date());
	}
	public static String getTen()
	{
		return getFormatDate("yyyy-MM-dd HH:mm:ss",dateAdd(1,-10));
	}
	public static String getYesterday()
	{
		return getFormatDate("yyyy-MM-dd",dateAdd(3, -1, new Date()));
	}
	public static String getBeforeYesterday()
	{
		return getFormatDate("yyyy-MM-dd",dateAdd(3, -2, new Date()));
	}
	public static String getTomorrow()
	{
		return getFormatDate("yyyy-MM-dd",dateAdd(3, 1, new Date()));
	}
  /**
	* 根据生日日期字符串和指定日期获取指定日期时的年龄
	* @param birthday 生日日期字符串
	* @param curDate  指定日期
	*/
  public static String getAge(Date birthday, Date curDate)
  {
    if (birthday == null)
      return "0";
    Date timenow = curDate;
    int byear = Integer.parseInt(getFormatDate("yyyy", birthday));
    int nyear = Integer.parseInt(getFormatDate("yyyy", timenow));
    int bmonth = Integer.parseInt(getFormatDate("MM", birthday));
    int nmonth = Integer.parseInt(getFormatDate("MM", timenow));
    int age = nyear - byear;
    if (age < 0)
      return "0";
    if (nmonth < bmonth)
      --age;
    return String.valueOf(age);
  }

  /**
	* 根据格式和日期获取按格式显示的日期字符串
	* @param sFormat 格式
	* @param date    日期
	*/
  public static String getFormatDate(String sFormat, Date date)
  {
    if (date == null)
      return null;

    if ((sFormat == "yy") || (sFormat == "yyyy") || (sFormat == "yyyy-MM")||
      (sFormat == "MM") || (sFormat == "dd") || (sFormat == "MM-dd") || 
      (sFormat == "MM/dd") || (sFormat == "yyyyMM") || 
      (sFormat == "yyyyMMdd") || (sFormat == "yyyy/MM") || 
      (sFormat == "yy/MM/dd") || 
      (sFormat == "yyyy/MM/dd") || 
      (sFormat == "yyyy-MM-dd") || (sFormat == "yyyy-MM-dd HH:mm:ss") || 
      (sFormat == "yyyy-MM-dd HH:mm") || 
      (sFormat == "HH:mm") || (sFormat == "HHmm") || 
      (sFormat == "yy/MM/dd HH:mm") || 
      (sFormat == "yyyy/MM/dd HH:mm:ss") || 
      (sFormat == "yyyyMMddHHmmss") || (sFormat == "yyyyMMddHHmmssSSS") ||
      (sFormat == "yyyy年MM月dd日") ||(sFormat == "yyyy年M月d日") || (sFormat == "yyyy年") || 
      (sFormat == "yyyy年MM月") || (sFormat == "MM月dd日") ||(sFormat == "M月d日") || (sFormat == "M月") || 
      (sFormat == "dd日") || (sFormat == "HH") || (sFormat == "H") || 
      (sFormat == "mm") || (sFormat == "ss")|| (sFormat == "SSS") || (sFormat == "HH:mm:ss") || (sFormat == "HH:mm") ||
      (sFormat == "yyyy/MM/dd HH:mm")||(sFormat == "yyyyMMddHHmm")){
      SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
      return formatter.format(date);
    }
    return null;
  }
  /**
	* 根据格式和日期获取按格式显示的日期字符串
	* @param sFormat 格式
	* @param date    日期
	*/
	public static String getFormatDate(String sFormat, Timestamp timestamp)
	{
	  if (timestamp == null)
	    return null;
	
	  if ((sFormat == "yy") || (sFormat == "yyyy") || (sFormat == "yyyy-MM")||
	    (sFormat == "MM") || (sFormat == "dd") || (sFormat == "MM-dd") || 
	    (sFormat == "MM/dd") || (sFormat == "yyyyMM") || 
	    (sFormat == "yyyyMMdd") || (sFormat == "yyyy/MM") || 
	    (sFormat == "yy/MM/dd") || 
	    (sFormat == "yyyy/MM/dd") || 
	    (sFormat == "yyyy-MM-dd") || (sFormat == "yyyy-MM-dd HH:mm:ss") || 
	    (sFormat == "yyyy-MM-dd HH:mm") || 
	    (sFormat == "HH:mm") || (sFormat == "HHmm") || 
	    (sFormat == "yy/MM/dd HH:mm") || 
	    (sFormat == "yyyy/MM/dd HH:mm:ss") || 
	    (sFormat == "yyyyMMddHHmmss") || (sFormat == "yyyyMMddHHmmssSSS") ||
	    (sFormat == "yyyy年MM月dd日") ||(sFormat == "yyyy年M月d日") || (sFormat == "yyyy年") || 
	    (sFormat == "yyyy年MM月") || (sFormat == "MM月dd日") ||(sFormat == "M月d日") ||  
	    (sFormat == "dd日") || (sFormat == "HH") || (sFormat == "H") || 
	    (sFormat == "mm") || (sFormat == "ss")|| (sFormat == "SSS") || (sFormat == "HH:mm:ss") || (sFormat == "HH:mm") ||
	    (sFormat == "yyyy/MM/dd HH:mm") || (sFormat == "yyyy年MM月dd日 HH:mm:ss")||(sFormat == "yyyy年MM月dd日 HH:mm")||(sFormat == "MM月dd日 HH:mm")){
	    SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
	    return formatter.format(timestamp);
	  }
	  return null;
	}
  /**
	* 根据格式和日期获取按格式显示的日期
	* @param sFormat 格式
	* @param date    日期
	*/
  public static Date getDate(String sFormat, String date)
  {
    if ((date == null) || ("".equals(date)))
      return null;

    if ((sFormat == "yy") || (sFormat == "yyyy") || 
      (sFormat == "MM") || (sFormat == "dd") || 
      (sFormat == "MM/dd") || (sFormat == "yyyyMM") || 
      (sFormat == "yyyyMMdd") || (sFormat == "yyyy/MM") || 
      (sFormat == "yy/MM/dd") || 
      (sFormat == "yyyy/MM/dd") || 
      (sFormat == "yyyy-MM-dd") || (sFormat == "HH:mm") || 
      (sFormat == "yy/MM/dd HH:mm") ||                
      (sFormat == "yyyy/MM/dd HH:mm:ss") || (sFormat == "yyyy-MM-dd HH:mm:ss") ||
      (sFormat == "yyyyMMddHHmm") || 
      (sFormat == "yyyyMMddHHmmss") || 
      (sFormat == "yyyyMMdd-HHmmss") || 
      (sFormat == "yyyy年MM月dd日") || 
      (sFormat == "yyyy年MM月") || (sFormat == "MM月dd日") || 
      (sFormat == "dd日") || (sFormat == "HH") || 
      (sFormat == "mm")) {
      SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
      try {
        return formatter.parse(date);
      }
      catch (ParseException e) {
      }
    }
    return null;
  }
  /**
	* 根据格式获取下一个月的当前时间的前一天时间
	* @param sFormat 格式
	*/
  public static Date getLastDay(Date date)
  {
    if (date == null)
      return null;

    return dateAdd(3, -1, dateAdd(2, 1, date));
  }

  /**
	* 根据格式获取按格式显示的当前日期
	* @param sFormat 格式
	*/
  public static String getFormatDate(String sFormat)
  {
    return getFormatDate(sFormat, getCurrent());
  }
  /**
	* 为给定的日期添加或减去指定的时间量
	* @param iField 日历的规则(1为多少年,2为多少月,3为多少天,10为多少小时,11为一天中的小时,12为分,13为秒)
	* @param iValue 时间量
	* @param date 日期
	*/
  public static Date dateAdd(int iField, int iValue, Date date)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    switch (iField) { case 1:
      cal.add(cal.YEAR, iValue);
      break;
    case 2:
      cal.add(cal.MONTH, iValue);
      break;
    case 3:
      cal.add(cal.DATE, iValue);
      break;
    case 10:
      cal.add(cal.HOUR, iValue);
      break;
    case 11:
      cal.add(cal.HOUR_OF_DAY, iValue);
      break;
    case 12:
      cal.add(cal.MINUTE, iValue);
      break;
    case 13:
      cal.add(cal.SECOND, iValue);
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9: } return cal.getTime();
  }
  /**
	* 根据规则得到两个给定日期之前相差的时间
	* @param iField 日历的规则(1为多少年,2为多少月,3为多少天,10为多少小时,11为一天中的小时,12为分,13为秒)
	* @param startDate 起始时间
	* @param endDate 结束时间
	*/
  public static long dateDiff(int iField, Date startDate, Date endDate)
  {
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();

    int startYear = Integer.parseInt(getFormatDate("yyyy", startDate));
    int endYear = Integer.parseInt(getFormatDate("yyyy", endDate));
    int startMonth = Integer.parseInt(getFormatDate("MM", startDate)) - 1;
    int endMonth = Integer.parseInt(getFormatDate("MM", endDate)) - 1;
    int startDay = Integer.parseInt(getFormatDate("dd", startDate));
    int endDay = Integer.parseInt(getFormatDate("dd", endDate));
    int startHour = Integer.parseInt(getFormatDate("HH", startDate));
    int endHour = Integer.parseInt(getFormatDate("HH", endDate));
    int startMinute = Integer.parseInt(getFormatDate("mm", startDate));
    int endMinute = Integer.parseInt(getFormatDate("mm", endDate));
    int startSecond = Integer.parseInt(getFormatDate("ss", startDate));
    int endSecond = Integer.parseInt(getFormatDate("ss", endDate));
    switch (iField) { case 1:
      return (endYear - startYear);
    case 2:
      long yearDiff = endYear - startYear;
      long monthDiff = endMonth - startMonth;
      return (yearDiff * 12L + monthDiff);
    case 3:
      start.set(startYear, startMonth, startDay, 0, 0, 0);
      end.set(endYear, endMonth, endDay, 0, 0, 0);
      return ((end.getTimeInMillis() - start.getTimeInMillis()) / 
        86400000L);
    case 10:
      start.set(startYear, startMonth, startDay, startHour, 0, 0);
      end.set(endYear, endMonth, endDay, endHour, 0, 0);
      return ((end.getTimeInMillis() - start.getTimeInMillis()) / 
        3600000L);
    case 11:
      start.set(startYear, startMonth, startDay, startHour, 0, 0);
      end.set(endYear, endMonth, endDay, endHour, 0, 0);
      return ((end.getTimeInMillis() - start.getTimeInMillis()) / 
        3600000L);
    case 12:
      start.set(startYear, startMonth, startDay, startHour, startMinute, 
        0);
      end.set(endYear, endMonth, endDay, endHour, endMinute, 0);
      return ((end.getTimeInMillis() - start.getTimeInMillis()) / 
        60000L);
    case 13:
        start.set(startYear, startMonth, startDay, startHour, startMinute, 
        		startSecond);
        end.set(endYear, endMonth, endDay, endHour, endMinute, endSecond);
        return ((end.getTimeInMillis() - start.getTimeInMillis()) / 
          1000L);
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9: } return (end.getTimeInMillis() - start.getTimeInMillis());
  }
  /**
	* 为当前日期添加或减去指定的时间量
	* @param iField 日历的规则(1为多少年,2为多少月,3为多少天,10为多少小时,11为一天中的小时,12为分,13为秒)
	* @param iValue 时间量
	*/
  public static Date dateAdd(int iField, int iValue)
  {
    return dateAdd(iField, iValue, getCurrent());
  }

  /**
	* 根据日期获取按yyyyMMdd格式显示的日期
	* @param date    日期
	*/
  public static Date dateTrunc(Date date)
  {
    return getDate("yyyyMMdd", getFormatDate(
      "yyyyMMdd", date));
  }
  /**
	* 根据日期所在的月有多少天
	* @param date    日期
	*/
  public static long getMonthDayCount(Date date)
  {
    Date start = getMonthFirstDay(date);
    Date end = getMonthEndDay(date);
    return (dateDiff(3, start, end) + 1L);
  }
  /**
	* 根据类型和日期获取日期是第几个星期
	* @param type    类型(1为日期所在年份的第几个星期,2为日期所在月份的第几个星期)
	* @param date    日期
	*/
  public static int getWeekNum(int type, Date date)
  {
    Date monthenddate = getMonthEndDay(date);
    Calendar cal = Calendar.getInstance();
    cal.setTime(monthenddate);

    if (type == 1)
    {
      return cal.get(3);
    }
    if (type == 2)
    {
      return cal.get(4);
    }

    return 0;
  }
  /**
	* 根据小时,分钟,秒,月,天,年获取当前日期
	* @param hour 小时
	* @param minute 分钟
	* @param second 秒
	* @param month 月
	* @param day 天
	* @param year 年
	*/
  public static Date mktime(int hour, int minute, int second, int month, int day, int year)
  {
    Calendar cal = Calendar.getInstance();
    cal.set(year, month - 1, day, hour, minute, second);
    return cal.getTime();
  }
  /**
	* 获取指定日期的时间戳
	* @param date    日期
	*/
  public static Timestamp getTimestamp(Date date)
  {
    return new Timestamp(date.getTime());
  }
  /**
	* 获取指定日期的时间戳
	* @param date    日期
	*/
  public static Date getDateFormTimestamp(Timestamp timestamp)
  {
	  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	  try {
		return df.parse(df.format(timestamp));
	  } catch (ParseException e) {
	  }
	  return null;
  }
  /**
	* 获取当前日期的时间戳
	*/
  public static Timestamp getCurrentDateTimestamp()
  {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Timestamp createDttm = Timestamp.valueOf(df
      .format(new Date()));
    return createDttm;
  }
  
  /**
	* 获取两个给定日期之前相差的天数,小时,分钟
	* @param startDate 起始时间
	* @param endDate 结束时间
	*/
  public static long[] dateDiffEx(Date startDate, Date endDate)
  {
    long[] diffTime = new long[3];

    long minuteDiff = 0L;
    long hourDiff = 0L;
    long dayDiff = 0L;

    long diff = dateDiff(12, startDate, endDate);

    if (diff > 0L)
    {
      minuteDiff = diff % 60L;
      diff /= 60L;
    }

    if (diff > 0L)
    {
      hourDiff = diff % 24L;
      diff /= 24L;
    }

    if (diff > 0L)
    {
      dayDiff = diff;
    }

    diffTime[0] = dayDiff;
    diffTime[1] = hourDiff;
    diffTime[2] = minuteDiff;

    return diffTime;
  }

  /**
	* 获取两个给定日期之前相差的年,月,日,时,分,秒
	* @param startDate 起始时间
	* @param endDate 结束时间
	*/
  public static int[] getTimeDiff(Date startTime, Date endTime)
  {
    int[] ret = new int[6];
    if ((startTime == null) || (endTime == null))
      return null;
    int syear = 0;
    if (getFormatDate("yyyy", startTime) != null)
      syear = Integer.parseInt(getFormatDate("yyyy", 
        startTime));
    int eyear = 0;
    if (getFormatDate("yyyy", endTime) != null)
      eyear = Integer.parseInt(getFormatDate("yyyy", 
        endTime));

    int smonth = 0;
    if (getFormatDate("MM", startTime) != null)
      smonth = Integer.parseInt(getFormatDate("MM", 
        startTime));
    int emonth = 0;
    if (getFormatDate("MM", endTime) != null)
      emonth = Integer.parseInt(getFormatDate("MM", 
        endTime));
    int sday = 0;
    if (getFormatDate("dd", startTime) != null)
      sday = Integer.parseInt(getFormatDate("dd", 
        startTime));
    int eday = 0;
    if (getFormatDate("dd", endTime) != null)
      eday = Integer.parseInt(
        getFormatDate("dd", endTime));
    int shour = 0;
    if (getFormatDate("HH", startTime) != null)
      shour = Integer.parseInt(getFormatDate("HH", 
        startTime));
    int ehour = 0;
    if (getFormatDate("HH", endTime) != null)
      ehour = Integer.parseInt(getFormatDate("HH", 
        endTime));
    int sminute = 0;
    if (getFormatDate("mm", startTime) != null)
      sminute = Integer.parseInt(getFormatDate("mm", startTime));
    int eminute = 0;
    if (getFormatDate("mm", endTime) != null)
      eminute = Integer.parseInt(getFormatDate("mm", endTime));

    int ssecond = 0;
    if (getFormatDate("ss", endTime) != null)
      ssecond = Integer.parseInt(getFormatDate("ss", startTime));
    int esecond = 0;
    if (getFormatDate("ss", endTime) != null)
      esecond = Integer.parseInt(getFormatDate("ss", endTime));

    int secondDif = esecond - ssecond;

    int minuteDif = eminute - sminute;

    int hourDif = ehour - shour;

    int dayDif = eday - sday;

    int monthDif = emonth - smonth;

    int yearDif = eyear - syear;

    if (secondDif < 0) {
      secondDif += 60;
      --minuteDif;
    }
    if (minuteDif < 0) {
      minuteDif += 60;
      --hourDif;
    }
    if (hourDif < 0) {
      hourDif += 24;
      --dayDif;
    }

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(endTime);
    int days = calendar.getMaximum(5);

    if (dayDif < 0) {
      dayDif += days;
      --monthDif;
    }
    if (monthDif < 0) {
      monthDif += 12;
      --yearDif;
    }

    ret[0] = yearDif;
    ret[1] = monthDif;
    ret[2] = dayDif;
    ret[3] = hourDif;
    ret[4] = minuteDif;
    ret[5] = secondDif;

    return ret;
  }
  /**
	* 根据给定格式和获取给定日期的星期
	* @param sFormat 格式
	* @param date    日期
	*/
  public static String getWeekday(String sFormat, String date)
  {
    Date datetime = getDate(sFormat, date);

    return getWeekday(datetime);
  }
  /**
	* 获取给定日期的星期
	* @param date    日期
	*/
  public static String getWeekday(Date date)
  {
    String week = "";

    if (date != null) {
      int day = date.getDay();
      switch (day)
      {
      case 1:
        week = "星期一";
        break;
      case 2:
        week = "星期二";
        break;
      case 3:
        week = "星期三";
        break;
      case 4:
        week = "星期四";
        break;
      case 5:
        week = "星期五";
        break;
      case 6:
        week = "星期六";
        break;
      case 0:
        week = "星期日";
      }

    }

    return week;
  }
  /**
	* 根据给定格式和日期获取按格式显示的日期字符串
	* @param sFormat 格式
	* @param date    日期
	*/
  public static String getStringDate(String sFormat, Date date)
  {
    if (date == null) {
      return null;
    }

    String year = getFormatDate("yyyy", date);

    String month = getFormatDate("MM", date);

    String day = getFormatDate("dd", date);

    if ("DYYYYMMDD".equals(sFormat))
      return year + "." + month + "." + day;

    if ("DYYYYMM".equals(sFormat))
      return year + "." + month;

    return "";
  }
  /**
   * 根据指定月数给出中文字
   * @param month
   * @return
   */
  public static String parseMonthNumber(int month){
		switch(month){
			case 1:
				return "一";
			case 2:
				return "二";
			case 3:
				return "三";
			case 4:
				return "四";
			case 5:
				return "五";
			case 6:
				return "六";
			case 7:
				return "七";
			case 8:
				return "八";
			case 9:
				return "九";
			case 10:
				return "十";
			case 11:
				return "十一";
			case 12:
				return "十二";
				
		}
		return "";
	}
  
  /**
   * 判断日期是否是yyyy-MM-dd的格式
   * @param dateString
   * @return
   */
  public static boolean validate(String dateString){
		//使用正则表达式 测试 字符 符合 dddd-dd-dd 的格式(d表示数字)
		Pattern p = Pattern.compile("\\d{4}+[-]\\d{1,2}+[-]\\d{1,2}+");
		Matcher m = p.matcher(dateString);
		if(!m.matches()){	return false;} 
		
		//得到年月日
		String[] array = dateString.split("-");
		int year = Integer.valueOf(array[0]);
		int month = Integer.valueOf(array[1]);
		int day = Integer.valueOf(array[2]);
		
		if(month<1 || month>12){	return false;}
		int[] monthLengths = new int[]{0, 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if(isLeapYear(year)){
			monthLengths[2] = 29;
		}else{
			monthLengths[2] = 28;
		}
		int monthLength = monthLengths[month];
		if(day<1 || day>monthLength){
			return false;	
		}
		return true;
	}
  
  /** 是否是闰年 */
  	public static boolean isLeapYear(int year){
		return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) ;
	}
  	public static void main(String args[]){
  		//for test
	}
  	/**
  	 * 日期格式化
  	 * @param date
  	 * @return
  	 */
  	public static String getDateFormat(Date date){
  		long second=dateDiff(13,date,new Date());
  		if(second<60){
  			if(second <1){
  				second =1;
  			}
  			return second +"秒前";
  		}else{
  			long minute = (long)(second/60);
  			if(minute<60){
  				return minute +"分钟前";
  			}else{
  				if(dateDiff(13,date,getDate("yyyy/MM/dd HH:mm:ss", getDayStartStr(new Date())))<0){
  					return "今天"+getFormatDate("HH:mm", date);
  				}else{
  					return getFormatDate("M月d日", date)+" "+getFormatDate("HH:mm", date);
  				}
  			}
  		}
  	}
  	/**
  	 * 日期格式化
  	 * @param date
  	 * @return
  	 */
  	public static String getDateFormat(Timestamp timestamp){
  		Date date = getDate("yyyy-MM-dd HH:mm:ss",getFormatDate("yyyy-MM-dd HH:mm:ss",timestamp));
  		long second=dateDiff(13,date,new Date());
  		if(second<60){
  			if(second <1){
  				second =1;
  			}
  			return second +"秒前";
  		}else{
  			long minute = (long)(second/60);
  			if(minute<60){
  				return minute +"分钟前";
  			}else{
  				if(dateDiff(13,date,getDate("yyyy/MM/dd HH:mm:ss", getDayStartStr(new Date())))<0){
  					return "今天"+getFormatDate("HH:mm", date);
  				}else{
  					return getFormatDate("M月d日", date)+" "+getFormatDate("HH:mm", date);
  				}
  			}
  		}
  	}
  	/**
  	 * 日期格式化
  	 * @param date
  	 * @return
  	 */
  	public static String getDateFormatDiary(Date date){
  		long day=dateDiff(3,date,new Date());
  		if(day<1){
  			return "今天";
  		}
		if(day<2){
			return "昨天";
		}
		if(day<3){
			return "前天";
		}
		return day+"天前";
  	}
  	/**
	* 判断字串是否能转换成date格式,成功为true,失败为false
	* @param sFormat 格式
	* @param date    日期
	*/
  public static boolean getDateBoolean(String sFormat, String date)
  {
    if ((date == null) || ("".equals(date)))
      return false;

    if ((sFormat == "yy") || (sFormat == "yyyy") || 
      (sFormat == "MM") || (sFormat == "dd") || 
      (sFormat == "MM/dd") || (sFormat == "yyyyMM") || 
      (sFormat == "yyyyMMdd") || (sFormat == "yyyy/MM") || 
      (sFormat == "yy/MM/dd") || 
      (sFormat == "yyyy/MM/dd") || 
      (sFormat == "yyyy-MM-dd") || (sFormat == "HH:mm") || 
      (sFormat == "yy/MM/dd HH:mm") ||                
      (sFormat == "yyyy/MM/dd HH:mm:ss") || (sFormat == "yyyy-MM-dd HH:mm:ss") ||
      (sFormat == "yyyyMMddHHmm") || 
      (sFormat == "yyyyMMddHHmmss") || 
      (sFormat == "yyyyMMdd-HHmmss") || 
      (sFormat == "yyyy年MM月dd日") || 
      (sFormat == "yyyy年MM月") || (sFormat == "MM月dd日") || 
      (sFormat == "dd日") || (sFormat == "HH") || 
      (sFormat == "mm")) {
      SimpleDateFormat formatter = new SimpleDateFormat(sFormat);
      try {
         formatter.parse(date);
         return true;
      }
      catch (ParseException e) {
      }
    }
    return false;
  }

	/**
	 * 获取昨天0点0分0秒的Date
	 * @param date
	 * @return
	 */
	public static Date getYesterdayDate(){
		return getDate("yyyy-MM-dd", getYesterday());
	}
	
	/**
	 * 获取今天0点0分0秒的Date,
	 * @return
	 */
	public static Date getTodayDate(){
		return getDate("yyyy-MM-dd", getToday());
	}
	/**
	 * 日期格式化
	 * @param date
	 * @return
	 */
	public static String getDateFormatDiary2(Date date){
		String dateStr = getFormatDate("yyyy-MM-dd", date);
		if(dateStr.equals(getToday())){
			return "今天";
		}
		if(dateStr.equals(getYesterday())){
			return "昨天";
		}
		if(dateStr.equals(getBeforeYesterday())){
			return "前天";
		}
		return DateUtil.getFormatDate("MM/dd", date);
	}
	/**
	 * date 距离今天几天  算出有益食物几天没吃会用到
	 * @param date
	 * @return
	 */
	public static int numTheDate2Today(Date date){
		return (int) ((getTodayDate().getTime() - date.getTime()) / DateUtil.ONE_DAY);
	}
	
	/**
	 *  获取当前月份
	 * @param date
	 * @return int 例如10  
	 */
	public static int getMonth() {
		  Calendar g = Calendar.getInstance();
		  return g.get(Calendar.MONTH)+1;//获得当前月份
	}
	
	/**
	 *  获取当前日期   多少号  
	 * @param date
	 * @return int 例如21  
	 */
	public static int getDay() {
		  Calendar g = Calendar.getInstance();
		  return g.get(Calendar.DAY_OF_MONTH);//获得当前日期
	}
}