# 学校综合管理平台beta1.0 #

业余时间做的一个小项目，目前还有很多功能没做，现在开源出来。因为只是学习的项目，代码质量不是太好，请大家多多包涵。

主要技术：
EXTJS4.2、SpringMVC、Mybatis、WebSocket、JETTY

此项目是做为学习extjs4而编写的。
启动入口：cn.com.shuyangyang.MyServer

### 主要截图： ###

### 登录界面： ###
![1.jpg](https://bitbucket.org/repo/AA5Bge/images/3623711572-1.jpg)

### 首页 ###
![2.jpg](https://bitbucket.org/repo/AA5Bge/images/1609439636-2.jpg)

### 各个模块： ###
![3.jpg](https://bitbucket.org/repo/AA5Bge/images/2743348968-3.jpg)

![4.jpg](https://bitbucket.org/repo/AA5Bge/images/2348305085-4.jpg)
![5.jpg](https://bitbucket.org/repo/AA5Bge/images/1181109605-5.jpg)

更多请关注我的个人网站：http://www.shuyangyang.com.cn/